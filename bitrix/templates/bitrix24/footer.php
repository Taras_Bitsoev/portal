<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/footer.php");?>

			</div>
		</div>
</td>
<td class="bx-layout-inner-right">
	<?if($APPLICATION->GetCurPage(true) == SITE_DIR."index.php"):?>
		<div id="sidebar">
			<?$APPLICATION->SetPageProperty("BodyClass", "start-page");?>
			<?$APPLICATION->ShowViewContent("sidebar")?>
			<?$APPLICATION->ShowViewContent("sidebar_tools_1")?>
			<?$APPLICATION->ShowViewContent("sidebar_tools_2")?>
		</div>
	<?endif?>
</td>
</tr>
<tr>
	<td class="bx-layout-inner-left"></td>
	<td class="bx-layout-inner-center">
		<div id="footer">
			<div class="footer-content-lowerEnd-wrap"><div class="footer-content-lowerEnd"></div></div>
			<span id="copyright">
				<?if (IsModuleInstalled("bitrix24")):?>
				<!--<span class="bitrix24-copyright"><?=GetMessage("BITRIX24_COPYRIGHT1")?></span>-->
				<span class="bx-lang-btn <?=LANGUAGE_ID?>" id="bx-lang-btn">
					<span class="bx-lang-btn-icon"></span>
				</span>
				<a id="bitrix24-logo" href="<?=GetMessage("BITRIX24_URL")?>"></a>
				<?endif?>
				<span class="bitrix24-copyright"><?=GetMessage("BITRIX24_COPYRIGHT2", array("#CURRENT_YEAR#" => date("Y")))?></span>
			</span>
			<?if (!$isIpAccessDenied):?>
				<?if (IsModuleInstalled("bitrix24")):
					if ($partnerID = COption::GetOptionString("bitrix24", "partner_id", "")):
						$arParamsPartner = array();
						$arParamsPartner["MESS"] = array(
							"BX24_PARTNER_TITLE" => GetMessage("BX24_SITE_PARTNER"),
							"BX24_CLOSE_BUTTON" => GetMessage("BX24_CLOSE_BUTTON"),
							"BX24_LOADING" => GetMessage("BX24_LOADING"),
						);
						?>
						<a href="javascript:void(0)" onclick="showPartnerForm(<?echo CUtil::PhpToJSObject($arParamsPartner)?>); return false;" class="footer-discuss-link"><?=GetMessage("BITRIX24_PARTNER_CONNECT")?></a><?
					else:?>
						<a href="<?=GetMessage("BITRIX24_SSL_URL")?>" target="_blank" class="footer-discuss-link"><?=GetMessage("BITRIX24_REVIEW")?></a><?
					endif?>
				<?elseif (file_exists($_SERVER["DOCUMENT_ROOT"].SITE_DIR."services/help/")):?>
					<a href="<?=SITE_DIR?>services/help/" class="footer-discuss-link"><?=GetMessage("BITRIX24_MENU_HELP")?></a>
				<?endif;?>
			<?endif?>
		</div>
	</td>
	<td class="bx-layout-inner-right"></td>
</tr>
</table>
</td>
</tr>
</table>
<div id="feed-up-btn-wrap" class="feed-up-btn-wrap" onclick="B24.goUp();">
	<div class="feed-up-btn"><span class="feed-up-text"><?=GetMessage("BITRIX24_UP")?></span><span class="feed-up-btn-icon"></span></div>
</div>


<script>		
$(document).ready(function(){
	 var log = $('.feed-com-files-cont').children('span');
	 jQuery.each(log, function() {
	    var elem = $(this).children('img');
		var src = elem.attr('src').split('?')[0];
		elem.attr('src',src);
		elem.attr('data-bx-src',src);
	});
});
</script>
<?

$APPLICATION->ShowViewContent("im");
$APPLICATION->ShowBodyScripts();

if (defined("BX24_HOST_NAME")):?>
<script>
var _baLoaded = BX.type.isArray(_ba);
var _ba = _ba || []; _ba.push(["aid", "1682f9867b9ef36eacf05e345db46f3c"]);
(function(alreadyLoaded) {
	if (alreadyLoaded)
	{
		return;
	}
	var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;
	ba.src = document.location.protocol + "//bitrix.info/ba.js";
	var s = document.getElementsByTagName("script")[0];
	s.parentNode.insertBefore(ba, s);
})(_baLoaded);
</script>
<?endif;?>
<script type="text/javascript">BX.onCustomEvent(window, "onScriptsLoaded");</script>

<script>
	BX.ready(function(){
		var lang_toggle = BX('bx-lang-btn');
		if (lang_toggle)
		{
			BX.bind(lang_toggle, 'click', function(){
				BX.PopupMenu.show('language-popup', lang_toggle, [
					{text : "<?=GetMessage("BITRIX24_LANG_RU")?>", className : "ru", onclick : function() { B24.changeLanguage("ru"); }},
					{text : "<?=GetMessage("BITRIX24_LANG_EN")?>", className : "en", onclick : function() { B24.changeLanguage("en"); }},
					{text : "<?=GetMessage("BITRIX24_LANG_DE")?>", className : "de", onclick : function() { B24.changeLanguage("de"); }},
					{text : "<?=GetMessage("BITRIX24_LANG_UA")?>", className : "ua", onclick : function() { B24.changeLanguage("ua"); }},
					{text : "<?=GetMessage("BITRIX24_LANG_LA")?>", className : "la", onclick : function() { B24.changeLanguage("la"); }}
				],
					{   offsetTop:10,
						offsetLeft:0
					}
				);
			})
		}
	});
</script>
</body>
</html>

