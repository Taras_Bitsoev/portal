<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init(array("access", "fx"));

function IsSubItemSelected($ITEMS)
{
	if (is_array($ITEMS))
	{
		foreach($ITEMS as $arItem)
		{
			if ($arItem["SELECTED"])
				return true;
		}
	}
	return false;
}

if (empty($arResult))
	return;

$arHiddenItemsSelected = array();
$sumHiddenCounters = 0;
$arHiddenItemsCounters = array();
$arAllItemsCounters = array();
?><div id="bx_b24_menu"><?
$inbinYAYA = CUser::GetID();
$YAYA = CUser::GetUserGroup($inbinYAYA);
$group_id = array(1,9);
if( in_array(1, $YAYA) ||   in_array(9, $YAYA))
{
?>
<form  id="general_form"  action="/table/bid/test.php?clear_cache=Y" method="post" name="ticket_statistick" accept-charset="UTF-8" method="post">
	<span  class="filter_name" style="">По ID Задачи:</span>
	<input id="general_input" type="text" name="TASK_ID" value="" placeholder="Только цифры" alt="">
	<span  class="task-list-toolbar-search-icon" style=""></span>
</form>
<?
}
foreach($arResult["TITLE_ITEMS"] as $title => $arTitleItem)
{
	if (is_array($arResult["SORT_ITEMS"][$title]["show"]) || is_array($arResult["SORT_ITEMS"][$title]["hide"]))
	{
		$hideOption = CUserOptions::GetOption("bitrix24", $arTitleItem["PARAMS"]["class"]);
		$SubItemSelected = false;
		if (!is_array($hideOption) || $hideOption["hide"] == "Y")
			$SubItemSelected = IsSubItemSelected($arResult["SORT_ITEMS"][$title]["show"]) || IsSubItemSelected($arResult["SORT_ITEMS"][$title]["hide"]) ? true : false;

		if (IsModuleInstalled("bitrix24"))
			$disabled = (!is_array($hideOption) && $arTitleItem["PARAMS"]["class"]=="menu-crm" && !$SubItemSelected) || (is_array($hideOption) && $hideOption["hide"] == "Y" && !$SubItemSelected);
		else
			$disabled = (!is_array($hideOption) && $arTitleItem["PARAMS"]["class"]!="menu-favorites" && !$SubItemSelected) || (is_array($hideOption) && $hideOption["hide"] == "Y" && !$SubItemSelected);?>

		<div class="menu-items-block <?=$arTitleItem["PARAMS"]["class"]?> " <?if ($arTitleItem["PARAMS"]["is_empty"] == "Y"):?>style="display:none"<?endif?> id="div_<?=$arTitleItem["PARAMS"]["menu_item_id"]?>">
			<div id="<?=$arTitleItem["PARAMS"]["menu_item_id"]?>" class="menu-items-title <?=$arTitleItem["PARAMS"]["class"]?>">
				<?if ($arTitleItem["PARAMS"]["class"] == "menu-favorites"):?>
					<span class="menu-items-title-text"><?echo $arTitleItem["TEXT"]?></span>
					<span class="menu-favorites-btn menu-favorites-settings" id="menu_favorites_settings" onclick="EditMode();" title="<?=GetMessage("MENU_SETTINGS_TITLE")?>"><span class="menu-fav-settings-icon"></span></span><span class="menu-favorites-btn menu-favorites-btn-done" onclick="EditMode();"><?=GetMessage("MENU_EDIT_READY")?></span>
				<?else:?>
					<?echo $arTitleItem["TEXT"]?>
					<span class="menu-toggle-text"><?=($disabled ? GetMessage("MENU_SHOW") : GetMessage("MENU_HIDE"))?></span>
				<?endif?>
			</div>
			<ul  class="menu-items<?if ($disabled):?> menu-items-close<?endif;?>" id="ul_<?=$arTitleItem["PARAMS"]["menu_item_id"]?>">
				<li class="menu-items-empty-li" id="empty_li_<?=$arTitleItem["PARAMS"]["menu_item_id"]?>" style="height: 3px;"></li>
				<?
				$arTmp = array("show", "hide");
				foreach($arTmp as $status)
				{
					if ($status=="hide"):?>
					<li class="menu-item-separator" id="separator_<?=$arTitleItem["PARAMS"]["menu_item_id"]?>">
						<span class="menu-item-sepor-text"><?=GetMessage("MENU_HIDDEN_ITEMS")?></span>
						<span class="menu-item-sepor-line"></span>
					</li>
					<li class="menu-item-block menu-item-favorites-more" id="hidden_items_li_<?=$arTitleItem["PARAMS"]["menu_item_id"]?>">
						<ul class="menu-items-fav-more-block" id="hidden_items_ul_<?=$arTitleItem["PARAMS"]["menu_item_id"]?>">
					<?endif;
					if (is_array($arResult["SORT_ITEMS"][$title][$status]))
					{
						foreach($arResult["SORT_ITEMS"][$title][$status] as $arItem)
						{
							if ($arItem["PERMISSION"] > "D")
							{
								$couterId = "";
								$counter = 0;
								if (array_key_exists("counter_id", $arItem["PARAMS"]) && strlen($arItem["PARAMS"]["counter_id"]) > 0)
								{
									$couterId = $arItem["PARAMS"]["counter_id"] == "live-feed" ? "**" : $arItem["PARAMS"]["counter_id"];
									$counter = isset($GLOBALS["LEFT_MENU_COUNTERS"]) && array_key_exists($couterId, $GLOBALS["LEFT_MENU_COUNTERS"]) ? $GLOBALS["LEFT_MENU_COUNTERS"][$couterId] : 0;
									if ($couterId == "crm_cur_act")
									{
										$counterCrm = (isset($GLOBALS["LEFT_MENU_COUNTERS"]) && array_key_exists("CRM_**", $GLOBALS["LEFT_MENU_COUNTERS"]) ? intval($GLOBALS["LEFT_MENU_COUNTERS"]["CRM_**"]) : 0);
										$counterAct = $counter;
										$counter += $counterCrm;
									}
								}

								if ($couterId == "bp_tasks" && IsModuleInstalled("bitrix24"))
								{
									$showMenuItem = CUserOptions::GetOption("bitrix24", "show_bp_in_menu", false);
									if ($showMenuItem === false && $counter > 0)
									{
										CUserOptions::SetOption("bitrix24", "show_bp_in_menu", true);
										$showMenuItem = true;
									}

									if ($showMenuItem === false)
										continue;
								}

								if ($couterId)
								{
									$arAllItemsCounters[$couterId] = $counter;
									if ($status=="hide")
									{
										$sumHiddenCounters+= $counter;
										$arHiddenItemsCounters[] = $couterId;
									}
								}
								?>
								<li <?if ($title!= "menu-favorites" && in_array($arItem["PARAMS"]["menu_item_id"],$arResult["ALL_FAVOURITE_ITEMS_ID"])):?>style="display:none; " <?endif?>
									id="<?if ($title!= "menu-favorites" && in_array($arItem["PARAMS"]["menu_item_id"],$arResult["ALL_FAVOURITE_ITEMS_ID"])) echo "hidden_"; echo $arItem["PARAMS"]["menu_item_id"]?>"
									data-status="<?=$status?>"
									data-title-item="<?=$arTitleItem["PARAMS"]["menu_item_id"]?>"
									data-counter-id="<?=$couterId?>"
									data-can-delete-from-favorite="<?=$arItem["PARAMS"]["can_delete_from_favourite"]?>"
									<?if (isset($arItem["PARAMS"]["is_application"])):?>
										data-app-id="<?=$arItem["PARAMS"]["app_id"]?>"
									<?endif?>
									class="menu-item-block <?if ($arItem["SELECTED"]):?> menu-item-active<?endif?><?if($counter > 0 && strlen($couterId) > 0 && (!$arItem["SELECTED"] || ($arItem["SELECTED"] && $couterId == "bp_tasks"))):?> menu-item-with-index<?endif?><?if ((IsModuleInstalled("bitrix24") && $arItem["PARAMS"]["menu_item_id"] == "menu_live_feed") || $arItem["PARAMS"]["menu_item_id"] == "menu_all_groups"):?> menu-item-live-feed<?endif?>">
									<?if (!((IsModuleInstalled("bitrix24") && $arItem["PARAMS"]["menu_item_id"] == "menu_live_feed") || $arItem["PARAMS"]["menu_item_id"] == "menu_all_groups")):?>
										<span class="menu-fav-editable-btn menu-favorites-btn" onclick="B24menuItemsObj.OpenMenuPopup(this, '<?=CUtil::JSEscape($arItem["PARAMS"]["menu_item_id"])?>')"><span class="menu-favorites-btn-icon"></span></span>
										<span class="menu-favorites-btn menu-favorites-draggable" onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');" onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');"><span class="menu-fav-draggable-icon"></span></span>
									<?endif?>
									<a class="menu-item-link" href="<?=($arItem["LINK"]=="/index.php") ? "/" : $arItem["LINK"]?>" onclick="if (IsEditMode()) return false;"><span class="menu-item-link-text"><?=$arItem["TEXT"]?>
										<?if (strlen($couterId) > 0):?>
											<span class="menu-item-index-wrap"><span class="menu-item-index"<?=($arItem["PARAMS"]["counter_id"] == "crm_cur_act" ? ' data-counter-crmstream="'.intval($counterCrm).'" data-counter-crmact="'.intval($counterAct).'"' : '')?> id="menu-counter-<?=strtolower($arItem["PARAMS"]["counter_id"])?>"><?=($arItem["PARAMS"]["counter_id"] == "mail_unseen" ? ($counter > 99 ? "99+" : $counter) : ($counter > 50 ? "50+" : $counter)); ?></span></span>
											<?if (!empty($arItem["PARAMS"]["warning_link"])):?>
												<span onclick="window.location.replace('<?=$arItem["PARAMS"]["warning_link"]; ?>'); return false; "
													  <? if (!empty($arItem["PARAMS"]["warning_title"])) { ?>title="<?=$arItem["PARAMS"]["warning_title"]; ?>"<? } ?>
													  class="menu-post-warn-icon"
													  id="menu-counter-warning-<?=strtolower($arItem["PARAMS"]["counter_id"]); ?>"></span>
											<?endif?>
										<?endif;?>
									</span></a>
								</li>
							<?
							}
						}
					}
					if ($status=="hide"):?>
						</ul>
					</li>
					<?endif;
				}
				?>
			</ul>
			<div class="menu-favorites-more-btn<?if ($disabled):?> menu-items-close<?endif;?>" id="more_btn_<?=$arTitleItem["PARAMS"]["menu_item_id"]?>" <?if (!is_array($arResult["SORT_ITEMS"][$title]["hide"])):?>style="display:none;"<?endif?> onclick="ShowHideMoreItems(this, '<?=CUtil::JSEscape($arTitleItem["PARAMS"]["menu_item_id"])?>');">
				<span class="menu-favorites-more-text"><?=GetMessage("MENU_MORE_ITEMS_SHOW")?></span>
				<?if ($title == "menu-favorites"):?>
					<span class="menu-item-index menu-item-index-more" id="menu-hidden-counter" <?if ($sumHiddenCounters<=0):?>style="display:none"<?endif?>><?=($sumHiddenCounters > 50 ? "50+" : $sumHiddenCounters)?></span>
				<?endif?>
				<span class="menu-favorites-more-icon"></span>
			</div>
			<?if (IsSubItemSelected($arResult["SORT_ITEMS"][$title]["hide"])) $arHiddenItemsSelected[] = $arTitleItem["PARAMS"]["menu_item_id"];?>
		</div>
	<?
	}
}
?>
</div>
<?/* 
<div class="ded">
	<img src="/upload/ded2.png" alt="" />
</div>

<style>
.ded{
	margin-left: -12px;
}
</style> */ ?>
<?

CJSCore::Init();
$APPLICATION->AddHeadScript("/bitrix/js/main/dd.js");
$ajaxPath = CUtil::JSEscape(SITE_TEMPLATE_PATH."/components/bitrix/menu/vertical_multilevel/ajax.php");
?>
<script>
	var arFavouriteAll = <?=CUtil::PhpToJsObject($arResult["ALL_FAVOURITE_ITEMS_ID"])?>;
	var arFavouriteShowAll = <?=CUtil::PhpToJsObject($arResult["ALL_SHOW_FAVOURITE_ITEMS_ID"])?>;
	var arTitles = <?=CUtil::PhpToJsObject(array_keys($arResult["TITLE_ITEMS"]))?>;
	var SiteID = "<?=SITE_ID?>";

	window.B24menuItemsObj = new B24menuItems(
		"<?=$ajaxPath?>",
		"<?=(IsModuleInstalled("bitrix24") && $GLOBALS['USER']->CanDoOperation('bitrix24_config') || !IsModuleInstalled("bitrix24") && $GLOBALS['USER']->IsAdmin()) ? "Y" : "N"?>",
		<?=CUtil::PhpToJsObject($arHiddenItemsCounters)?>,
		<?=CUtil::PhpToJsObject($arAllItemsCounters)?>
	);

	function IsBitrix24()
	{
		return <?if (IsModuleInstalled("bitrix24")):?>true<?else:?>false<?endif?>;
	}

	BX.message({
		add_to_favorite: '<?=CUtil::JSEscape(GetMessage('MENU_ADD_TO_FAVORITE'))?>',
		delete_from_favorite: '<?=CUtil::JSEscape(GetMessage('MENU_DELETE_FROM_FAVORITE'))?>',
		hide_item: '<?=CUtil::JSEscape(GetMessage('MENU_HIDE_ITEM'))?>',
		show_item: '<?=CUtil::JSEscape(GetMessage('MENU_SHOW_ITEM'))?>',
		add_to_favorite_all: '<?=CUtil::JSEscape(GetMessage('MENU_ADD_TO_FAVORITE_ALL'))?>',
		delete_from_favorite_all: '<?=CUtil::JSEscape(GetMessage('MENU_DELETE_FROM_FAVORITE_ALL'))?>',
		more_items_hide: '<?=CUtil::JSEscape(GetMessage('MENU_MORE_ITEMS_HIDE'))?>',
		more_items_show: '<?=CUtil::JSEscape(GetMessage('MENU_MORE_ITEMS_SHOW'))?>',
		edit_error: '<?=CUtil::JSEscape(GetMessage('MENU_ITEM_EDIT_ERROR'))?>',
		set_rights: '<?=CUtil::JSEscape(GetMessage('MENU_ITEM_SET_RIGHTS'))?>'
	});

	BX.ready(function () {

		var menuTitles = BX.findChildren(BX("bx_b24_menu"), {className:"menu-items-title"}, true);
		for(var i=0; i<menuTitles.length; i++)
		{
			if (!BX.hasClass(menuTitles[i], "menu-favorites"))
			{
				BX.bind(menuTitles[i], "click", function(){
					if (!IsEditMode())
						B24.toggleMenu(this, '<?=GetMessageJS("MENU_SHOW")?>', '<?=GetMessageJS("MENU_HIDE")?>');
				});
			}
		}

		var menuToggleObj = BX.findChildren(BX("bx_b24_menu"), {className:"menu-toggle-text"}, true);
		for(var i=0; i<menuToggleObj.length; i++)
		{
			BX.bind(menuToggleObj[i], "click", function(){
				var e = event || window.event;
				e.stopPropagation();
				B24.toggleMenu(this.parentNode, '<?=GetMessageJS("MENU_SHOW")?>', '<?=GetMessageJS("MENU_HIDE")?>');
			});
		}

		//show hidden items, if they are selected
		<?if (!empty($arHiddenItemsSelected)):?>
		<?foreach($arHiddenItemsSelected as $titleID):?>
		ShowHideMoreItems(BX("more_btn_<?=$titleID?>"), '<?=CUtil::JSEscape($titleID)?>');
		<?endforeach?>
		<?endif?>
	});

</script>