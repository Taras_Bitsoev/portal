<?php
class tenderBD {

	private static $dbhost = 'localhost';	// Имя хоста БД
	private static $dbusername = 'report';		// Пользователь БД root
	private static $dbpass = '123qweasd';		// Пароль к базе   123qweasd
	private static $dbname = 'report_ms_portal';// Имя базы
	private static $link = '1';				// Соединение с БД

	//sdfsdfsd

	public function AddTENDER($tender = null)
	{
		pre(BDAddTENDER);
		if($tender===null) return false;
		$dateStart = $this->convert_time_to_sql($tender[DATE_CREATE]);
		$sql = "";
		$sql .= "INSERT INTO `tndr_tender`(`ID`, `NAME`, `DATE_CREATE`, `DATE_START`, `DATE_FINISH`, `DURATION`, `CODE_BITRIX`, `ID_BITRIX`) ";
		$sql .= " VALUES ('', '$tender[NAME]', '$dateStart', '', '', '', '$tender[CODE]', '$tender[ID]') ";
		pre($sql);
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить список таблиц\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		return $tenderID = $this->dbCheckTenderByID($tender['ID']);
	}
	public function AddLOT($tenderID=null, $lot=null)
	{
		if($tenderID===null || $lot===null) return false;
		$turn = $this->getTURN($tenderID);
		$dateStart = $this->convert_time_to_sql($lot['ACTIVE_FROM']);
		$dateEnd = $this->convert_time_to_sql($lot['ACTIVE_TO']);
		$sql  = "INSERT INTO `tndr_lot` (`ID`, `ID_TENDER`, `TURN`, `NAME`, `DATE_START`, `DATE_FINISH`, `CODE_BITRIX`, `ID_BITRIX`)";
		$sql .=	"VALUES ( '', '$tenderID', '$turn', '$lot[NAME]', '$dateStart', '$dateEnd', '$lot[CODE]', '$lot[ID]')";
		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		$lotID = $this->dbCheckLotByID($lot['ID']);
		return $lotID;
	}
	public function UpdateLOT($tenderID=null, $lot=null)
	{
		if($tenderID===null || $lot===null) return false;
		$turn = $this->getTURN($tenderID);
		$dateStart = $this->convert_time_to_sql($lot['ACTIVE_FROM']);
		$dateEnd = $this->convert_time_to_sql($lot['ACTIVE_TO']);
		$sql  = "UPDATE `tndr_lot` ";
		$sql .= " SET `ID_TENDER`= '$tenderID', `NAME`='$lot[NAME]', `DATE_START`='$dateStart', ";
		$sql .= " `DATE_FINISH`='$dateEnd', `CODE_BITRIX`='$lot[CODE]' ";
		$sql .= " WHERE `ID_BITRIX` = '$lot[ID]'";
		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		$lotID = $this->dbCheckLotByID($lot['ID']);
		return $lotID;
	}
	public function dbCheckTenderByCODE($tenderCODE=null)
	{
		if($tenderCODE===null) return false;
		$sql = "SELECT ID FROM tndr_tender WHERE CODE_BITRIX='$tenderCODE' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function dbCheckTenderByID($tenderID=null)
	{
		if($tenderID===null) return false;
		$sql = "SELECT ID FROM tndr_tender WHERE ID_BITRIX='$tenderID' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function dbCheckLotByCODE($lotCODE=null)
	{
		if($lotCODE===null) return false;
		
		$sql = "SELECT ID FROM tndr_lot WHERE CODE_BITRIX='$lotCODE' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function dbCheckLotByID($lotID=null)
	{
		if($lotID===null) return false;
		
		$sql = "SELECT ID FROM tndr_lot WHERE ID_BITRIX='$lotID' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function freeQuery($sql)
	{
		$sql = $sql;
		$query =  mysqli_query(self::$link,($sql));
		if (!$query)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			$Result[] = $arResult;
		}
		return $Result;
	}
	public function Fdbname ()
	{
		return self::$dbname;
	}
	public function dbnameconnect ()
	{
		return self::$link;
	}
	public function format_to_save_string ($text)
	{
		$text = addslashes($text);
		$text= htmlspecialchars ($text);
		$text = addslashes($text);
		$username = preg_replace("/[^А-Яа-яA-Za-z0-9]/i", "", $_GET['username']);
		$text = str_replace("'/\|",'',$text);
		$text = str_replace("\r\n",' ',$text);
		$text = str_replace("\n",' ',$text);
		return $text;
	}
	public function convert_time_to_sql($time) {
		if(!isset($time)) return;
		//in  DD.MM.YYYY HH:MM:SS
		$two_side = explode(' ',$time);
		$left_side = explode('.',$two_side['0']); //  DD.MM.YYYY
		//out YYYY-MM-DD HH:MM:SS
		return $left_side['2'].'-'.$left_side['1'].'-'.$left_side['0'].' '.$two_side['1'];
	}
	public function convert_sql_to_time($sqltime) {
		//in  DD.MM.YYYY HH:MM:SS
		if(!isset($sqltime)) return;
		$two_side = explode(' ', $sqltime);
		$left_side = explode('-',$two_side['0']); //  DD.MM.YYYY
		//out YYYY-MM-DD HH:MM:SS
		return $left_side['2'].'-'.$left_side['1'].'-'.$left_side['0'].' '.$two_side['1'];
	}
	public function take_now_time_MONTH () {
		//MONTH,
		$format = 'm';
		$NOW_MONTH = date($format,time());
		return $NOW_MONTH;
	}
	public function take_now_time_YEAR () {
		//YEAR,
		$format = 'Y';
		$NOW_YEAR = date($format,time());
		return $NOW_YEAR;
	}
	public function __construct()
	{
	//	echo 'Устанавливаем соедениение с базой данных!<br>';
		self::$link = mysqli_connect(self::$dbhost, self::$dbusername, self::$dbpass, self::$dbname);
		if (!self::$link)
		{
			echo "Error: Unable to connect to MySQL." . PHP_EOL;
			echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
		if (!self::$link->set_charset("utf8")) 
		{
			printf("Ошибка при загрузке набора символов utf8: %s\n", $mysqli->error);
		}
	}
	public function __destruct() {
		if(!mysqli_close(self::$link))
		{ 
			echo 'Connect with DB NOT close!<br>';
			echo "Error: Unable to connect to MySQL." . PHP_EOL;
			echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		}
		//echo 'Соедениение с базой данных закрыто!<br>';
	}
	private function getTURN($tender)
	{
		$sql = "SELECT MAX(TURN) as TURN  FROM tndr_lot WHERE ID_TENDER='$tender' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['TURN']+1;
		}
		return false;
	}
}