<?
class serverGroupClass extends staffGroupClass{
	public $NAME;
	public $ID;
	public $fail = array(
			array('min'=>0,   'max'=>20,  'M'=>20,  'C'=>0), // до 20%
			array('min'=>20,  'max'=>50,  'M'=>60,  'C'=>0), // от 20%  до 60%
			array('min'=>50,  'max'=>100, 'M'=>100, 'C'=>0), // от 60%  до 100%
			array('min'=>100, 'max'=>200, 'M'=>200, 'C'=>0), // от 100%  до 200%
			array('min'=>200, 'max'=>300, 'M'=>300, 'C'=>0), // от 200%  до 300%
			array('min'=>300, 'max'=>0,   'M'=>400, 'C'=>0), // от 300% и более
	);

	public $defeat = array(
			array('min'=>0, 'max'=>100, 'M'=>100),
	);
		
	public $price1;
	public $price2;
	public $price3;
	public $price4;
	
	public $countAllElements;
	public $countAllElements1;
	public $countAllElements2;
	public $countAllElements3;
	public $countAllElements4;
	
	public $elementWight = NULL;
	public $elementWight1 = NULL;
	public $elementWight2 = NULL;
	public $elementWight3 = NULL;
	public $elementWight4 = NULL;
	public $complexity = array(1,2,3,4);
	
	
	/* ---------------------------------------- */
	
	public $CountTrueElements = NULL;
	public $CountFailElementsA = NULL;
	public $CountFailElementsB = NULL;
	public $CountFailElementsC = NULL;
	public $CountEpicFailElements = NULL;
	public $CountPostponedEpicFailElements = NULL;
	public $maxMotivation = NULL;
	public $MotivationForTrue = NULL;
	public $MotivationForFail = NULL;
	public $DeMotivation = NULL;
	public $total = NULL;
	public $delta = NULL;
	public $award = NULL;
	public $result = NULL;
	
	/* --------------------------------------- */
	public function __construct($groupID=null){
		parent::__construct();
		if($groupID === null ){
			echo 'Инициализация расчета не возможна! Не указан ID группы!';
			self::__destruct();
		}
		$this->ID = $groupID;
		self::giveOutGroupUsersList();
	}
	public function __destruct(){
		if($this->UsersList !== NULL){
			foreach ($this->UsersList as $user){
				$user->__destruct();
			}
		}
		parent::__destruct();
	}
	
}