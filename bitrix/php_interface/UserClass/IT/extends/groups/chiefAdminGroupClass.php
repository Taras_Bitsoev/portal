<?
class chiefAdminGroupClass extends staffGroupClass{
	public $NAME;
	public $ID;
	public $fail = array(
			array('min'=>0,   'max'=>20,  'M'=>40,  'C'=>0), // до 20%
			array('min'=>20,  'max'=>50,  'M'=>60,  'C'=>0), // от 20%  до 60%
			array('min'=>50,  'max'=>100, 'M'=>100, 'C'=>0), // от 60%  до 100%
			array('min'=>100, 'max'=>200, 'M'=>200, 'C'=>0), // от 100%  до 200%
			array('min'=>200, 'max'=>300, 'M'=>300, 'C'=>0), // от 200%  до 300%
			array('min'=>300, 'max'=>0,   'M'=>400, 'C'=>0), // от 300% и более
	);
	
	public $defeat = array(
			array('min'=>0,'max'=>91,  'M'=>100),
			array('min'=>91,'max'=>96,  'M'=>110),
			array('min'=>96,'max'=>100,  'M'=>125),
	);
	/*public $defeat = array(
			array('min'=>0,'max'=>60,   'M'=>0.00001),
			array('min'=>60,'max'=>75, 'M'=>60),
			array('min'=>75,'max'=>85,  'M'=>70),
			array('min'=>85,'max'=>91,  'M'=>100),
			array('min'=>91,'max'=>96,  'M'=>110),
			array('min'=>96,'max'=>100,  'M'=>125),
	);*/

	public $totalOperateWork=0;
	public $price1;
	public $price2;
	public $price3;
	public $price4;
	
	public $countAllElements;
	public $countAllElements1;
	public $countAllElements2;
	public $countAllElements3;
	public $countAllElements4;
	

	public $elementWight = NULL;
	public $elementWight1 = NULL;
	public $elementWight2 = NULL;
	public $elementWight3 = NULL;
	public $elementWight4 = NULL;
	public $complexity = array(1,2,3,4);
	
	
	/* ---------------------------------------- */
	
	public $CountTrueElements = NULL;
	public $CountFailElementsA = NULL;
	public $CountFailElementsB = NULL;
	public $CountFailElementsC = NULL;
	public $CountEpicFailElements = NULL;
	public $CountPostponedEpicFailElements = NULL;
	public $maxMotivation = NULL;
	public $MotivationForTrue = NULL;
	public $MotivationForFail = NULL;
	public $DeMotivation = NULL;
	public $total = NULL;
	public $delta = NULL;
	public $award = NULL;
	public $result = NULL;
	
	/* --------------------------------------- */
	public function getAllCountelements(){
		foreach($this->staffGrops as $groupe){
			$this->countAllElements +=$groupe->countAllElements;
			$this->CountTrueElements +=$groupe->CountTrueElements;
		}
		return true;
	}
	public function PrintReport(){
		$this->getAllCountelements();
		$atributes_table = 'style="border: 1px solid black; border-collapse: collapse; "'; //width: 100%;
		$atributes_tr = 'style="border: 1px solid black; border-collapse: collapse;"';
		$atributes_td = 'style="border: 1px solid black; border-collapse: collapse;  text-align: center;"';//width: 40px;
		$table = $this->NAME.'<br>';
		$table .= '<table '.$atributes_table.'>';
	
		$table .= $this->printHEADofTABLE();
		//$i = 1;
		foreach ($this->USERLIST as $USER){
			$table .= '<tr '.$atributes_tr.'>';
			$table .= $this->giveUserOutTable($USER);
			$table .= '</tr>';
			++$i;
		}
		$table .= $this->printFOOTERofTABLE();
		$table .= '</table>';
		$table .= '<br/>';
		$table .= '<br/>';
		return $table;
	}
	public function printHEADofTABLE($USER){
		$atributes_table = 'border: 1px solid black; border-collapse: collapse;'; //width: 100%;
		$atributes_tr = 'border: 1px solid black; border-collapse: collapse;';
		$atributes_td = 'border: 1px solid black; border-collapse: collapse; text-align: center;';
	
		$table .= '<tr style="'.$atributes_tr.'">';
		$table .= '<td style=" width: 20px; '.$atributes_td.'">№';
		$table .= '</td>';
		$table .= '<td style=" width:160px; '.$atributes_td.'" >ФИО';
		$table .= '</td>';
		$table .= '<td style=" width: 110px; '.$atributes_td.'">Должность';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Операционная работа';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Проектная работа';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Всего заявок';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Выполнено вовремя';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'"> Факт выполнения %';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'"> Коэффициент';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">За операционную деятельность';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Выполнения проектных работ %';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">За проектную деятельность';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Итог';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Отклонение';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Премия';
		$table .= '</td>';
		$table .= '<td style=" width: 80px; '.$atributes_td.'">Результат';
		$table .= '</td>';
		$table .= '</tr>';
		
		return $table;
	}
	public function giveUserOutTable($USER){
		$atributes_table = 'border: 1px solid black; border-collapse: collapse;'; //width: 100%;
		$atributes_tr = 'border: 1px solid black; border-collapse: collapse;';
		$atributes_td = 'border: 1px solid black; border-collapse: collapse; text-align: center;';
		
		$table .= '<tr style="'.$atributes_tr.'">';
		$table .= '<td style="'.$atributes_td.'" >№';
		$table .= '</td>';
		
		$table .= '<td style="'.$atributes_td.'" >';
		$user = $USER->User[LAST_NAME].' '.$USER->User[NAME];
		$table .= $user;
		$table .= '</td>';
		
		$table .= '<td style="'.$atributes_td.'">Руководитель группы сис.адм.';
		$table .= '</td>';
		
		$table .= '<td style="'.$atributes_td.'">';
		if(!isset($_REQUEST['print']))
		{
			$content = '<input type="text" style="width: 80px;"  name="g_'.$USER->group.'_op_'.$USER->User[ID].'" value="'.$USER->operateWork.'">';
		} else {
			$content = $USER->operateWork;
		}
		$table .= $content;
		$table .= '</td>';
		
		$table .= '<td style="'.$atributes_td.'">';
		if(!isset($_REQUEST['print']))
		{
			$content = '<input type="text" style="width: 80px;"  name="g_'.$USER->group.'_pr_'.$USER->User[ID].'" value="'.$USER->projectWork.'">';
		} else {
			$content = $USER->projectWork;
		}
		$table .= $content;
		$table .= '</td>';
		
		$table .= '<td style="'.$atributes_td.'" >';
		$table .= $this->countAllElements;
		$table .= '</td>';
		
		$table .= '<td style="'.$atributes_td.'" >';
		$table .= $this->CountTrueElements;
		$table .= '</td>';
		
		$this->mathOperationWork($USER);
		
		$table .= '<td style="'.$atributes_td.'" >';
		$total_persent = round($this->CountTrueElements/$this->countAllElements*100, 2);
		$table .= $total_persent;
		$table .= '</td>';
		
		$table .= '<td style="'.$atributes_td.'" >';
		$total_persent = round($USER->level, 2);
		$table .= $total_persent;
		$table .= '</td>';
		
		
		$table .= '<td style="'.$atributes_td.'" >';
		$operation = round($this->totalOperateWork, 2);
		$table .= $operation;
		$table .= '</td>';
		
		//факт по проектной работе
		$table .= '<td style="'.$atributes_td.'" >';
		if(!isset($_REQUEST['print']))
		{
			$content = '<input type="text" style="width: 80px;"  name="g_'.$USER->group.'_prf_'.$USER->User[ID].'" value="'.$USER->projectWorkfakt.'">';
		} else {
			$content = $USER->projectWorkfakt;
		}
		$table .= $content;
		$table .= '</td>';

		$table .= '<td style="'.$atributes_td.'" >';
		$projectale = round($USER->projectWork*$USER->projectWorkfakt/100, 2);
		$table .= $projectale;
		$table .= '</td>';
		
		
		$table .= '<td style="'.$atributes_td.'" >';
		$totalby = $operation + $projectale;
		$table .= $totalby;
		$table .= '</td>';
		
		// отклонение
		$table .= '<td style="'.$atributes_td.'" >';
		$USER->maxMotiv = $USER->projectWork + $USER->operateWork;
		$delta = $USER->maxMotiv - $totalby;
		$table .= $delta;
		$table .= '</td>';
		$table .= '<td style="'.$atributes_td.'" >';
		if(!isset($_REQUEST['print'])){
			$content = '<input type="text" style="width: 80px;" ';
			$content .= 'name="g_'.$USER->group.'_p_'.$USER->User[ID].'" value="'.$USER->award.'">';
		} else {
			$content = $USER->award;
		}
		$table .= $content;
		$table .= '</td>';
		$table .= '<td style="'.$atributes_td.'" >';
		$res = $totalby + $USER->award;
		$this->result = $res;
		$table .= $res;
		$table .= '</td>';
		$table .= '</tr>';
		return $table;
	}
	public function printFOOTERofTABLE(){
		return false;
	}
	public function __construct($groupID=null){
		if($groupID === null ){
			echo 'Инициализация расчета не возможна! Не указан ID группы!';
			self::__destruct();
		}
		parent::__construct();
		$this->ID = $groupID;
		self::giveOutGroupUsersList();
	}
	public function __destruct(){
		if($this->UsersList !== NULL){
			foreach ($this->UsersList as $user){
				$user->__destruct();
			}
		}
		parent::__destruct();
	}
	
	public function mathOperationWork($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		
		$preTotal = round(($this->CountTrueElements/$this->countAllElements*100), 2);
		$t = $USER->operateWork;
		foreach ($this->defeat as $level){
			if($level['min'] < $preTotal && $preTotal <= $level['max']){
				$USER->level = ($level['M']*1/100);
				$tt = $t * $level['M']/100;
			}
		}
	
		if($t <=0 ){
			$this->totalOperateWork = 0;
		}else{
			$this->totalOperateWork = $tt;
		}
		return true;
	}
}