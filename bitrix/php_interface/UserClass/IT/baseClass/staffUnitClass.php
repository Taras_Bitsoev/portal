<?
class staffUnitClass
{
	public $UserID = null;
	public $group = null;
	public $User = null;
	public $award = 0;
	public $operateWork=0;
	public $projectWork=0;
	public $projectWorkfakt=0;
	
	
	//  Расчитываемые свойства  //
	public $total = 0;
	public $totalLev=0;
	public $delta = 0;
	public $result = 0;
	
	public $maxMotivation = 0;
	public $maxMotivation1 = 0;
	public $maxMotivation2 = 0;
	public $maxMotivation3 = 0;
	public $maxMotivation4 = 0;
	
	public $MotivationForTrue = 0;
	public $MotivationForTrue1 = 0;
	public $MotivationForTrue2 = 0;
	public $MotivationForTrue3 = 0;
	public $MotivationForTrue4 = 0;
	
	public $MotivationForFail = 0;
	public $MotivationForFail1 = 0;
	public $MotivationForFail2 = 0;
	public $MotivationForFail3 = 0;
	public $MotivationForFail4 = 0;
	
	public $DeMotivation = 0;
	public $DeMotivation1 = 0;
	public $DeMotivation2 = 0;
	public $DeMotivation3 = 0;
	public $DeMotivation4 = 0;
	
	//  Количествнные свойства, получаем от "Группы"  //	
	public $CountAllElements = 0;
	public $CountAllElements1 = 0;
	public $CountAllElements2 = 0;
	public $CountAllElements3 = 0;
	public $CountAllElements4 = 0;

	public $CountTrueElements = 0;
	public $CountTrueElements1 = 0;
	public $CountTrueElements2 = 0;
	public $CountTrueElements3 = 0;
	public $CountTrueElements4 = 0;

	public $CountFailElements = 0;
	public $CountFailElements1 = 0;
	public $CountFailElements2 = 0;
	public $CountFailElements3 = 0;
	public $CountFailElements4 = 0;

		
	public $CountAllEpicFailElements = 0;
	
	public $CountEpicFailElements = 0;
	public $CountEpicFailElements1 = 0;
	public $CountEpicFailElements2 = 0;
	public $CountEpicFailElements3 = 0;
	public $CountEpicFailElements4 = 0;
	
	public $CountPostponedEpicFailElements = 0;
	public $CountPostponedEpicFailElements1 = 0;
	public $CountPostponedEpicFailElements2 = 0;
	public $CountPostponedEpicFailElements3 = 0;
	public $CountPostponedEpicFailElements4 = 0;

	//  Внешние данные, получаем от "Группы"  //
	
	public $priceComplexity1 = 0;
	public $priceComplexity2 = 0;
	public $priceComplexity3 = 0;
	public $priceComplexity4 = 0;
	
	public $price1 = 0;
	public $price2 = 0;
	public $price3 = 0;
	public $price4 = 0;
	
	public $level = 0;
	
	private static $complexity1 = 1;
	private static $complexity2 = 2;
	private static $complexity3 = 3;
	private static $complexity4 = 4;
	
	// System method
	public function __construct($User = null)
	{	
		if($User === null){
			echo 'Инициализация расчета не возможна! Пользователь не определен!';
			self::__destruct();
		}
		$this->User = $User;
		if(true === !empty($User['ID'])){
			$this->UserID = $User['ID'];
		}
	}
	public function __destruct(){
	}
	
	// Private methods
}