<?
class staffDepartamentClass {
	public $NAME;
	public $GROUPS_ID;
	public $MODEL;
	public $GROUPS;
	public $UNITS;
	public $HTMLResult;
	public $result = NULL;
	public $fact = NULL;

	//Public method
	public function getAcssesToGrope (array $GroupsID = array()){
		if(count($GroupsID)<1) return false;
		
		
	}
	
	public function solveResult(){
		if(!is_object($this->MODEL)){
			return false;
		}
		$result = $this->giveOutForm($this->MODEL);
		echo $result;
	}
	public function giveOutForm($MODEL){
		if(!is_object($MODEL)){return false;}
		$atributes_table = 'style="border: 1px solid black; border-collapse: collapse; "'; //width: 100%;
		$atributes_tr = 'style="border: 1px solid black; border-collapse: collapse;"';
		$atributes_td = 'style="border: 1px solid black; border-collapse: collapse; text-align: center;"';//width: 40px;
		
		$form = '<form charset="UTF-8" name="motivation_report" method="post" action="/table/d_reports/report.php" style="width:2600px;">';
		foreach ($this->GROUPS as $group)
		{
			$group->fillProperty($MODEL);
			$form .= $group->PrintReport();
			$this->result += $group->result;
			$form .= '<br>';
			$form .= '<br>';
		}
		$form .= '<br>';
		$form .= '<br>';
		$form .= '<table '.$atributes_table.'>';
		$form .= '<tr '.$atributes_tr.'>';
		$form .= '<td rowspan="2" '.$atributes_td.'>';
		$form .= 'ИТОГ';
		$form .= '</td>';
		$form .= '<td '.$atributes_td.'>';
		$form .= 'ПЛАН';
		$form .= '</td>';
		$form .= '<td '.$atributes_td.'>';
		$form .= 'ФАКТ';
		$form .= '</td>';
		$form .= '<td '.$atributes_td.'>';
		$form .= 'ОТКЛОНЕНИЕ';
		$form .= '</td>';
		$form .= '</tr>';
		$form .= '<tr '.$atributes_tr.'>';
		$form .= '<td '.$atributes_td.'>';
		$form .= $this->fact;
		$form .= '</td>';
		$form .= '<td '.$atributes_td.'>';
		$form .= round($this->result);
		$form .= '</td>';
		$form .= '<td '.$atributes_td.'>';
		$form .= round($this->fact - $this->result);
		$form .= '</td>';
		$form .= '</tr>';
		$form .= '</table>';
		$form .= '<br>';
		$form .= '<br>';
		$form .= '<input name="math" value="Пересчитать" type="submit">';
		//$form .= '<input name="save" value="Сохранить" type="submit">';
		$form .= '<input type="submit" name="print" value="Для печати">';
		$form .='<input type="hidden" value="clear_cache=Y">';
		$form .='<input type="hidden" name="IN_COMPANY_TITLE" value="'.$MODEL->getDepartament().'">';
		$form .='<input type="hidden" name="MONTH" value="'.$MODEL->getMonth().'">';
		$form .='<input type="hidden" name="YEAR" value="'.$MODEL->getYear().'">';
		$form .= '</form>';
		return $form;
	}
	
	public function GetModel($start, $end){
		$this->MODEL = self::initiateModel($start, $end, $this->NAME);
		return true;
	}
	public function name(){
		return $this->NAME;
	}
	public function givMessagesToGroup(array $QUEST){
		foreach ($QUEST as $key => $value){
			self::processingKey($key, $value);
		}
		return true;
	}
	
	// System method
	public function __construct(){
		
	}
	public function __destruct(){
		//$MODEL->_destruct();
	}

	private function initiateModel($start = null, $end = null, $name = null){
		if($start === null || $end === null || $name === null) return false;
		$MODEL = new CompanyReport('periods', $start, $end, $name);
		return $MODEL;
	}
	private function processingKey ($key = NULL, $value){
		if($key == NULL){return false;}
		if($key == 'save'){
			$this->save = true;
		}
		if($key == 'max'){
			$this->maxMotiv = $value;
			$this->fact += $value;
		}
		/*if($key == 'award'){
			$this->award_res = $value;
		}*/
	
		$t = explode('_', $key);
		$z = count($t);
		if($z == 2 && $t['0'] == 'g' ){
			$this->GROUPS[$t['1']]->RISE=$value;
			$this->fact += $value;
		}
		if($z == 4 && $t['0'] == 'g' && $t['2'] == 'p'){
			$this->GROUPS[$t['1']]->USERLIST[$t['3']]->award=$value;
		}
		if($z == 4 && $t['0'] == 'g' && $t['2'] == 'op'){
			$this->GROUPS[$t['1']]->USERLIST[$t['3']]->operateWork=$value;
			$this->fact += $value;
		}
		if($z == 4 && $t['0'] == 'g' && $t['2'] == 'pr'){
			$this->GROUPS[$t['1']]->USERLIST[$t['3']]->projectWork=$value;
			$this->fact += $value;
		}
		if($z == 4 && $t['0'] == 'g' && $t['2'] == 'prf'){
			$this->GROUPS[$t['1']]->USERLIST[$t['3']]->projectWorkfakt=$value;
		}
		return true;
	}
}