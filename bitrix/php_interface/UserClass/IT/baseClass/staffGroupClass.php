<?class staffGroupClass{
	public $NAME;
	public $ID;
	public $RISE;
	public $USERLIST;
	public $MODEL;
	public $totalLev;
	public $staffGrops;
	
	public $price1 = 0;
	public $price2 = 0;
	public $price3 = 0;
	public $price4 = 0;
	
	public $countAllElements;
	public $countAllElements1;
	public $countAllElements2;
	public $countAllElements3;
	public $countAllElements4;
	
	public $elementWight = NULL;
	public $elementWight1 = NULL;
	public $elementWight2 = NULL;
	public $elementWight3 = NULL;
	public $elementWight4 = NULL;
	
	public $CountTrueElements = NULL;
	public $CountFailElementsA = NULL;
	public $CountFailElementsB = NULL;
	public $CountFailElementsC = NULL;
	public $CountEpicFailElements = NULL;
	public $CountPostponedEpicFailElements = NULL;
	public $maxMotivation = NULL;
	public $MotivationForTrue = NULL;
	public $MotivationForFail = NULL;
	public $DeMotivation = NULL;
	public $total = NULL;
	public $delta = NULL;
	public $award = NULL;
	public $result = NULL;
	
	public $fail = array(
		array('min'=>0,'max'=>50,   'M'=>0,   'C'=>0),   // до 50%
		array('min'=>50,'max'=>100, 'M'=>25,  'C'=>0), // от 50%  до 100%
		array('min'=>100,'max'=>0,  'M'=>100, 'C'=>0), // от 100% и более
	);
	public $defeat = array(
		array('min'=>0,'max'=>100, 'M'=>100),   
	);
	public $pofesional = array(
		'383' =>'Оператор call-центра',
		'384' =>'Системный администратор',
		'450' =>'Системный администратор',
		'449' =>'Программист 1С',
		'453' =>'Программист 1С',
		'454' =>'Программист 1С',
		'451' =>'Программист PHP',
		'455' =>'Программист PHP'
	);
	public $CountFailTotal = null;
	

	public function PrintReport(){
		

		
		
		$atributes_table = 'style="border: 1px solid black; border-collapse: collapse; "'; //width: 100%;
		$atributes_tr = 'style="border: 1px solid black; border-collapse: collapse;"';
		$atributes_td = 'style="border: 1px solid black; border-collapse: collapse;  text-align: center;"';//width: 40px;
		$table = $this->NAME.'<br>';
		$table .= '<table '.$atributes_table.'>';
		
		$table .= $this->printHEADofTABLE();
		$i = 1;
		foreach ($this->USERLIST as $USER){
			$table .= '<tr '.$atributes_tr.'>';
			$table .= '<td '.$atributes_td.'>';
			$table .= $i;
			$table .= '</td>';
			$table .= $this->giveUserOutTable($USER);
			$table .= '</tr>';
			++$i;
		}
		$table .= $this->printFOOTERofTABLE();
		$table .= '</table>';
		$table .= '<br/>';
		$table .= '<br/>';
		return $table;
	}
	public function printHEADofTABLE(){
		$atributes_table = 'style="border: 1px solid black; border-collapse: collapse; "'; //width: 100%;
		$atributes_tr = 'style="border: 1px solid black; border-collapse: collapse;"';
		$atributes_td = 'style="border: 1px solid black; border-collapse: collapse;  text-align: center;"';//width: 40px;
		$table .= '<tr '.$atributes_tr.'>';
			$table .= '<td '.$atributes_td.'> № </td>';
			$table .= '<td '.$atributes_td.'> ФИО </td>';
			$table .= '<td '.$atributes_td.'> Должность </td>';
			$table .= '<td '.$atributes_td.'> Сложность </td>';
			$table .= '<td '.$atributes_td.'> Всего заявок </td>';
			$table .= '<td '.$atributes_td.'> Выполнено вовремя </td>';
			foreach ($this->fail as $limit){
				if($limit['max'] <> 0){
					$table .= '<td '.$atributes_td.'> от'.$limit['min'].'% '.$limit['max'].'% </td>';
				} else {
					$table .= '<td '.$atributes_td.'> более '.$limit['min'].'% </td>';
				}
			}
			$table .= '<td '.$atributes_td.'> Не сделанно </td>';
			$table .= '<td '.$atributes_td.'> Отложенно </td>';
			$table .= '<td '.$atributes_td.'> Стоимость заявки </td>';
			$table .= '<td '.$atributes_td.'> Мах. мотивация </td>';
			$table .= '<td '.$atributes_td.'> За сделанные </td>';
			$table .= '<td '.$atributes_td.'> За просроченные </td>';
			$table .= '<td '.$atributes_td.'> Штраф за не сделанные </td>';
			$table .= '<td '.$atributes_td.'> Фактически </td>';
			$table .= '<td '.$atributes_td.'> Коэффициент </td>';
			$table .= '<td '.$atributes_td.'> Итог </td>';
			$table .= '<td '.$atributes_td.'> Отклонение </td>';
			$table .= '<td '.$atributes_td.'> Премия </td>';
			$table .= '<td '.$atributes_td.'> Результат </td>';
		$table .= '</tr>';
		return $table;
	}
	public function printFOOTERofTABLE(){
		$atributes_table = 'style="border: 1px solid black; border-collapse: collapse; "'; //width: 100%;
		$atributes_tr = 'style="border: 1px solid black; border-collapse: collapse;"';
		$atributes_td = 'style="border: 1px solid black; border-collapse: collapse;  text-align: center;"';//width: 40px;
		$table .= '<tr '.$atributes_tr.'>';
		$table .= '<td '.$atributes_td.'></td>';
		//Всего
		$table .= '<td '.$atributes_td.'>';
		$table .= 'Всего: ';
		$table .= '</td>';

		$table .= '<td '.$atributes_td.'></td>';
		
		$table .= '<td '.$atributes_td.'></td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= $this->countAllElements;
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= $this->CountTrueElements;
		$table .= '</td>';

		foreach ($this->fail as $limit){
			$table .= '<td '.$atributes_td.'>';
			$content = '<div>'.$limit['C'].'</div>';
			$table .= $content;
			$table .= '</td>';
		}

		$table .= '<td '.$atributes_td.'>';
		$table .= $this->CountEpicFailElements;
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= $this->CountPostponedEpicFailElements;
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		if(!isset($_REQUEST['print']))
		{
			$content = '<input type="text" style="width: 80px;" name="g_'.$this->ID.'" value="'.$this->RISE.'">';
		} else {
			$content = $this->RISE;
		}
		$table .= $content;
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= round($this->maxMotivation);
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= round($this->MotivationForTrue);
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= round($this->MotivationForFail);
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= round($this->DeMotivation);
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= round($this->totalLev);
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'> ';
		//$table .= round($this->DeMotivation);
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= round($this->total);
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'">';
		$table .= round($this->delta);
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= $this->award;
		$table .= '</td>';
		
		$table .= '<td '.$atributes_td.'>';
		$table .= round($this->result);
		$table .= '</td>';
		
		$table .= '</tr>';
		
		return $table;
	}
	public function giveUserOutTable($USER){
		$atributes_td = 'style="border: 1px solid black; border-collapse: collapse;  text-align: center;"';//width: 40px;
		//ФИО
		
		$table .= '<td '.$atributes_td.'>';
		$user = $USER->User[LAST_NAME].' '.$USER->User[NAME];
		$table .= $user;
		$table .= '</td>';
		
		//Должность
		$table .= '<td '.$atributes_td.'>';
		$staff_position = $this->pofesional[$this->ID];
		$table .= $staff_position;
		$table .= '</td>';
	
		//Сложность
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>1</div><div>2</div><div>3</div><div>4</div>';
		$table .= $content;
		$table .= '</td>';
	
		//Всего заявок
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		$USER->CountAllElements1.'</div><div>'.
		$USER->CountAllElements2.'</div><div>'.
		$USER->CountAllElements3.'</div><div>'.
		$USER->CountAllElements4.'</div>';
		$table .= $content;
		$table .= '</td>';
	
		//Выполненно вовремя
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		$USER->CountTrueElements1.'</div><div>'.
		$USER->CountTrueElements2.'</div><div>'.
		$USER->CountTrueElements3.'</div><div>'.
		$USER->CountTrueElements4.'</div>';
		$table .= $content;
		$table .= '</td>';

		foreach ($this->fail as $key =>$limit){
			$a = 0;
			$b = 0;
			$c = 0;
			$d = 0;
			$table .= '<td '.$atributes_td.'>';
			if($limit['max'] <> 0){
				foreach ($USER->CountFailElements1 as $el1){
					if($limit['min']< $el1 &&  $el1<=$limit['max']){$a++;}
				}
				foreach ($USER->CountFailElements2 as $el2){
					if($limit['min']< $el2 &&  $el2<=$limit['max']){$b++;}
				}
				foreach ($USER->CountFailElements3 as $el3){
					if($limit['min']< $el3 &&  $el3<=$limit['max']){$c++;}
				}
				foreach ($USER->CountFailElements4 as $el4){
					if($limit['min']< $el4 &&  $el4<=$limit['max']){$d++;}
				}
				$e = $this->fail[$key]['C'];
				$this->fail[$key]['C'] = $a+$b+$c+$d+$e;
				$content = '<div>'.$a.'</div><div>'.$b.'</div><div>'.$c.'</div><div>'.$d.'</div>';
				$table .= $content;	
			} else {
				foreach ($USER->CountFailElements1 as $el1){
					if($limit['min']< $el1){$a++;}
				}
				foreach ($USER->CountFailElements2 as $el2){
					if($limit['min']< $el2){$b++;}
				}
				foreach ($USER->CountFailElements3 as $el3){
					if($limit['min']< $el3){$c++;}
				}
				foreach ($USER->CountFailElements4 as $el4){
					if($limit['min']< $el4){$d++;}
				}
				$e = $this->fail[$key]['C'];
				$this->fail[$key]['C'] = $a+$b+$c+$d+$e;
				$content = '<div>'.$a.'</div><div>'.$b.'</div><div>'.$c.'</div><div>'.$d.'</div>';
				$table .= $content;
			}
			$table .= '</td>';
		}

		//Не сделанно
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		$USER->CountEpicFailElements1.'</div><div>'.
		$USER->CountEpicFailElements2.'</div><div>'.
		$USER->CountEpicFailElements3.'</div><div>'.
		$USER->CountEpicFailElements4.'</div>';
		$table .= $content;
		$table .= '</td>';
	
		//Отложенно
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		$USER->CountPostponedEpicFailElements1.'</div><div>'.
		$USER->CountPostponedEpicFailElements2.'</div><div>'.
		$USER->CountPostponedEpicFailElements3.'</div><div>'.
		$USER->CountPostponedEpicFailElements4.'</div>';
		$table .= $content;
		$table .= '</td>';
	
		//Стоимость заявки
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		$USER->price1.'</div><div>'.
		$USER->price2.'</div><div>'.
		$USER->price3.'</div><div>'.
		$USER->price4.'</div>';
		$table .= $content;
		$table .= '</td>';
	
		//Максимально возможная мотивация
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		round($USER->maxMotivation1).'</div><div>'.
		round($USER->maxMotivation2).'</div><div>'.
		round($USER->maxMotivation3).'</div><div>'.
		round($USER->maxMotivation4).'</div>';
		$table .= $content;
		$table .= '</td>';
	
		//Сделанные в срок
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		round($USER->MotivationForTrue1).'</div><div>'.
		round($USER->MotivationForTrue2).'</div><div>'.
		round($USER->MotivationForTrue3).'</div><div>'.
		round($USER->MotivationForTrue4).'</div>';
		$table .= $content;
		$table .= '</td>';
		//За просроченные
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		round($USER->MotivationForFail1).'</div><div>'.
		round($USER->MotivationForFail2).'</div><div>'.
		round($USER->MotivationForFail3).'</div><div>'.
		round($USER->MotivationForFail4).'</div>';
		$table .= $content;
		$table .= '</td>';
		//За не сделанные
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.
		round($USER->DeMotivation1).'</div><div>'.
		round($USER->DeMotivation2).'</div><div>'.
		round($USER->DeMotivation3).'</div><div>'.
		round($USER->DeMotivation4).'</div>';
		$table .= $content;
		$table .= '</td>';
		
		//Промежуточный итог
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.round($USER->totalLev).'</div>';
		$table .= $content;
		$table .= '</td>';
		
		//Выполнение
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.round($USER->level, 2).'</div>';
		$table .= $content;
		$table .= '</td>';
	
		//Промежуточный итог
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.round($USER->total).'</div>';
		$table .= $content;
		$table .= '</td>';
		
		//Отклонение
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.round($USER->delta).'</div>';
		$table .= $content;
		$table .= '</td>';
		//Премия
		$table .= '<td '.$atributes_td.'>';
		if(!isset($_REQUEST['print'])){
			$content = '<input type="text" style="width: 80px;" ';
			$content .= 'name="g_'.$USER->group.'_p_'.$USER->User[ID].'" value="'.$USER->award.'">';
		} else {
			$content = $USER->award;
		}
		$table .= $content;
		$table .= '</td>';
	
		//Результат
		$table .= '<td '.$atributes_td.'>';
		$content = '<div>'.round($USER->result).'</div>';
		$table .= $content;
		$table .= '</td>';
	
		return $table;
	}
	public function fillProperty($MODEL){
		if(!($MODEL instanceof CompanyReport)){return false;}
		$this->MODEL = $MODEL;
		foreach($this->USERLIST as $USER){
			$this->getUnitStatistik($USER);
		}
		$this->mathElement($complexity);
		foreach($this->USERLIST as $USER){
			$this->giveOutElementPrice($USER);
			$this->math($USER);
			$this->getResultStatistick($USER);
		}
	}
	public function giveOutElementPrice($USER){
		$this->getPriceElements($USER, $this->price1, 1);
		$this->getPriceElements($USER, $this->price2, 2);
		$this->getPriceElements($USER, $this->price3, 3);
		$this->getPriceElements($USER, $this->price4, 4);
		return true;
	}
	public function giveOutGroupUsersList(){
		if(empty($this->USERLIST)){
			self::fillUnits();
		}
		return $this->USERLIST;
	}
	public function __construct(){
	}
	public function __destruct() {
		if($this->USERLIST !== NULL){
			foreach ($this->USERLIST as $user){
				$user->__destruct();
			}
		}
	}
	
	private function getResultStatistick($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$totals = $this->giveOutTotals($USER);
		$this->CountTrueElements += $totals['CountTrueElements'];
		$this->CountFailElementsA += $totals['CountFailElementsA'];
		$this->CountFailElementsB += $totals['CountFailElementsB'];
		$this->CountFailElementsC += $totals['CountFailElementsC'];
		$this->CountEpicFailElements += $totals['CountEpicFailElements'];
		$this->CountPostponedEpicFailElements += $totals['CountPostponedEpicFailElements'];
		$this->maxMotivation += $totals['maxMotivation'];
		$this->MotivationForTrue += $totals['MotivationForTrue'];
		$this->MotivationForFail += $totals['MotivationForFail'];
		$this->DeMotivation += $totals['deMotivation'];
		$this->totalLev += $totals['totalLev'];
		$this->total += $totals['total'];
		$this->delta += $totals['delta'];
		$this->award += $totals['award'];
		$this->result += $totals['result'];
		return true;
	}
	private function giveOutTotals($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$totals = array(
				'CountTrueElements' => $USER->CountTrueElements,
				'CountFailElementsA' => $USER->CountFailElementsA,
				'CountFailElementsB' => $USER->CountFailElementsB,
				'CountFailElementsC' => $USER->CountFailElementsC,
				'CountEpicFailElements' => $USER->CountEpicFailElements,
				'CountPostponedEpicFailElements' => $USER->CountPostponedEpicFailElements,
				'maxMotivation' => $USER->maxMotivation,
				'MotivationForTrue' => $USER->MotivationForTrue,
				'MotivationForFail' => $USER->MotivationForFail,
				'deMotivation' => $USER->DeMotivation,
				'totalLev' => $USER->totalLev,
				'total' => $USER->total,
				'delta' => $USER->delta,
				'award' => $USER->award,
				'result' => $USER->result
		);
		return $totals;
	}
	private function fillUnits(){
		/*
		 *  480, 479 - Дубровин, Байгузин.
		 *  1149 - Редько
		 * 
		 *  Нужно добавить в  класс Департамент ИТ
		 *  определение ИТ директора и руководителя 
		 *  группа системного администрирования
		 */
		
		$filter = Array("ACTIVE" => "Y", "GROUPS_ID" => Array($this->ID));
		$dbUsers = CUser::GetList(($by="id"), ($order="asc"), $filter);
		while($arU = $dbUsers->Fetch()){
			if ($this->ID == 383 || $this->ID == 384){
				if($arU['ID'] != 479 && $arU['ID'] != 1149){
					$unit = new staffUnitClass($arU);
					$unit->group = $this->ID;
					$this->USERLIST[$arU['ID']] = $unit;
					
				}
			} else {
				$unit = new staffUnitClass($arU);
				$unit->group = $this->ID;
				$this->USERLIST[$arU['ID']] = $unit;
			}
		}
		return true;
	}
	private function getUnitStatistik(staffUnitClass $USER){
		if($this->MODEL instanceof CompanyReport && $USER instanceof staffUnitClass){
			$MODEL = $this->MODEL;
			for ($complexity = 1 ;$complexity < 5; $complexity++){
				$this->getCountAllElements($USER, $MODEL, $complexity);
				$this->getCountTrueElements($USER, $MODEL, $complexity);	
				$this->getCountFailElements($USER ,$MODEL, $complexity);
				$this->getCountEpicFailElements($USER, $MODEL, $complexity);
				$this->getCountPostponedEpicFailElements($USER, $MODEL, $complexity);
				$this->getAllCountElement($USER, $complexity);
			}
		}
		return true;
	}
	private function giveOutCountElements(staffUnitClass $USER, $complexity=0){
		if(!($USER instanceof staffUnitClass)){return false;}
		switch ($complexity){
			case 0 :
				return $USER->CountAllElements;
				break;
			case 1 :
				return $USER->CountAllElements1;
				break;
			case 2 :
				return $USER->CountAllElements2;
				break;
			case 3 :
				return $USER->CountAllElements3;
				break;
			case 4 :
				return $USER->CountAllElements4;
				break;
		}
	}
	private function getCountTrueElements(staffUnitClass $USER, $MODEL, $complexity = 2){
		if(!($USER instanceof staffUnitClass)){return false;}
		if(!($MODEL instanceof CompanyReport)){return false;}
		$CountTrueTicket = $MODEL->getInTimeCountTicket($USER->UserID, $complexity, c);
		$CountTrueTask = $MODEL->getInTimeCountTask($USER->UserID, $complexity, c);
		$CountTrueElements = $CountTrueTask + $CountTrueTicket;
		switch ($complexity){
			case 1 :
				$USER->CountTrueElements1 += $CountTrueElements;
				break;
			case 2 :
				$USER->CountTrueElements2 += $CountTrueElements;
				break;
			case 3 :
				$USER->CountTrueElements3 += $CountTrueElements;
				break;
			case 4 :
				$USER->CountTrueElements4 += $CountTrueElements;
				break;
		}
		$USER->CountTrueElements = $USER->CountTrueElements1+$USER->CountTrueElements2
			+$USER->CountTrueElements3+$USER->CountTrueElements4;
		return true;
	}
	private function getCountFailElements(staffUnitClass $USER, $MODEL, $complexity = 2){
		if(!($USER instanceof staffUnitClass)){return false;}
		if(!($MODEL instanceof CompanyReport)){return false;}
		switch ($complexity){
			case 1 :
				$CountFail1 = array();
				$CountFailTicket1 = array();
				$CountFailTask1  = array();
				$userID = $USER->UserID;
				$CountFailTicket1 = $MODEL->FailTicketReport($userID,$complexity, c);
				$CountFailTask1 = $MODEL->FailTaskReport($userID,$complexity, c);
				
				$CountFail1 = array_merge($CountFailTicket1, $CountFailTask1);
				
				$a1 = array();
				if(!is_array($USER->CountFailElements1) || empty($USER->CountFailElements1)){
					$a1 = array();
				}else{
					$a1 = $USER->CountFailElements1;
				}
				$USER->CountFailElements1 =  array_merge($a1, $CountFail1);
				break;
			case 2 :
				$CountFail2 = array();
				$CountFailTicket2 = array();
				$CountFailTask2  = array();
				
				$CountFailTicket2 = $MODEL->FailTicketReport($USER->UserID, '2', c);
				$CountFailTask2 = $MODEL->FailTaskReport($USER->UserID, '2', c);
				
				$CountFail2 = array_merge($CountFailTicket2, $CountFailTask2);
				
				$a2 = array();
				if(!is_array($USER->CountFailElements2) || empty($USER->CountFailElements2)){
					$a2 = array();
				}else{
					$a2 = $USER->CountFailElements2;
				}
				$USER->CountFailElements2 =  array_merge($a2, $CountFail2);
				break;
			case 3 :
				$CountFail3 = array();
				$CountFailTicket3 = array();
				$CountFailTask3  = array();
				
				$CountFailTicket3 = $MODEL->FailTicketReport($USER->UserID, '3', c);
				$CountFailTask3 = $MODEL->FailTaskReport($USER->UserID, '3', c);
				
				$CountFail3 = array_merge($CountFailTicket3, $CountFailTask3);
				
				$a3 = array();
				if(!is_array($USER->CountFailElements3) || empty($USER->CountFailElements3)){
					$a3 = array();
				}else{
					$a3 = $USER->CountFailElements3;
				}
				$USER->CountFailElements3 =  array_merge($a3, $CountFail3);
				break;
			case 4 :
				$CountFail4 = array();
				$CountFailTicket4 = array();
				$CountFailTask4  = array();
				
				$CountFailTicket4 = $MODEL->FailTicketReport($USER->UserID, '4', c);
				$CountFailTask4 = $MODEL->FailTaskReport($USER->UserID, '4', c);
				
				$CountFail4 = array_merge($CountFailTicket4, $CountFailTask4);
				
				$a4 = array();
				if(!is_array($USER->CountFailElements4) || empty($USER->CountFailElements4)){
					$a4 = array();
				}else{
					$a4 = $USER->CountFailElements4;
				}
				$USER->CountFailElements4 =  array_merge($a4, $CountFail4);
				break;
		}
		
		$USER->CountFailElements = count($USER->CountFailElements1)+count($USER->CountFailElements2)
			+count($USER->CountFailElements3)+count($USER->CountFailElements4);
		if($this->CountFailTotal === null){
			$total = array();
		//	echo '1';
		}else {
			$total = $this->CountFailTotal;
		}
		$this->CountFailTotal = array_merge($total, $USER->CountFailElements1, $USER->CountFailElements2, $USER->CountFailElements3, $USER->CountFailElements4);
		//echo 'UserID:'.$USER->UserID.' : '.count($this->CountFailTotal).'<br>';
		return true;
	}
	private function getCountEpicFailElements(staffUnitClass $USER, $MODEL, $complexity = 2){
		if(!($USER instanceof staffUnitClass)){return false;}
		if(!($MODEL instanceof CompanyReport)){return false;}
		$CountEpicFailTask = $MODEL->getEpicFailCountTask($USER->UserID, $complexity, c);
		$CountEpicFailTicket = $MODEL->getEpicFailCountTicket($USER->UserID, $complexity, c);
		$CountEpicFailElements = $CountEpicFailTask + $CountEpicFailTicket;
		switch ($complexity){
			case 1 :
				$USER->CountEpicFailElements1 += $CountEpicFailElements;
				break;
			case 2 :
				$USER->CountEpicFailElements2 += $CountEpicFailElements;
				break;
			case 3 :
				$USER->CountEpicFailElements3 += $CountEpicFailElements;
				break;
			case 4 :
				$USER->CountEpicFailElements4 += $CountEpicFailElements;
				break;
		}
		$USER->CountEpicFailElements = $USER->CountEpicFailElements1+$USER->CountEpicFailElements2
									+$USER->CountEpicFailElements3+$USER->CountEpicFailElements4;
		return true;
	}
	private function getCountPostponedEpicFailElements(staffUnitClass $USER, $MODEL, $complexity = 2){
		if(!($USER instanceof staffUnitClass)){return false;}
		if(!($MODEL instanceof CompanyReport)){return false;}
		$CountPostponedEpicFailTicket = $MODEL->getPostponedEpicFailCountTicket($USER->UserID, $complexity, c);
		$CountPostponedEpicFailTask = $MODEL->getPostponedEpicFailCountTask($USER->UserID, $complexity, c);
		$CountPostponedEpicFailElements = $CountPostponedEpicFailTicket + $CountPostponedEpicFailTask;
		switch ($complexity){
			case 1 :
				$USER->CountPostponedEpicFailElements1 += $CountPostponedEpicFailElements;
				break;
			case 2 :
				$USER->CountPostponedEpicFailElements2 += $CountPostponedEpicFailElements;
				break;
			case 3 :
				$USER->CountPostponedEpicFailElements3 += $CountPostponedEpicFailElements;
				break;
			case 4 :
				$USER->CountPostponedEpicFailElements4 += $CountPostponedEpicFailElements;
				break;
		}
		$USER->CountPostponedEpicFailElements = $USER->CountPostponedEpicFailElements1+$USER->CountPostponedEpicFailElements2
											+$USER->CountPostponedEpicFailElements3+$USER->CountPostponedEpicFailElements4;
		return true;
	}
	private function getCountAllElements(staffUnitClass $USER, $MODEL, $complexity = 2){
		if(!($USER instanceof staffUnitClass)){return false;}
		if(!($MODEL instanceof CompanyReport)){return false;}
		
		$CountAllTicket = $MODEL->getALLCountTicket($USER->UserID, $complexity, c);
		$CountAllTask = $MODEL->getALLCountTask($USER->UserID, $complexity, c);
		$CountAllElements = $CountAllTicket + $CountAllTask;
		switch ($complexity){
			case 1 :
				$USER->CountAllElements1 += $CountAllElements;
				break;
			case 2 :
				$USER->CountAllElements2 += $CountAllElements;
				break;
			case 3 :
				$USER->CountAllElements3 += $CountAllElements;
				break;
			case 4 :
				$USER->CountAllElements4 += $CountAllElements;
				break;
		}
		$USER->CountAllElements = $USER->CountAllElements1+$USER->CountAllElements2
		+$USER->CountAllElements3+$USER->CountAllElements4;
		return true;
	}
	
	private function getPriceElements(staffUnitClass $USER, $price=null, $complexity = 2){
		if(!($USER instanceof staffUnitClass)){return false;}
		if($price < 0 || $price == NULL){
			return false;
		}
		switch ($complexity){
			case 1 :
				$USER->priceComplexity1 =  $price;
				$USER->price1  = round($price, 2);
				break;
			case 2 :
				$USER->priceComplexity2 = $price;
				$USER->price2  = round($price, 2);
				break;
			case 3 :
				$USER->priceComplexity3 = $price;
				$USER->price3  = round($price, 2);
				break;
			case 4 :
				$USER->priceComplexity4 = $price;
				$USER->price4  = round($price, 2);
				break;
		}
		return true;
	}
	private function getAllCountElement($USER, $complexity){
		if(!($USER instanceof staffUnitClass)){return false;}
		switch ($complexity){
			case 1 :
				$this->countAllElements1 += $USER->CountAllElements1;
				break;
			case 2 :
				$this->countAllElements2 += $USER->CountAllElements2;
				break;
			case 3 :
				$this->countAllElements3 += $USER->CountAllElements3;
				break;
			case 4 :
				$this->countAllElements4 += $USER->CountAllElements4;
				break;
		}
		$this->countAllElements = $this->countAllElements1 + $this->countAllElements2 
			+ $this->countAllElements3 + $this->countAllElements4;
		return true;
	}
	private function mathElement(){
		for ($complexity = 1; $complexity < 5; $complexity++){
			$this->mathComplexityElementWight($complexity);
		}
		$this->elementWight = $this->elementWight1 + $this->elementWight2 + $this->elementWight3 + $this->elementWight4;
		for ($complexity = 1; $complexity < 5; $complexity++){	
			$this->mathComplexityPriceElements($complexity);
		}
		return true;
	}
	private function mathComplexityElementWight($complexity){
		switch ($complexity){
			case 1 :
				$this->elementWight1 = $complexity / 10 * $this->countAllElements1;
				break;
			case 2 :
				$this->elementWight2 = $complexity / 10 * $this->countAllElements2;
				break;
			case 3 :
				$this->elementWight3 = $complexity / 10 * $this->countAllElements3;
				break; 
			case 4 :
				$this->elementWight4 = $complexity / 10 * $this->countAllElements4;
				break;
		}
		return true;
	}
	
	private function mathComplexityPriceElements($complexity){
		switch ($complexity){
			case 1 :
				$this->price1 = $this->RISE / $this->elementWight * $complexity/10;
				//echo  $this->ID.' -> '.round($this->price1, 3).'<br>';
				break;
			case 2 :
				$this->price2 = $this->RISE / $this->elementWight * $complexity/10;
				//echo $this->ID.' -> '.round($this->price2, 3).'<br>';
				break;
			case 3 :
				$this->price3 = $this->RISE / $this->elementWight * $complexity/10;
				//echo $this->ID.' -> '.round($this->price3, 3).'<br>';
				break;
			case 4 :
				$this->price4 = $this->RISE / $this->elementWight * $complexity/10;
				//echo $this->ID.' -> '.round($this->price4, 3).'<br>';
				break;
		}
		return true;
	}
	private function math($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$this->mathMaxMotivation($USER);
		$this->mathMotivationForTrue($USER);
		$this->mathMotivationForFail($USER);
		$this->mathDeMotivation($USER);
		$this->mathTotal($USER);
		$this->mathDelta($USER);
		$this->mathResult($USER);
	}
	
	
	
	
	private function mathResult($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$USER->result = $USER->total + $USER->award;
	}
	private function mathDelta($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$USER->delta = $USER->maxMotivation - $USER->total;
	}
	private function mathTotal($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$preTotal = round(($USER->CountTrueElements*100/$USER->CountAllElements), 2);
		$t = $USER->MotivationForTrue + $USER->MotivationForFail + $USER->DeMotivation;
		foreach ($this->defeat as $level){
			if($level['min'] < $preTotal && $preTotal <= $level['max']){
				$USER->level = ($level['M']*1/100);
				$tt = $t * $level['M']/100;
			}
		}
		
		if($t < 0 ){
			$USER->total = 0;
			$USER->totalLev = 0;
		}else{
			$USER->total = $tt;
			$USER->totalLev = $t;
		}
		return true;
	}
	private function mathDeMotivation($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$USER->DeMotivation1 = ( $USER->CountEpicFailElements1 + $USER->CountPostponedEpicFailElements1 ) * $USER->priceComplexity1 * -1;
		$USER->DeMotivation2 = ( $USER->CountEpicFailElements2 + $USER->CountPostponedEpicFailElements2 ) * $USER->priceComplexity2 * -1;
		$USER->DeMotivation3 = ( $USER->CountEpicFailElements3 + $USER->CountPostponedEpicFailElements3 ) * $USER->priceComplexity3 * -1;
		$USER->DeMotivation4 = ( $USER->CountEpicFailElements4 + $USER->CountPostponedEpicFailElements4 ) * $USER->priceComplexity4 * -1;
		$USER->DeMotivation = $USER->DeMotivation1 + $USER->DeMotivation2 + $USER->DeMotivation3 + $USER->DeMotivation4;
		return true;
	}

	private function mathMotivationForFail($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		foreach ($USER->CountFailElements1 as $q){
			foreach ($this->fail as $limit){
				if($limit['max'] <> 0){
					if($limit['min']< $q &&  $q<=$limit['max']){
						$USER->MotivationForFail1 += $USER->priceComplexity1 - ($USER->priceComplexity1 * $limit['M']/100);
					}
				} else {
					if($limit['min']< $q){
						$USER->MotivationForFail1 += $USER->priceComplexity1 - ($USER->priceComplexity1 * $limit['M']/100);
					}
				}
			}
		}
		foreach ($USER->CountFailElements2 as $q){
			foreach ($this->fail as $limit){
				if($limit['max'] <> 0){
					if($limit['min']< $q &&  $q<=$limit['max']){
						$USER->MotivationForFail2 += $USER->priceComplexity2 - ($USER->priceComplexity2 * $limit['M']/100);
					}
				} else {
					if($limit['min']< $q){
						$USER->MotivationForFail2 += $USER->priceComplexity2 - ($USER->priceComplexity2 * $limit['M']/100);
					}
				}
			}
		}
		foreach ($USER->CountFailElements3 as $q){
			foreach ($this->fail as $limit){
				if($limit['max'] <> 0){
					if($limit['min']< $q &&  $q<=$limit['max']){
						$USER->MotivationForFail3 += $USER->priceComplexity3 - ($USER->priceComplexity3 * $limit['M']/100);
					}
				} else {
					if($limit['min']< $q){
						$USER->MotivationForFail3 += $USER->priceComplexity3 - ($USER->priceComplexity3 * $limit['M']/100);
					}
				}
			}
		}
		foreach ($USER->CountFailElements4 as $q){
			foreach ($this->fail as $limit){
				if($limit['max'] <> 0){
					if($limit['min']< $q &&  $q<=$limit['max']){
						$USER->MotivationForFail4 += $USER->priceComplexity4 - ($USER->priceComplexity4 * $limit['M']/100);
					}
				} else {
					if($limit['min']< $q){
						$USER->MotivationForFail4 += $USER->priceComplexity4 - ($USER->priceComplexity4 * $limit['M']/100);
					}
				}
			}
		}

		$USER->MotivationForFail = $USER->MotivationForFail1 + $USER->MotivationForFail2
		+ $USER->MotivationForFail3 + $USER->MotivationForFail4;
		return true;
	}
	private function mathMotivationForTrue($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$USER->MotivationForTrue1 = $USER->CountTrueElements1 * $USER->priceComplexity1;
		$USER->MotivationForTrue2 = $USER->CountTrueElements2 * $USER->priceComplexity2;
		$USER->MotivationForTrue3 = $USER->CountTrueElements3 * $USER->priceComplexity3;
		$USER->MotivationForTrue4 = $USER->CountTrueElements4 * $USER->priceComplexity4;
		$USER->MotivationForTrue = $USER->MotivationForTrue1 + $USER->MotivationForTrue2
		+ $USER->MotivationForTrue3 + $USER->MotivationForTrue4;
		return true;
	}
	private function mathMaxMotivation($USER){
		if(!($USER instanceof staffUnitClass)){return false;}
		$USER->maxMotivation1 = $USER->CountAllElements1 * $USER->priceComplexity1;
		$USER->maxMotivation2 = $USER->CountAllElements2 * $USER->priceComplexity2;
		$USER->maxMotivation3 = $USER->CountAllElements3 * $USER->priceComplexity3;
		$USER->maxMotivation4 = $USER->CountAllElements4 * $USER->priceComplexity4;
		$USER->maxMotivation = $USER->maxMotivation1 + $USER->maxMotivation2 + $USER->maxMotivation3 + $USER->maxMotivation4;
		return true;
	}
}