<?
class UserClassWorkTime
{

	# Класс предназначен для определения дедлайна заявки исходя из количества
	# рабочих часов в дне, неделе, месяце.
	# Не рабочие часы не учитываются как продуктивные в срок выполнения заявки.
	#
	# ЛОГИКА РАБОТЫ КЛАССА:
	#
	# п.1 Получаем текущую дату и время.
	# п.2 Определяем текущий час, день, месяц подачи заявки для определения
	#     времени старта исполнения Заявки.
	# п.3 Проверяем рабочий ли текущий час подачи заявки.
	# п.4 Проверяем рабочий ли текущий день подачи заявки.
	# п.5 Если п.3 и п.4 "ДА", то сл. п.5
	#     Если п.3 и п.4 "НЕТ", то сл. п.6
	#
	#     п.5.1 Определяем количество оставшихся рабочих часов до конца
	#			текущего рабочего дня.
	#	  п.5.1.1 Если оставшего рабочего времени больше чем объем SLA, то
	#             устанавливаем Дедлайн заявки.
	#     п.5.1.2 Если оставшего рабочего времени меньше чем объем SLA, то
	#             "откусываем" его от SLA и переходим к п.6
	# п.6 Определяем ближайший следующий рабочий день и переходим к п.5.1
	#
	# ВХОДЯЩИЕ ДАННЫЕ:
	#	SLA - целое число в часах.
	#
	# ОПИСАНИЕ ПЕРЕМЕННЫХ:
	#
	# $holidays - массив праздничных дней в году согласно КЗоТ
	# $weekends - выходные дни недели
	# $freeDay  - нерабочий день
	# $SLA      - объем рабочих часов для выполнения Заявки
	# $curTime  - текущее время инициализации объекта, timestamp

	private static $holidays = array(
		'01-01',	# 1 января — Новый год;
		'01-07',	# 7 января — Рождество Христово;
		'03-08',	# 8 марта — Международный женский день;
		'05-01',	# 1 и 2 мая — День международной солидарности трудящихся;
		'05-02',
		'05-09',	# 9 мая — День Победы;
		'06-28',	# 28 июня — День Конституции Украины;
		'08-24', 	# 24 августа — День Независимости Украины;
		'10-14'		# 14 октября - День защитника Украины;
	);
	private static $weekends = array(0, 6);	# 0 - Воскресенье, 6 - Суббота
	private $SLA = null;
	private $curTime=null;
	private $freeDay = null;


	/*********************************** Интерфейсы **************************************/
	
	/**
	 * Расчитывает дедлайн по 24 часовому графику 7 дней в неделю
	 *
	 * @param int $SLA
	 * return date 'd.m.Y H:i:s'
	 */
	public function getDeadlineDate_24h_to_7d ()
	{
		$SLA = $this->SLA;
		$startTime = $this->curTime;
		$timestamp = self::solveDeadline_24h_to_5d ($startTime, $SLA);
		$sekSLA = $SLA*60*60;
		$timestamp = $startTime + $sekSLA;
		$date = date('d.m.Y H:i:s', $timestamp);
		return $date;
	}
	

	/**
	 * Расчитывает дедлайн по 24 часовому графику 5 дней в неделю
	 *
	 * @param int $SLA
	 * return date 'd.m.Y H:i:s'
	 */
	public function getDeadlineDate_24h_to_5d ()
	{
		$SLA = $this->SLA;
		$startTime = $this->curTime;
		$timestamp = self::solveDeadline_24h_to_5d ($startTime, $SLA);
		$date = date('d.m.Y H:i:s', $timestamp);
		return $date;
	}

	
	
	/**
	* Расчитывает дедлайн по 8 часовому графику 7 дней в неделю
	*
	* @param int $SLA
	* return date 'd.m.Y H:i:s'
	*/
	public function getDeadlineDate_8h_to_7d ()
	{
		$SLA = $this->SLA;
		$startTime = $this->curTime;
		$timestamp = self::solveDeadline_8h_to_7d ($startTime, $SLA);
		$date = date('d.m.Y H:i:s', $timestamp);
		return $date;
	}


	/**
	* Расчитывает дедлайн по 8 часовому графику 5 дней в неделю
	* @param timestamp $startTime
	* @param int $SLA
	* return date 'd.m.Y H:i:s'
	*/
	public function getDeadlineDate_8h_to_5d ()
	{
		$SLA = $this->SLA;
		$startTime = $this->curTime;
		$timestamp = self::solveDeadline_8h_to_5d ($startTime, $SLA);
		$date = date( 'd.m.Y H:i:s', $timestamp);
		return $date;
	}
	
	
	/**
	 * Расчитывает дедлайн по 24 часовому графику 5 дней в неделю
	 * @param timestamp $startTime
	 * @param int $SLA
	 * @return timestamp
	 */
	private function solveDeadline_24h_to_5d ($startTime, $SLA)
	{
		$secSLA = $SLA*60*60;
		$deadline = null;
		$curentDay = $startTime;
		while ($deadline == null)
		{
			$iswday = self::verificationWorkDay($curentDay);
			if($iswday == true)
			{
				$workTime = self::solveResidueWorkHour($curentDay, 24);
				if($secSLA > $workTime)
				{
					$secSLA = $secSLA - $workTime;
					$curentDay = self::solveNextDay($curentDay);
				}
				else
				{
					$deadline = $curentDay+$secSLA;
				}
			}
			else
			{
				$curentDay = self::solveNextDay($curentDay);
			}
		}
		return $deadline;
	}
	

	/**
	* Расчитывает дедлайн по 8 часовому графику 7 дней в неделю
	* @param timestamp $startTime
	* @param int $SLA
	* @return timestamp
	*/
	private function solveDeadline_8h_to_7d ($startTime, $SLA)
	{
		$secSLA = $SLA*60*60;
		$deadline = null;
		$curentDay = $startTime;
	
		while ($deadline == null)
		{
			$workTime = self::solveResidueWorkHour($curentDay);
			if($secSLA > $workTime)
			{
				$secSLA = $secSLA - $workTime;
				$curentDay = self::solveNextDay($curentDay);
			}
			else
			{
				$deadline = self::solveDeadline ($secSLA, $curentDay);
			}
		}
		return $deadline;
	}


	/**
	* Расчитывает дедлайн по 8 часовому графику 5 дней в неделю
	* @param timestamp $startTime
	* @param int $SLA
	* @return timestamp
	*/
	private function solveDeadline_8h_to_5d ($startTime, $SLA)
	{
		$secSLA = $SLA*60*60;
		$deadline = null;
		$curentDay = $startTime;
		while ($deadline == null)
		{
			$iswday = self::verificationWorkDay($curentDay);
			if($iswday == true)
			{
				$workTime = self::solveResidueWorkHour($curentDay);
				if($secSLA > $workTime)
				{
					$secSLA = $secSLA - $workTime;
					$curentDay = self::solveNextDay($curentDay);
				}
				else
				{
					$deadline = self::solveDeadline ($secSLA, $curentDay);
				}
			}
			else
			{
				$curentDay = self::solveNextDay($curentDay);
			}
		}
		return $deadline;
	}


	/**
	* Определяет количество рабочего времени до конца рабочего дня;
	*
	* @param int, timestamp
	* @return timestamp - юникспредставление дедлайна
	*
	*/
	private function solveDeadline ($residueTimeSLA, $curTime, $limit=false)
	{
		$ThatHour = self::getThatHour($curTime);
		$ThatMinute = self::getDayOfWeek($curTime);
		$ThatSecond = self::getThatSecond($curTime);
		$workTime = self::solveResidueWorkHour($curTime);
		$h = 24 - $ThatHour;
		
		if($limit === false)
		{
			if($h > 17 ) # ранее 8.00 утра
			{
				if($residueTimeSLA < 14400)
				{
					$zerroLine = self::solveZerroLine ($curTime, 8);
					$deadline = $zerroLine + $residueTimeSLA;
					return $deadline;
				}
				else
				{
					$zerroLine = self::solveZerroLine ($curTime, 8);
					$deadline = $zerroLine + $residueTimeSLA + 3600;
					return $deadline;
				}
			}
			elseif( 17 > $h && $h > 12) # c 8.00 до 12.00 дня
			{
				$zerroLine =  self::solveZerroLine ($curTime, 12);
				$deltetime = $zerroLine - $curTime;
				if($deltetime > $residueTimeSLA) //укладываемся до 12.00 дня
				{
					$deadline = $curTime + $residueTimeSLA;
				}
				else // не укладываемся до 12.00 дня
				{
					$deadline = $curTime + $residueTimeSLA+3600;
				}
					return $deadline;
			}
			elseif( 12 > $h && $h > 11) # c 12.00 до 13.00 дня
			{
				$zerroLine = self::solveZerroLine ($curTime, 13);
				$deadline = $zerroLine + $residueTimeSLA;
				return $deadline;
			}
			else  # после 13.00 дня
			{
				$deadline = $curTime + $residueTimeSLA;
				return $deadline;
			}
		}
	}


	/**
	* Определяет количество рабочего времени до конца рабочего дня;
	*
	* @param string
	* @return int -количество секунд
	*
	*/
	private function solveResidueWorkHour ($curTime, $limit = false)
	{
		$ThatHour = self::getThatHour($curTime);
		$ThatMinute = self::getThatMinute($curTime);
		$h = 24 - $ThatHour;
		if($limit === false) 
		{
			if($h > 17) # ранее 8.00 утра
			{
				return 28800;
			}
			elseif( 17 >= $h && $h > 12) # c 8.00 до 12.00 дня
			{
				$wh = 14400; // 4h
				$reserv = 12*60*60; //12h - 43200;
				$deduct = ($ThatHour*60*60) + ($ThatMinute*60);
				$result = ($reserv - $deduct) + $wh;
				return $result;
			}
			elseif( 12 >= $h && $h > 11) # c 12.00 до 13.00 дня
			{
				return 14400;
			}
			elseif (11 >= $h && $h > 7) # с 13.00 до 17.00 дня
			{
				$reserv =  17*60*60;
				$deduct = ($ThatHour*60*60) + ($ThatMinute*60);
				$result = $reserv - $deduct;
				return $result;
			}
			elseif ($h <= 7) # позже 17.00
			{
				return 0;
			}
		}
		if($limit == '24')
		{
			$reserv =  24*60*60;
			$deduct = ($ThatHour*60*60) + ($ThatMinute*60);
			$result = $reserv - $deduct;
			return $result;
		}
	}



	/**
	* Определяет начало следующих суток;
	*
	* @param int timestamp
	* @return int timestamp
	*
	*/
	private function solveNextDay ($curTime)
	{
		$ThatHour = self::getThatHour($curTime);
		$ThatMinute = self::getThatMinute($curTime);
		$ThatSecond = self::getThatSecond($curTime);
		$staticDay = 86400;

		$i = ($ThatHour*60*60)+($ThatMinute*60)+$ThatSecond;
	  	$deltaDay = $staticDay - $i;
	  	$day = $curTime + $deltaDay+10;
		return $day;
	}

	/**
	* Определяет начало следующих суток;
	*
	* @param int timestamp
	* @return int timestamp
	*
	*/
	private function solveZerroLine ($curTime, $hour)
	{
		$ThatHour = self::getThatHour($curTime);
		$ThatMinute = self::getThatMinute($curTime);
		$ThatSecond = self::getThatSecond($curTime);

		$h = $hour*60*60;

		$i = ($ThatHour*60*60)+($ThatMinute*60)+$ThatSecond;
		$day = $curTime - $i + $h;

		return $day;
	}


	/**
	* Определяет рабочий ли это день;
	*
	* @param string
	* @return boolean
	*
	*/
	private function verificationWorkDay ($curTime)
	{
		$tempHol = self::verificationHoliday($curTime);
		$tempWeek = self::verificationWeekend($curTime);

		if($tempHol === true  && $tempWeek === true)
		{
			$this->freeDay += 1;
			return false;
		}
			elseif ($tempHol === true  || $tempWeek === true)
		{
			return false;
		}
		elseif ($tempHol === false  && $tempWeek === false && $this->freeDay > 0)
		{
			$this->freeDay -= 1;
			return false;
		}
		else
		{
			 return true;
		}
	}


	/*  -----------------  Выполняем статические проверки -------------------  */


	/**
	* Определяет праздничный ли это день;
	*
	* @param string
	* @return boolean
	*
	*/
	private function verificationHoliday ($curTime)
	{
		$day = date('m-d', $curTime);
		return in_array($day, self::$holidays, FALSE);
	}


	/**
	* Определяет выходной ли это день;
	*
	* @param string
	* @return boolean
	*
	*/
	private function verificationWeekend ($curTime)
	{
		$DayOfWeek = date('w', $curTime);
		return in_array($DayOfWeek, self::$weekends, FALSE);
	}


	/*  -----------------  Получаем входящие данные -------------------  */

	/**
	* Определяем текущий день недели по порядку;
	*
	* @param timestamp $curTime текущее время создания объекта.
	* @return int Порядковый номер дня недели
	* от 0 (воскресенье) до 6 (суббота)
	*/
	private function getDayOfWeek ($curTime)
	{
		return date('w', $curTime);
	}


	/**
	* Определяем текущий час;
	*
	* @param timestamp $curTime текущее время создания объекта.
	* @return int Декущий час суток в 24-часовом формате с ведущим нулём
	*
	*/
	private function getThatHour ($curTime)
	{
		return date('H', $curTime);
	}

	/**
	* Определяем текущую минуту;
	*
	* @param timestamp $curTime текущее время создания объекта.
	* @return int Декущая минута суток с ведущим нулём
	*
	*/
	private function getThatMinute ($curTime)
	{
		return date('i', $curTime);
	}

	/**
	* Определяем текущую секунду;
	*
	* @param timestamp текущее время создания объекта.
	* @return int Декущая секунда с ведущим нулём
	*
	*/
	private function getThatSecond ($curTime)
	{
		return date('s', $curTime);
	}


	/**
	* Проверяет переданный SLA и записываем во внутренюю приватную переменную;
	*
	* @param string $SLA объем рабочих часов.
	* @return timestamp
	*
	*/
	private function getSLA ($s)
	{

		if ($s !== null && !is_int($s))
		{
			echo 'SLA передан не корректно: "'.$s.'"';
			self::__destruct();
		}
		else
		{
			$this->SLA = $s;
		}
	}

	/**
	 * 
	 * @param timestamp $curTime
	 * @return date
	 */
	private function convDate($curTime)
	{
		return date('d.m.Y H:i:s', $curTime);
	}



	/* -------------------------  Служебные методы объекта/класса  -------------------------- */

	/**
	*
	* @param int $sla - количество целых рабочих часов
	* @param timestamp $startDate - дата старта, если нужно.
	*/
	public function __construct($sla=null, $startDate = null)
	{
		if($startDate != null)
		{
			$this->curTime = $startDate;
		}
		else
		{
			$this->curTime = time();
		}

		if($sla === null)
		{
		echo 'SLA не указанно! Дальнейшая работа не возможна!';
			self::__destruct();
		}
		else
		{
			self::getSLA($sla);
		}
	}

	public function __destruct()
	{

	}
}
?>