<?
class reportForCompani {
	
	//private  static $useDB;
	private  $table = 0;
	private  $start = 0;
	private  $end = 0;
	private  $month = 0;
	private  $year = 0;
	private  $departament = 0;
	private  $departament_link = 0;
	

	public function getDepartamentWorker ($grup_ID)
	{
		$filter = Array("ACTIVE" => "Y", "GROUPS_ID" => Array($grup_ID));
		$dbUsers = CUser::GetList(($by="id"), ($order="desc"), $filter);
		while($arU = $dbUsers->Fetch())
		{
			$arUser[] = $arU['ID'];
		}
		return $arUser;
	}
	
	public function getAPercentageOfDelay($dateCreate, $deadline, $finishTime){
		$deadline =  $this->convertToUnixTime($deadline);
		$finishTime =  $this->convertToUnixTime($finishTime);
		$dateCreate =  $this->convertToUnixTime($dateCreate);
		$q = (($finishTime - $deadline)*100)/($deadline - $dateCreate);
		return  round($q, 3);
	}
	
	public function getDistinktWorker ()
	{
		$sql = " SELECT DISTINCT REGION_DIR_DEV FROM `periods` ";
		$result = mysql_query($sql);
	
		if (!$result) {
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		$arResult = array();
	
		while ($row = mysql_fetch_assoc($result))
		{
			if($row['REGION_DIR_DEV'] <> 0)
				$arResult[] = $row['REGION_DIR_DEV'];
		}
		return $arResult;
	}
	
	public function getUnitedRow ($personal_id, $grayd, $type)
	{
		$all 		= $this->getAllCountTicket($personal_id, $grayd, $type) + $this->getALLCountTask($personal_id, $grayd, $type);
		$inTime     = $this->getInTimeCountTicket($personal_id, $grayd, $type) + $this->getInTimeCountTask($personal_id, $grayd, $type);
		$inEpicFail = $this->getEpicFailCountTicket($personal_id, $grayd, $type) + $this->getEpicFailCountTask($personal_id, $grayd, $type);
		
		$inFailTask = $this->getFailCountTask($personal_id, $grayd, $type);
		$inFailTiket= $this->getFailCountTicket($personal_id, $grayd, $type);
		
		$a = $inFailTiket['a'] + $inFailTask['a'];
		$b = $inFailTiket['b'] + $inFailTask['b'];
		$c = $inFailTiket['c'] + $inFailTask['c'];		
		
	
		echo '<td>'.$grayd.'</td><td>'.$all.'</td><td class="green">'.$inTime.'</td>';
		echo '<td class="yellow" >'.$a.'</td><td class="yellow-dark">'.$b.'</td><td class="red">'.$c.'</td>';
		echo '<td class="ddd">'.$inEpicFail.'</td></tr>';
		return;
	}
	
	public function getRowTicket ($personal_id, $grayd)
	{
		$all 		= $this->getAllCountTicket($personal_id, $grayd);
		$inTime     = $this->getInTimeCountTicket($personal_id, $grayd);
		$inFail     = $this->getFailCountTicket($personal_id, $grayd);
		$inEpicFail = $this->getEpicFailCountTicket($personal_id, $grayd);
		$inPEpicFail = $this->getPostponedEpicFailCountTicket($personal_id, $grayd);
		
		echo '<td>'.$grayd.'</td>'.$all.$inTime.$inFail.$inEpicFail.$inPEpicFail.'</tr>';
		return; 
	}
	
	public function getRowTask ($personal_id, $grayd)
	{
		$all 		= $this->getALLCountTask($personal_id, $grayd);
		$inTime     = $this->getInTimeCountTask($personal_id, $grayd);
		$inFail     = $this->getFailCountTask($personal_id, $grayd);
		$inEpicFail = $this->getEpicFailCountTask($personal_id, $grayd);
		$inPEpicFail = $this->getPostponedEpicFailCountTask($personal_id, $grayd);
		
		echo  '<td>'.$grayd.'</td>'.$all.$inTime.$inFail.$inEpicFail.$inPEpicFail.'</tr>';
		return;
	}
		
	public function showTable($arResult)
	{
		$i=0;
		?><table style='border: 1px solid black;'><tr  style='border: 1px solid black; text-align:center;'>
			<td style='border: 1px solid black;text-align:center;'>№ п/п</td><?
			foreach($arResult['0'] as $key => $value)
			{
			?><th style='border: 1px solid black; text-align:center;'><?
				echo $key;
			?></th><?
			}
			?></tr><?
			foreach($arResult as $value)
			{
				$i++;
			?><tr  style='border: 1px solid black;text-align:center;'>
				<td style='border: 1px solid black;text-align:center;'><?=$i;?></td><?
				foreach ($value as $el)
				{
				?><td style='border: 1px solid black;text-align:center;'><?
					echo $el;
				?></td><?
				}
			?></tr><?
			}
		?></table><?
	}
	
/**************************************************************************************************/
	
	public function getALLTaskDetail ($personal_id,  $grayd = 'null', $main = 'null')
	{
		//echo $personal_id;
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR, TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID ';
	
		//Выполнена
		// Пишем условия выборки
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND ( ";
		
		$rule .= " ( ";
			
		// просроченно
		$rule .= " `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_CLOSED_DATE` BETWEEN $start AND $end ";
		$rule .= " AND `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < `TASK_CLOSED_DATE` ";
		
		$rule .= " ) ";
		
		$rule .= " OR ";
		
		$rule .= " ( ";
		
		// не выполненно
		$rule .= " `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE`< $end ";
		$rule .= " AND ( (`TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` > $end ) ";
		$rule .= " OR `TASK_CLOSED_DATE` = '0000-00-00 00:00:00' ) ";
		
		$rule .= " ) ";
		
		$rule .= " OR ";
		
		$rule .= " ( ";
			
		//сделанно
		$rule .= " `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_CLOSED_DATE` BETWEEN $start AND $end ";
		$rule .= " AND (  ( `TASK_DEADLINE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` < `TASK_DEADLINE` ) ";
		$rule .= " OR  `TASK_DEADLINE` = '0000-00-00 00:00:00' )";
		
		$rule .= " ) )";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$arResult[]=$row;
		}
		
		return $arResult;
	}
	
	public function getALLTicketDetail ($personal_id, $grayd = 'null', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
	
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, TYPE, REGION_DIR_DEV, TICKET_ID, TICKET_ID, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_ID` > '0' ";
		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND ( ";
		
		$rule .= " ( ";
		// просроченно
		$rule .= " `FINISH_TIME` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` BETWEEN $start AND $end ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` <  `FINISH_TIME` ";
		$rule .= " ) ";
			
		$rule .= " OR ";
			
		$rule .= " ( ";
		// не выполненно
		$rule .= " `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND  `DEADLINE` < $end ";
		$rule .= " AND ( ( `FINISH_TIME` <> '0000-00-00 00:00:00' AND `FINISH_TIME` > $end ) ";
		$rule .= " OR `FINISH_TIME` = '0000-00-00 00:00:00' ) ";
		$rule .= " ) ";
			
		$rule .= " OR ";
			
		$rule .= " ( ";
		
		// сделанно вовремя 1
		$rule .= " `FINISH_TIME` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` BETWEEN $start AND $end ";
		$rule .= " AND ( ( `DEADLINE` <> '0000-00-00 00:00:00' AND `FINISH_TIME` < `DEADLINE` ) ";
		$rule .= " OR `DEADLINE` = '0000-00-00 00:00:00' ) ";
		
		$rule .= " )  )";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		//echo $sql.'<br>';
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$arResult[]=$row;
		}
		
		return $arResult;
	}
	
	/*************************************** FAIL ****************************************************/
	
	public function getFailTicketDetail ($personal_id, $grayd = 'null', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
	
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, `TYPE`, REGION_DIR_DEV, TICKET_ID, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_ID` > '0' ";
		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND `FINISH_TIME` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` BETWEEN $start AND $end";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` < `TASK_CLOSED_DATE` ";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$q = $this->getAPercentageOfDelay($row['DATE_CREATE'], $row['DEADLINE'], $row['FINISH_TIME']);
				
			if ($q <= 50)
			{
				$res['a'][] = $row;
			}
			elseif ($q >50 && $q <= 100)
			{
				$res['b'][] = $row;
			}
			else
			{
				$res['c'][] = $row;
			}
	
		}
		
		return $res;
	}
	
	public function getFailTaskDetail ($personal_id,  $grayd = 'null', $type = 's', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR,  TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID ';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}
		
		$rule .= " AND `TASK_CREATED_DATE` < $end ";
		$rule .= " AND `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_CLOSED_DATE` BETWEEN $start AND $end ";
		$rule .= " AND `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < `TASK_CLOSED_DATE` ";
			
		$sql = "SELECT $field  FROM $table WHERE $rule";
	
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$q = $this->getAPercentageOfDelay($row['TASK_CREATED_DATE'], $row['TASK_DEADLINE'], $row['TASK_CLOSED_DATE']);
				
			if ($q <= 50)
			{
				$res['a'][] = $row;
			}
			elseif ($q >50 && $q <= 100)
			{
				$res['b'][] = $row;
			}
			else
			{
				$res['c'][] = $row;
			}
	
		}
		
		return $res;
		
	}

	/*************************************** EPIC FAIL ******************************************************/
	
	public function getEpicFailTicketDetail ($personal_id, $grayd = 'null', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
	
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, TYPE, REGION_DIR_DEV, TICKET_ID, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND `TASK_REAL_STATUS` <> '6' ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` < $end ";
		$rule .= " AND( (`FINISH_TIME` <> '0000-00-00 00:00:00'  AND `FINISH_TIME` > $end ) ";
		$rule .= " OR `FINISH_TIME` = '0000-00-00 00:00:00' )";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		//echo $sql."<br>";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$arResult[]=$row;
		}
		
		return $arResult;
	
	}
	
	public function getPostponedEpicFailTicketDetail ($personal_id, $grayd = 'null', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
	
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
	
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
	
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, TYPE, REGION_DIR_DEV, TICKET_ID, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND `TASK_REAL_STATUS` = '6' ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` < $end ";
		$rule .= " AND( (`FINISH_TIME` <> '0000-00-00 00:00:00'  AND `FINISH_TIME` > $end ) ";
		$rule .= " OR `FINISH_TIME` = '0000-00-00 00:00:00' )";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		//echo $sql."<br>";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$arResult[]=$row;
		}
	
		return $arResult;
	
	}
	
	public function getEpicFailTaskDetail ($personal_id,  $grayd = 'null', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR, TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID ';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_REAL_STATUS` <> '6' ";
		$rule .= " AND `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < $end ";
		$rule .= " AND ( ( `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` > $end ) ";
		$rule .= " OR `TASK_CLOSED_DATE` = '0000-00-00 00:00:00' ) ";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$arResult[]=$row;
		}
		
		return $arResult;
	
	}
	
	public function getPostponedEpicFailTaskDetail ($personal_id,  $grayd = 'null', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
	
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
	
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR, TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID ';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_REAL_STATUS` = '6' ";
		$rule .= " AND `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < $end ";
		$rule .= " AND ( ( `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` > $end ) ";
		$rule .= " OR `TASK_CLOSED_DATE` = '0000-00-00 00:00:00' ) ";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$arResult[]=$row;
		}
	
		return $arResult;
	
	}
	
/*************************************** IN TIME ******************************************************/
	
	public function getInTimeTicketDetail ($personal_id, $grayd = 'null', $main = 'null')
	{
		//echo $personal_id."<br>";
		//echo $grayd."<br>";
		//echo $this->start."<br>";
		//echo $this->end."<br>";
	
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$table = $this->table;
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
	
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
		
		//echo $month,$year;
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
		$field = 'COUNT, MONTH, YEAR, DEPARTMENT, TYPE, REGION_DIR_DEV, ID, TICKET_ID, TICKET_ID, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		//echo $month,$year;
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_ID` > '0' ";
		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND `FINISH_TIME` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` BETWEEN $start AND $end ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` < `DEADLINE` ";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		//echo $sql;
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$arResult[]=$row;
		}
		
		return $arResult;
	
	}
	
	public function getInTimeTaskDetail ($personal_id,  $grayd = 'null', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR, TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID ';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_TASK_ID` > '0' ";
		$rule .= " AND `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND  `TASK_CLOSED_DATE` BETWEEN $start AND $end ";
		$rule .= " AND ( ( `TASK_DEADLINE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` < `TASK_DEADLINE` ) ";
		$rule .= " OR  `TASK_DEADLINE` = '0000-00-00 00:00:00' ) ";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$arResult[]=$row;
		}
		
		return $arResult;
	
	}
	
/**************************************************************************************************/
	
/*************************************** ALL ******************************************************/
	public function getALLCountTask ($personal_id,  $grayd = 'null', $type = 's', $main = 'null')
	{
		//echo $personal_id;
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
		
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR, TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID ';
	
		//Выполнена
		// Пишем условия выборки
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND ( ";
		
		$rule .= " ( ";
			
		// просроченно
		$rule .= " `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_CLOSED_DATE` BETWEEN $start AND $end ";
		$rule .= " AND `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < `TASK_CLOSED_DATE` ";
		$rule .= " ) ";
		
		$rule .= " OR ";
		
		$rule .= " ( ";
		
		// не выполненно
		$rule .= " `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < $end ";
		$rule .= " AND(  (`TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` > $end ) ";
		$rule .= " OR `TASK_CLOSED_DATE` = '0000-00-00 00:00:00' ) ";
		$rule .= " ) ";
		
		$rule .= " OR ";
		
		$rule .= " ( ";
			
		//сделанно
		$rule .= " `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_CLOSED_DATE` BETWEEN $start AND $end ";
		$rule .= " AND ( ( `TASK_DEADLINE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` < `TASK_DEADLINE` )";
		$rule .= " OR `TASK_DEADLINE` = '0000-00-00 00:00:00' ) ";
		
		$rule .= " ) ";

		$rule .= " ) ";
		
		$sql = "SELECT COUNT(`COUNT`), $field  FROM $table WHERE $rule";
		//echo $sql;
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			//return '<td>'.$row['COUNT(`COUNT`)'].'</td>';
			if($type == 's')
			{
				//return '<td>'.$row['COUNT(`COUNT`)'].'</td>';
				$href = '/table/a_reports/detail.php?';
				$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
				$href .='&GR='.$grayd_link.'&COLUM=AL_TS&month='.$this->month.'&YEAR='.$this->year;
				return '<td class="fallow"><a class="detail_link" href="'.$href.'">'.$row['COUNT(`COUNT`)'].'</a></td>';
			} else {
				return $row['COUNT(`COUNT`)'];
			}
		}
	}

	public function getALLCountTicket ($personal_id, $grayd = 'null', $type = 's', $main = 'null')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
		
		
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
	
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, TYPE, REGION_DIR_DEV, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND ( ";
		
		$rule .= " ( ";
			
		// просроченно
		$rule .= " `FINISH_TIME` <> '0000-00-00 00:00:00' ";
		$rule .= " AND  `FINISH_TIME` BETWEEN $start AND $end  ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` <  `FINISH_TIME` ";
		
		$rule .= " ) ";
			
		$rule .= " OR ";
			
		$rule .= " ( ";
		// не выполненно
		$rule .= " `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` < $end ";
		$rule .= " AND( (`FINISH_TIME` <> '0000-00-00 00:00:00'  AND `FINISH_TIME` > $end ) ";
		$rule .= " OR `FINISH_TIME` = '0000-00-00 00:00:00' )";
			
		$rule .= " ) ";
			
		$rule .= " OR ";
			
		$rule .= " ( ";
		// сделанно вовремя
		$rule .= " `FINISH_TIME` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` BETWEEN $start AND $end ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` < `DEADLINE` ";
		$rule .= " ) ";
		
		$rule .= " )";
		
		$sql = "SELECT COUNT(`COUNT`), $field  FROM $table WHERE $rule";
		//echo $sql.'<br>';
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			//return '<td>'.$row['COUNT(`COUNT`)'].'</td>';
			if($type == 's')
			{
				//return '<td>'.$row['COUNT(`COUNT`)'].'</td>';
				//return '<td>aaaaaaa</td>';
				$href = '/table/a_reports/detail.php?';
				$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
				$href .='&GR='.$grayd_link.'&COLUM=AL_TK&month='.$this->month.'&YEAR='.$this->year;
				return '<td class="fallow"><a class="detail_link" href="'.$href.'">'.$row['COUNT(`COUNT`)'].'</a></td>';
			} else {
				return $row['COUNT(`COUNT`)'];
			}
		}
	}
	
/*************************************** FAIL ****************************************************/
	
	public function getFailCountTicket ($personal_id, $grayd = 'null', $type = 's', $main = 'null', $colum='TK')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
		
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
	
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, `TYPE`, REGION_DIR_DEV, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND `FINISH_TIME` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` BETWEEN $start AND $end ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` <  `FINISH_TIME` ";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
		
		while ($row = mysql_fetch_assoc($result))
		{
			$q = $this->getAPercentageOfDelay($row['DATE_CREATE'], $row['DEADLINE'], $row['FINISH_TIME']);		
			if ($q <= 50)
			{
				$res['a'][] = $row;
			}
			elseif ($q >50 && $q <= 100)
			{
				$res['b'][] = $row;
			}
			else
			{
				$res['c'][] = $row;
			}

		}

		$href = '/table/a_reports/detail.php?';
		$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
		$href .='&GR='.$grayd_link.'&month='.$this->month.'&YEAR='.$this->year.'&COLUM=';
		
		$strHTML = '<td class="yellow"><a class="detail_link" href="'.$href.'FA_'.$colum.'">'.count($res['a']).'</a></td>';
		
		$strHTML .= '<td class="yellow-dark"><a class="detail_link" href="'.$href.'FB_'.$colum.'">'.count($res['b']).'</a></td>';
		
		$strHTML .= '<td class="red"><a class="detail_link" href="'.$href.'FC_'.$colum.'">'.count($res['c']).'</a></td>';
		if($type == 's')
		{
			return $strHTML;
		} else {
			$simple['a']=count($res['a']);
			$simple['b']=count($res['b']);
			$simple['c']=count($res['c']);
			return $simple;
		}	
	}
	
	public function getFailCountTask ($personal_id, $grayd = 'null', $type = 's', $main = 'null', $colum = 'TS')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR,  TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID ';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_CREATED_DATE` < $end ";
		$rule .= " AND `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_CLOSED_DATE` BETWEEN $start AND $end ";
		$rule .= " AND `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < `TASK_CLOSED_DATE` ";
		
		$sql = "SELECT $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			$q = $this->getAPercentageOfDelay($row['TASK_CREATED_DATE'], $row['TASK_DEADLINE'], $row['TASK_CLOSED_DATE']);
			if ($q <= 50)
			{
				$res['a'][] = $row;
			}
			elseif ($q >50 && $q <= 100)
			{
				$res['b'][] = $row;
			}
			else
			{
				$res['c'][] = $row;
			}
		}
		$href = '/table/a_reports/detail.php?';
		$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
		$href .='&GR='.$grayd_link.'&month='.$this->month.'&YEAR='.$this->year.'&COLUM=';
		
		$strHTML = '<td class="yellow"><a class="detail_link" href="'.$href.'FA_'.$colum.'">'.count($res['a']).'</a></td>';
		
		$strHTML .= '<td class="yellow-dark"><a class="detail_link" href="'.$href.'FB_'.$colum.'">'.count($res['b']).'</a></td>';
		
		$strHTML .= '<td class="red"><a class="detail_link" href="'.$href.'FC_'.$colum.'">'.count($res['c']).'</a></td>';
		//return $strHTML;
		if($type == 's')
		{
			return $strHTML;
		} else {
			$simple['a']=count($res['a']);
			$simple['b']=count($res['b']);
			$simple['c']=count($res['c']);
			return $simple;
		}
	}
	
/*************************************** EPIC FAIL ******************************************************/
	
	public function getEpicFailCountTicket ($personal_id, $grayd = 'null', $type = 's', $main = 'null', $colum = 'EF_TK')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
		
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
	
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, TYPE, REGION_DIR_DEV, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND `TASK_REAL_STATUS` <> '6' ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` < $end ";
		$rule .= " AND( (`FINISH_TIME` <> '0000-00-00 00:00:00'  AND `FINISH_TIME` > $end ) ";
		$rule .= " OR `FINISH_TIME` = '0000-00-00 00:00:00' )";
		
		$sql = "SELECT COUNT(`COUNT`), $field  FROM $table WHERE $rule";
		//echo $sql."<br>";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			if($type == 's')
			{
				//return '<td class="ddd">'.$row['COUNT(`COUNT`)'].'</td>';
				$href = '/table/a_reports/detail.php?';
				$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
				$href .='&GR='.$grayd_link.'&COLUM='.$colum.'&month='.$this->month.'&YEAR='.$this->year;
				return '<td class="fail"><a class="detail_link" href="'.$href.'">'.$row['COUNT(`COUNT`)'].'</a></td>';
			} else {
				return $row['COUNT(`COUNT`)'];
			}			
		}
	
	} 
	
	public function getPostponedEpicFailCountTicket ($personal_id, $grayd = 'null', $type = 's', $main = 'null', $colum='EP_TK')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
	
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, TYPE, REGION_DIR_DEV, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE, TASK_REAL_STATUS';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		if($this->departament == 'Девелопмент')
		{
			// по региональному директору
			#{
			$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
			#}
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND `TASK_REAL_STATUS` = '6' ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `DEADLINE` < $end ";
		$rule .= " AND( (`FINISH_TIME` <> '0000-00-00 00:00:00'  AND `FINISH_TIME` > $end ) ";
		$rule .= " OR `FINISH_TIME` = '0000-00-00 00:00:00' )";
		
		$sql = "SELECT COUNT(`COUNT`), $field  FROM $table WHERE $rule";
		//echo $sql."<br>";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			if($type == 's')
			{
				//return '<td class="ddd">'.$row['COUNT(`COUNT`)'].'</td>';
				$href = '/table/a_reports/detail.php?';
				$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
				$href .='&GR='.$grayd_link.'&COLUM='.$colum.'&month='.$this->month.'&YEAR='.$this->year;
				return '<td><a class="detail_link" href="'.$href.'">'.$row['COUNT(`COUNT`)'].'</a></td>';
			} else {
				return $row['COUNT(`COUNT`)'];
			}
		}
	
	}
	
	public function getEpicFailCountTask ($personal_id,  $grayd = 'null', $type = 's', $main = 'null', $colum='EF_TS')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR,  TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID, TASK_REAL_STATUS ';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_REAL_STATUS` <> '6' ";
		$rule .= " AND `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < $end ";
		$rule .= " AND ( ( `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` > $end ) ";
		$rule .= " OR `TASK_CLOSED_DATE` = '0000-00-00 00:00:00' ) ";
		
		$sql = "SELECT COUNT(`COUNT`), $field  FROM $table WHERE $rule";
		//echo $sql."<br>";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			if($type == 's')
			{
				//return '<td class="ddd">'.$row['COUNT(`COUNT`)'].'</td>';
				$href = '/table/a_reports/detail.php?';
				$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
				$href .='&GR='.$grayd_link.'&COLUM='.$colum.'&month='.$this->month.'&YEAR='.$this->year;
				return '<td class="fail"><a class="detail_link" href="'.$href.'">'.$row['COUNT(`COUNT`)'].'</a></td>';
			} else {
				return $row['COUNT(`COUNT`)'];
			}
		}
	
	}
	
	public function getPostponedEpicFailCountTask ($personal_id, $grayd = 'null', $type = 's', 	$main = 'null', $colum='EP_TS')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';
	
		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR,  TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID, TASK_REAL_STATUS ';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
	
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_REAL_STATUS` = '6' ";
		$rule .= " AND `TASK_DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `TASK_DEADLINE` < $end ";
		$rule .= " AND ( ( `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` > $end ) ";
		$rule .= " OR `TASK_CLOSED_DATE` = '0000-00-00 00:00:00' ) ";
		
		$sql = "SELECT COUNT(`COUNT`), $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			if($type == 's')
			{
				//return '<td class="ddd">'.$row['COUNT(`COUNT`)'].'</td>';
				$href = '/table/a_reports/detail.php?';
				$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
				$href .='&GR='.$grayd_link.'&COLUM='.$colum.'&month='.$this->month.'&YEAR='.$this->year;
				return '<td><a class="detail_link" href="'.$href.'">'.$row['COUNT(`COUNT`)'].'</a></td>';
			} else {
				return $row['COUNT(`COUNT`)'];
			}
		}
	
	}
	
/*************************************** IN TIME ******************************************************/

	public function getInTimeCountTicket ($personal_id, $grayd = 'null', $type = 's', $main = 'null', $colum = 'IT_TK')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$table = $this->table;
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$departament = '\''.$this->departament.'\'';
		
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';

		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
		$field = 'COUNT, MONTH, YEAR,  DEPARTMENT, TYPE, REGION_DIR_DEV, TASK_ID, DEADLINE, ';
		$field .= 'FINISH_TIME, DATE_CREATE, TASK_RESPONSIBLE_ID, TASK_GRADE';
		
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = "`TYPE`='TK' AND `DEPARTMENT`= $departament  ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
		
		if($this->departament == 'Девелопмент')
		{
		// по региональному директору
		#{
		$rule .= " AND `REGION_DIR_DEV` = $personal_id ";
		#}	
		} else {
			//по исполнителю
			#{
			$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
			#}
		}
		
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_ID` > '0' ";
		$rule .= " AND `DATE_CREATE` < $end ";
		$rule .= " AND `FINISH_TIME` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` BETWEEN $start AND $end ";
		$rule .= " AND `DEADLINE` <> '0000-00-00 00:00:00' ";
		$rule .= " AND `FINISH_TIME` < `DEADLINE` ";
		
		$sql = "SELECT COUNT(`COUNT`), $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			if($type == 's')
			{
				//return '<td class="green">'.$row['COUNT(`COUNT`)'].'</td>';
				$href = '/table/a_reports/detail.php?';
				$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
				$href .='&GR='.$grayd_link.'&COLUM='.$colum.'&month='.$this->month.'&YEAR='.$this->year;
				return '<td class="intime"><a class="detail_link" href="'.$href.'">'.$row['COUNT(`COUNT`)'].'</a></td>';
			} else {
				return $row['COUNT(`COUNT`)'];
			}
		}
	
	}
	
	public function getInTimeCountTask ($personal_id,  $grayd = 'null', $type = 's', $main = 'null', $colum = 'IT_TS')
	{
		if($this->start == 0) return 'Null';
		if($this->end == 0) return 'Null';
		if(!isset($personal_id)) return 'Null';
		
		$personal_id_link = $personal_id;
		$grayd_link = $grayd;
		
		$personal_id = '\''.$personal_id.'\'';
		$start = '\''.$this->start.'\'';
		$end = '\''.$this->end.'\'';
		$table = $this->table;
		$month = '\''.$this->month.'\'';
		$year = '\''.$this->year.'\'';

		switch ($grayd)
		{
			case '1':
				$grayd = '146';
				break;
			case '2':
				$grayd = '147';
				break;
			case 'null':
				$grayd = '147';
				break;
			case '3':
				$grayd = '148';
				break;
			case '4':
				$grayd = '149';
				break;
		}
			
		$field =  ' COUNT, MONTH, YEAR,  TYPE, TASK_TASK_ID, TASK_GRADE, TASK_CREATED_DATE, TASK_DEADLINE, ';
		$field .= ' TASK_CLOSED_DATE, TASK_RESPONSIBLE_ID ';
	
		//Выполнена
		// Пишем условия выборки
		//типа заявка:"ТК", служба:'Девелопмент'
		#{
		$rule = " `TYPE` = 'TS' ";
		$rule .= " AND  `YEAR` = $year ";
		$rule .= " AND  `MONTH` = $month ";
		#}
	
		//по исполнителю
		#{
		$rule .= " AND `TASK_RESPONSIBLE_ID` = $personal_id ";
		#}
		
		if($main == 'null')
		{
			if( $grayd == '147')
			{
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND (`TASK_GRADE` = '0' OR `TASK_GRADE` = '147')";
				#}
			} else {
				$grayd = '\''.$grayd.'\'';
				// выбираем грейд
				#{
				$rule .= " AND `TASK_GRADE` = $grayd ";
				#}
			}
		}

		$rule .= " AND `TASK_TASK_ID` > '0' ";
		$rule .= "  AND `TASK_CREATED_DATE` < $end ";
		$rule .= "  AND `TASK_CLOSED_DATE` <> '0000-00-00 00:00:00' ";
		$rule .= "  AND  `TASK_CLOSED_DATE` BETWEEN $start AND $end ";
		$rule .= " AND ( ( `TASK_DEADLINE` <> '0000-00-00 00:00:00' AND `TASK_CLOSED_DATE` < `TASK_DEADLINE` ) ";
		$rule .= " OR  `TASK_DEADLINE` = '0000-00-00 00:00:00' ) ";
		
		$sql = "SELECT COUNT(`COUNT`), $field  FROM $table WHERE $rule";
		$result = mysql_query($sql);
	
		if (!$result)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		while ($row = mysql_fetch_assoc($result))
		{
			if($type == 's')
			{
				//return '<td class="green">'.$row['COUNT(`COUNT`)'].'</td>';
				$href = '/table/a_reports/detail.php?';
				$href .= 'table='.$table.'&DEP='.$this->departament_link.'&ID='.$personal_id_link;
				$href .='&GR='.$grayd_link.'&COLUM='.$colum.'&month='.$this->month.'&YEAR='.$this->year;
				return '<td class="intime"><a class="detail_link" href="'.$href.'">'.$row['COUNT(`COUNT`)'].'</a></td>';
			} else {
				return $row['COUNT(`COUNT`)'];
			}
		}
	
	}
	
	private function convert_time_to_sql($time) {
		if(!isset($time)) return;
		//in  DD.MM.YYYY HH:MM:SS
		$two_side = explode(' ',$time);
		$left_side = explode('.',$two_side['0']); //  DD.MM.YYYY
		//out YYYY-MM-DD HH:MM:SS
		return $left_side['2'].'-'.$left_side['1'].'-'.$left_side['0'].' '.$two_side['1'];
	}
	
	private function convert_sql_to_time($sqltime) {
		//in  DD.MM.YYYY HH:MM:SS
		if(!isset($sqltime)) return;
		$two_side = explode(' ', $sqltime);
		$left_side = explode('-',$two_side['0']); //  DD.MM.YYYY
		//out YYYY-MM-DD HH:MM:SS
		return $left_side['2'].'-'.$left_side['1'].'-'.$left_side['0'].' '.$two_side['1'];
	}
	
	/*private function getAPercentageOfDelay($dateCreate, $deadline, $finishTime)
	{
		$deadline =  $this->convertToUnixTime($deadline);
		$finishTime =  $this->convertToUnixTime($finishTime);
		$dateCreate =  $this->convertToUnixTime($dateCreate);			
		$q = (($finishTime - $deadline)*100)/($deadline - $dateCreate);
		return  round($q);
	}*/
	
	private function convertToUnixTime($date)
	{
		//'YYYY-MM-DD HH:MI:SS';
		$two_side = explode(' ',$date);
		$left_side = explode('-',$two_side['0']); //  YYYY-MM-DD								
		$right_side = explode(':',$two_side['1']); //  HH:MI:SS
		//  часы, минуты, секунды, месяц, день, год
		return  mktime ($right_side['0'], $right_side['1'], $right_side['2'], $left_side['1'], $left_side['2'], $left_side['0']);
	}
		
	private function take_now_time_MONTH () {
		$format = 'm';
		$NOW_MONTH = date($format,time());
		return $NOW_MONTH;
	}
	
	private function take_now_time_YEAR () {
		$format = 'Y';
		$NOW_YEAR = date($format,time());
		return $NOW_YEAR;
	}

	private function setDataStart ($start)
	{
		$this->start = $this->convert_time_to_sql($start);
	}
	
	private function setDataEnd ($end)
	{
		$this->end = $this->convert_time_to_sql($end);
	}

	function __construct($table = 0, $start = 0, $end = 0, $departament = 0)
	{
		if($table === 0)
		{
			echo 'Не указана таблица для выборки!';
			exit;
		}
		
		if($start === 0)
		{
			echo 'Не указана дата начала периода!';
			exit;
		}
		
		if($end === 0)
		{
			echo 'Не указана дата конца периода!';
			exit;
		}
		
		if($departament === 0)
		{
			echo 'Не указана Служба!';
			exit;
		}

		$this->table = $table;
		$this->start = $this->convert_time_to_sql($start);
		$this->end = $this->convert_time_to_sql($end);

		$delim_date = explode(' ',$this->start);
		$left_side = explode('-',$delim_date['0']);
		
		$this->month = $left_side['1'];
		$this->year = $left_side['0'];
		$this->departament_link = $departament;
		switch ($departament)
		{
			case 'IT':
				$this->departament = 'Служба ИТ';
				//$this->userGroupID = $this->getDepartamentWorker(13);
				break;
			case 'DEV':
				$this->departament = 'Девелопмент';
				//$this->userGroupID = $this->getDistinktWorker();
				break;
			case 'MRKT':
				$this->departament = 'Служба Маркетинга';
				//$this->userGroupID = $this->getDepartamentWorker(13);
				break;
			case 'HR':
				$this->departament = 'Служба HR';
				//$this->userGroupID = $this->getDepartamentWorker(13);
				break;
		}
	}
	
	function __destruct(){}
}