<?
class workWithDB {
	private static $dbhost = 'localhost'; // Имя хоста БД
	private static $dbusername = 'report'; // Пользователь БД root
	private static $dbpass = '123qweasd'; // Пароль к базе   123qweasd 
	private static $dbname = 'report_ms_portal'; // Имя базы
	private $dbnameconnect = '1'; // Имя базы
	
	public function Fdbname ()
	{
		return self::$dbname;
	}
	
	public function dbnameconnect ()
	{
		return $this->dbnameconnect;
	}
	
	public function getDistinktMonth ($tableName)
	{
		$sql = " SELECT DISTINCT MONTH FROM $tableName ";
		$result = mysql_query($sql);
	
		if (!$result) {
			echo "Ошибка базы, не удалось получить список таблиц!\n<br>";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		$arResult = array();
	
		while ($row = mysql_fetch_assoc($result))
		{
				$arResult[] = $row['MONTH'];
		}
		return $arResult;
	}
	
	public function getMonthName ($monthNumber)
	{
		$monthNumber= ltrim($monthNumber,'0');
		switch ($monthNumber)
		{
			case 1:
				return 'Январь';
				break;
			case 2:
				return 'Февраль';
				break;
			case 3:
				return 'Март';
				break;
			case 4:
				return 'Апрель';
				break;
			case 5:
				return 'Май';
				break;
			case 6:
				return 'Июнь';
				break;
			case 7:
				return 'Июль';
				break;
			case 8:
				return 'Август';
				break;
			case 9:
				return 'Сентябрь';
				break;
			case 10:
				return 'Октябрь';
				break;
			case 11:
				return 'Ноябрь';
				break;
			case 12:
				return 'Декабрь';
				break;
				
				
		}
		return $arResult;
	}
	
	public function getDistinktYEAR ($tableName)
	{
		$sql = " SELECT DISTINCT YEAR FROM $tableName ";
		$result = mysql_query($sql);
	
		if (!$result) {
			echo "Ошибка базы, не удалось получить список таблиц!\n<br/>";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
	
		$arResult = array();
	
		while ($row = mysql_fetch_assoc($result))
		{
	
			$arResult[] = $row['YEAR'];
	
		}
		return $arResult;
	}
	
	
	public function format_to_save_string ($text)
	{
		$text = addslashes($text);
		$text= htmlspecialchars ($text);
		$text = addslashes($text);
		$username = preg_replace("/[^А-Яа-яA-Za-z0-9]/i", "", $_GET['username']);
		$text = str_replace("'/\|",'',$text);
		$text = str_replace("\r\n",' ',$text);
		$text = str_replace("\n",' ',$text);
		return $text;
	}
	
	public function dropTable ($name)
	{
		$sql = 'DROP TABLE '.$name;
		$result = mysql_query($sql);
		return $result;
	}
	
	public function createTable ($name)
	{
		$sql = '
		CREATE TABLE '.$name.'
		(
			COUNT INT AUTO_INCREMENT,
			MONTH INT,
			YEAR INT,
			TYPE CHAR(3),
			ID INT,
	        NAME CHAR(255),
			DETAIL_TEXT TEXT,
			DATE_CREATE DATETIME,
	        TICKET_ID INT,
	        STATUS_ID VARCHAR(50),
	        APPLICANT INT,
	        LAST_NAME_APPLICANT VARCHAR(50),
	        NAME_APPLICANT VARCHAR(50),
	        SECOND_NAME_APPLICANT VARCHAR(50),
	        APPLICANT_DEPATMENT VARCHAR(50),
	        RESPONSIBLE INT,
	        LAST_NAME_RESPONSIBLE VARCHAR(50),
	        NAME_RESPONSIBLE VARCHAR(50),
	        SECOND_NAME_RESPONSIBLE VARCHAR(50),
	        SITY VARCHAR(50),
	        ADDRESS VARCHAR(100),
	        KOMMERS_DIR INT,
	        SALES_DIR INT,
	        REGION_DIR_DEV INT,
	        TICKET_TIME DATETIME,
	        START_TIME DATETIME,
	        DEADLINE DATETIME,
	        FINISH_TIME DATETIME,
	        OVERDUE VARCHAR(50),
	        TASK_ID INT,
	        IN_COMPANY_TITLE VARCHAR(50),
	        WORK_FLOW_TEMPLATED INT,
	        TASK_RESULT VARCHAR(100),
	        LIMITATION DATETIME,
	        CATEGORIYA VARCHAR(50),
	        CRITICALITY_ID  VARCHAR(50),
	        SECTION_1 VARCHAR(100),
	        SECTION_2 VARCHAR(100),
	        SECTION_3 VARCHAR(100),
	        SECTION_4 VARCHAR(100),
	        SECTION_5 VARCHAR(100),
	        BP_TITLE VARCHAR(50),
	        DEPARTMENT VARCHAR(50),
	        SLA_TITLE VARCHAR(50),
	        SLA_TIME INT,
	        SLA_TIME_TITLE VARCHAR(50),
	        FINISH_HH INT,
	        FINISH_DAY VARCHAR(50),
	        NAME_KOMMERS_DIR VARCHAR(50),
	        SECOND_NAME_KOMMERS_DIR VARCHAR(50),
	        LAST_NAME_KOMMERS_DIR VARCHAR(50),
	        NAME_SALES_DIR VARCHAR(50),
	        SECOND_NAME_SALES_DIR VARCHAR(50),
	        LAST_NAME_SALES_DIR VARCHAR(50),
	        NAME_REGION_DIR_DEV VARCHAR(50),
	        SECOND_NAME_REGION_DIR_DEV VARCHAR(50),
	        LAST_NAME_REGION_DIR_DEV VARCHAR(50),
	        TASK_TASK_ID INT,
	        TASK_TITLE CHAR(255),
	        TASK_DESCRIPTION TEXT,
	        TASK_RESPONSIBLE_ID INT,
	        TASK_RESPONSIBLE_NAME VARCHAR(50),
	        TASK_RESPONSIBLE_LAST_NAME VARCHAR(50),
	        TASK_RESPONSIBLE_SECOND_NAME VARCHAR(50),
	        TASK_RESPONSIBLE_LOGIN VARCHAR(50),
	        TASK_RESPONSIBLE_WORK_POSITION VARCHAR(100),
	        TASK_DATE_START DATETIME,
	        TASK_DURATION_PLAN VARCHAR(20),
	        TASK_DURATION_TYPE VARCHAR(20),
	        TASK_DURATION_FACT VARCHAR(20),
	        TASK_TIME_ESTIMATE VARCHAR(20),
	        TASK_DEADLINE DATETIME,
	        TASK_DEADLINE_ORIG DATETIME,
	        TASK_START_DATE_PLAN DATETIME,
	        TASK_END_DATE_PLAN DATETIME,
	        TASK_CREATED_BY INT,
	        TASK_CREATED_BY_NAME VARCHAR(50),
	        TASK_CREATED_BY_LAST_NAME VARCHAR(50),
	        TASK_CREATED_BY_SECOND_NAME VARCHAR(50),
	        TASK_CREATED_BY_LOGIN VARCHAR(50),
	        TASK_CREATED_DATE DATETIME,
	        TASK_CHANGED_DATE DATETIME,
	        TASK_STATUS_CHANGED_DATE DATETIME,
	        TASK_CLOSED_BY INT,
	        TASK_CLOSED_DATE DATETIME,
	        TASK_ALLOW_CHANGE_DEADLINE VARCHAR(5),
	        TASK_ALLOW_TIME_TRACKING VARCHAR(5),
	        TASK_TASK_CONTROL VARCHAR(5),
	        TASK_PARENT_ID INT,
	        TASK_COMMENTS_COUNT INT,
	        TASK_GRADE INT,
	        TASK_STATUS INT,
	        TASK_REAL_STATUS INT,
	        TASK_MULTITASK VARCHAR(5),
			PRIMARY KEY(COUNT)
		)
	';
	if(!mysql_query($sql))
	{
		echo mysql_error().'<br>';
	}
		return $result;
	}
	
	public function convert_time_to_sql($time) {
		if(!isset($time)) return;
		//in  DD.MM.YYYY HH:MM:SS
		$two_side = explode(' ',$time);
		$left_side = explode('.',$two_side['0']); //  DD.MM.YYYY
		//out YYYY-MM-DD HH:MM:SS
		return $left_side['2'].'-'.$left_side['1'].'-'.$left_side['0'].' '.$two_side['1'];
	}
	
	public function convert_sql_to_time($sqltime) {
		//in  DD.MM.YYYY HH:MM:SS
		if(!isset($sqltime)) return;
		$two_side = explode(' ', $sqltime);
		$left_side = explode('-',$two_side['0']); //  DD.MM.YYYY
		//out YYYY-MM-DD HH:MM:SS
		return $left_side['2'].'-'.$left_side['1'].'-'.$left_side['0'].' '.$two_side['1'];
	}
	
	public function take_now_time_MONTH () {
		//MONTH,
		$format = 'm';
		$NOW_MONTH = date($format,time());
		return $NOW_MONTH;
	}
	
	public function take_now_time_YEAR () {
		//YEAR,
		$format = 'Y';
		$NOW_YEAR = date($format,time());
		return $NOW_YEAR;
	}
	
	
	
	function __construct()
	{
		//echo 'Устанавливаем соедениение с базой данных!<br>';
		$this->dbnameconnect = mysql_connect (self::$dbhost, self::$dbusername, self::$dbpass);
		mysql_set_charset('utf8',$this->dbnameconnect);
		if (!$this->dbnameconnect) 
		{ 
			echo 'Не могу подключиться к серверу базы данных!<br>';
		} 
		
		if(mysql_select_db(self::$dbname)) 
		{
			//echo 'Подключение к базе '.self::$dbname.' установлено! <br>'; 
		}
		else {
			mysql_close($this->dbnameconnect);
			die ('Не могу подключиться к базе данных '.self::$dbname.'!<br>');
		}

	}
	
	function __destruct() {
		//echo 'Закрываем соеденение с базой данных!<br>';
		if(mysql_close($this->dbnameconnect))
		{
			//echo 'Connect with DB close!<br>';
		} 
		else {
			echo 'Connect with DB NOT close!<br>';
		}
	}
}