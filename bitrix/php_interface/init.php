<?
	function pre($array){
		echo '<pre>';
		print_r($array);
		echo '</pre>';
		return;
	}
	
	# Подключаем пользовательские классы.
	CModule::AddAutoloadClasses(
			'', // не указываем имя модуля
			array(
				// ключ - имя класса, 
				// значение - путь относительно корня сайта к файлу с классом
				'UserClassWorkTime' => '/bitrix/php_interface/UserClass/UserClassWorkTime.php',
				'workWithDB' => '/bitrix/php_interface/UserClass/workWithDB.php',
				'tenderAPI'  => '/bitrix/php_interface/UserClass/tenderAPI.php',
				'tenderBD'   => '/bitrix/php_interface/UserClass/tenderBD.php',
				'reportForCompani' => '/bitrix/php_interface/UserClass/reportForCompani.php',
				'CompanyReport' => '/bitrix/php_interface/UserClass/CompanyReport.php',
				
				//Reports
				'report' => '/bitrix/php_interface/UserClass/reports/report.php',
				'threeIntervalReport' => '/bitrix/php_interface/UserClass/reports/threeIntervalReport.php',
				'subTaskReport' => '/bitrix/php_interface/UserClass/reports/subTaskReport.php',
				
				//Base Class
				'staffDepartamentClass' => '/bitrix/php_interface/UserClass/IT/baseClass/staffDepartamentClass.php',
				'staffGroupClass' => '/bitrix/php_interface/UserClass/IT/baseClass/staffGroupClass.php',
				'staffUnitClass' => '/bitrix/php_interface/UserClass/IT/baseClass/staffUnitClass.php',
					
				//extends Departament Class
				'ITstaffDepartamentClass' => '/bitrix/php_interface/UserClass/IT/extends/departaments/ITstaffDepartamentClass.php',
				'DEVstaffDepartamentClass' => '/bitrix/php_interface/UserClass/IT/extends/departaments/DEVstaffDepartamentClass.php',
				'MRKTstaffDepartamentClass' => '/bitrix/php_interface/UserClass/IT/extends/departaments/MRKTstaffDepartamentClass.php',
				'HRstaffDepartamentClass' => '/bitrix/php_interface/UserClass/IT/extends/departaments/HRstaffDepartamentClass.php',
				
				//extends Group Class
				'chiefAdminGroupClass' => '/bitrix/php_interface/UserClass/IT/extends/groups/chiefAdminGroupClass.php',
				'phpGroupClass' => '/bitrix/php_interface/UserClass/IT/extends/groups/phpGroupClass.php',
				'callcenterGroupClass' => '/bitrix/php_interface/UserClass/IT/extends/groups/callcenterGroupClass.php',
				'adminGroupClass' => '/bitrix/php_interface/UserClass/IT/extends/groups/adminGroupClass.php',
				'serverGroupClass' => '/bitrix/php_interface/UserClass/IT/extends/groups/serverGroupClass.php',
				'oneCGroupClass' => '/bitrix/php_interface/UserClass/IT/extends/groups/oneCGroupClass.php',
			)
	);




AddEventHandler("support", "OnAfterTicketAdd", "OnAfterTicketAddHandler");


	// Create function
	//static public $NEW_ELEment;
	//public $WORK_FLOW_TEMPLATED;
	
	function OnAfterTicketAddHandler($arFields, $is_new)
	{
		if($is_new !== false)
		{
		//$SLA -  массив содержащий параметры SLA
		//$Ticket - массив содержащий параметры тикета
		//$deadline_time - дата дедлайна тикета
		//$support_group - ID группы ответственных лиц
		//$SLA_ID - ID SLA
		//$sheif - ID руководителя группы
		//$users - массив ответственных пользователей
		//$BP_ID - ID шаблона бизнеспроцесса

			if( (CModule::IncludeModule("support")) && (CModule::IncludeModule("iblock")) )
			{

			/************************************************/
			
				$ticketID = $arFields['ID'];
				$arTicket = CTicket::GetByID($ticketID);
				while($Ticket_array = $arTicket->GetNext())
				{
					$Ticket = $Ticket_array;
				}
				
			/************************************************/
				
				$support = explode(" ", $Ticket['TITLE']);
				$SLA_ID = $support['0'];
				$support_group = $support['1'];
				$BP_ID = $support['2'];
				$Last_section = $support['3'];
				$schedule = $support['4'];
				
			/************************************************/			
					
			// формируем привязки к регионам и определяем адрес возникновения заявки	
			
				$rs_autor = CUser::GetByID($Ticket['OWNER_USER_ID']);
				$autor = $rs_autor->Fetch();
				if(isset($autor['UF_DEPARTMENT']['0']) && !empty($autor['UF_DEPARTMENT']['0']))
				{
					$res_autor_post = CIBlockSection::GetByID($autor['UF_DEPARTMENT']['0']);
					if($autor_post = $res_autor_post->GetNext())
					{
						$autorAddress = $autor_post['NAME'];
						
						$autor_podrazdelenie = $autor_post['NAME'];
						$autor_podrazdelenie = trim($autor_podrazdelenie,' ');
						$autor_podrazdelenie = explode(' ', $autor_podrazdelenie);
						$podrazdelenie = trim($autor_podrazdelenie['1'],'№');
						
						$adres = explode(',', $autor_post['NAME']);
						array_shift($adres);
						$autor_adres = '';
						foreach ($adres as $adr)
						{
							echo $adr;
							$autor_adres .= $adr.' ';
						}
						$autor_adres = trim($autor_adres, ' ');
					}
					
					$autor_siti = CIBlockSection::GetByID($autor_post['IBLOCK_SECTION_ID']);
					if($autor_siti = $autor_siti->Fetch())
					{
						$autor_sity = $autor_siti['NAME'];
					}
					if($support['2'] == 23){
						$podrazdelenie = $autor_post['NAME'];
					}
					
				}
			
			/***********************************************************/
				
			// вытягиваем из инфоблока Подразделения в 
			// оргструктуре ХХХХХХ_REGION для определения регионального подчинения
				
				$arOrder_region = Array("SORT"=>"ASC");
				$arFilter_region = Array(
						"IBLOCK_ID" => $autor_post['IBLOCK_ID'],
						"ID" => $autor['UF_DEPARTMENT']['0']
				);
				$bIncCnt_region = false;
				$Select_region = Array("UF_IT_REGION");
				$NavStartParams_region = false;
				$res = CIBlockSection::GetList(
						$arOrder_region, 
						$arFilter_region, 
						$bIncCnt_region, 
						$Select_region, 
						$NavStartParams_region
				);
				while($ar = $res->GetNext())
				{
					$rsEnum = CUserFieldEnum::GetList(array(), array("ID" =>$ar["UF_IT_REGION"]));
					$arEnum = $rsEnum->GetNext();
				}
				$IT_REGION_ID_GROUP_array = explode("_", $arEnum['VALUE']);
				$IT_REGION_ID_GROUP = $IT_REGION_ID_GROUP_array['0'];
				$arUsers_region = CGroup::GetGroupUser($IT_REGION_ID_GROUP_array['0']);
				
			/************************************************/
				
				$arSLA = CTicketSLA::GetByID($SLA_ID);
				while($SLA_array = $arSLA->GetNext())
				{
					$SLA = $SLA_array;
				}
					
			/************************************************/

				$stmp = MakeTimeStamp($Ticket['DATE_CREATE'], "DD.MM.YYYY HH:MI:SS").'<br/>'; // переводим дату создания в ЮНИКС вид
				$SLA_TIME_H = $SLA['M_RESPONSE_TIME']/60;
				$delta_time = $SLA['M_RESPONSE_TIME']*60;
				$deadline_time_unix = $stmp + $delta_time;	
				$deadline_time = ConvertTimeStamp($deadline_time_unix, "FULL"); // дата дедлайна тикета
				
				
			/************************************************/
				
				$a = 's_number';
				$b = 'asc';
				$c = array(
						"TICKET_ID" => $Ticket['ID'],
						"TICKET_ID_EXACT_MATCH" => "Y",
						"IS_HIDDEN" => "N"
						//"IS_MESSAGE" => "Y"
				);
					
				$d = '';
				$CHECK_RIGHTS = 'N';
				$arMessage_of_Ticket = array();
				$mess = CTicket::GetMessageList($a, $b, $c, $d, $CHECK_RIGHTS);
				while($msg = $mess->GetNext())
				{
					$arMessage_of_Ticket[] = $msg;
				}
				
			/************************************************/
				
				
				//$Last_section
				$el = new CIBlockElement;
				$time = ConvertTimeStamp(time(), "FULL");
				$document_name = '№ '.$Ticket['ID'].' от '.$autorAddress;
				$PROP = array(
						"TICKET_ID"	=> $Ticket['ID'], // Индетификатор заявки
						"WORK_FLOW_TEMPLATED" => $BP_ID, // Номер шаблона бизнес процесса
						"DEADLINE" => $deadline_time, // Крайний срок исполнения 2014-10-20 10:53:59
						"STATUS" => Array("VALUE" => "317" ), // Статус документа
						"TASK_RESULT" => "", // Результат выполнения Заявки
						"RESPONSIBLE_GROUP_ID" => array($support_group), // ИД ответственной группы
						"LAST_SECTION_ID" => $Last_section, // ветка услуг
						"LIMITATION" => $deadline_time, // Срок исполнения
						"ADDRESS" => $autorAddress, //адрес возникновения заявки
						"APPLICANT" => $Ticket['OWNER_USER_ID'], // Создал заявку
						"APPLICANT_ID" => $Ticket['OWNER_USER_ID'], // ИД создателя заявки
						"SITY" => $autor_sity, //город создателя заявки
						"NAME_APPLICANT" => $autor['NAME'], // Создал заявку
						"SECOND_NAME_APPLICANT" => $autor['SECOND_NAME'], // Создал заявку
						"LAST_NAME_APPLICANT" => $autor['LAST_NAME'], // Создал заявку
						"APPLICANT_DEPATMENT" => $podrazdelenie,
						"ADDRESS" => $autor_adres,
						"SLA_ID" => $SLA_ID, // ИД условий обслуживания SLA
						"SLA_TIME_H" => $SLA_TIME_H,
						"BP_EXEMPLE" => $BP_ID, // шаблон бизнеспроцесса
						"SCHEDULE" => $schedule,
						"PRIORITY" => "", // Приоритет
						"COMPLEXITY" => "", // Сложность
						"FILE_ATTACHMENT" => "", // Файл вложение
				);
				$arLoadProductArray = Array(
						"IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
						"IBLOCK_ID"      => 31,
						"NAME"           => $document_name,
						"ACTIVE"         => "N",            // активен
						"DATE_ACTIVE_FROM" => $time,
						"PREVIEW_TEXT_TYPE" => "html",
						"PREVIEW_TEXT"	=> $arMessage_of_Ticket['1']['MESSAGE'],
						"DETAIL_TEXT_TYPE" => "html",
						"DETAIL_TEXT"    => $arMessage_of_Ticket['0']['MESSAGE'],
						"PROPERTY_VALUES"=> $PROP,
				);
				if($PRODUCT_ID = $el->Add($arLoadProductArray))
				{
					
					
					$NEW_ELEment = $PRODUCT_ID;
					$TICKET_ID = $arFields['ID'];
					$MESSAGE_ID = null;
					$title_str = 'Заявка принята в обработку!';
					$message_text = "Ваша заявка успешно добавлена в автоматическую обработку обработку! Установленный плановый срок исполнения: $deadline_time ";
					$arFields = array(
							"CREATED_MODULE_NAME"		=> "support", // модуль создатель обращения
							"MODIFIED_MODULE_NAME"		=> "support", // модуль создатель собщения
							"MESSAGE_AUTHOR_USER_ID"	=> "1", //автор обращения/сообщения
							"STATUS_SID" 				=> "new_request", //сим. код статуса обращения
							"SLA_ID"					=> "$SLA_ID", // для установки нового SLA
							"TITLE"						=> $title_str, // заголовок обращения/сообщения
							"MESSAGE"      				=> $message_text, // текст сообщения
					);
					CTicket::Set ($arFields,  $MESSAGE_ID,	$TICKET_ID, "N");
					
				}
			}
		}
	}

	AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");

	function OnAfterIBlockElementAddHandler(&$arayFields) 
	{
		global $USER;
		$userID=$USER->GetID();
		if( (CModule::IncludeModule("bizproc")) && (CModule::IncludeModule("iblock")) ) 
		{
			$IBlockID = CIBlockElement::GetIBlockByID($arayFields['ID']);
			if($IBlockID == 31)
			{	
				$documentId = $arayFields['ID'];//CreateElement::$NEW_ELEment;
				$arOrder = Array("SORT"=>"ASC");
				$arFilter = Array("IBLOCK_ID" => 31, "ID" => $documentId);
				$Select = Array("ID", "IBLOCK_ID", "NAME","PROPERTY_BP_EXEMPLE");
				$NavStartParams = false;
				$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $Select);
				while($ar = $res->GetNext())
				{
					$workflowTemplateId = $ar['PROPERTY_BP_EXEMPLE_VALUE']; 
				}	
				$arErrorsTmp = array();
				$wfId = CBPDocument::StartWorkflow(
					$workflowTemplateId,
					array("iblock", "CIBlockDocument", $documentId),
					array(),
					$arErrorsTmp
				);
				
				if (count($arErrorsTmp) > 0)
				{
					foreach ($arErrorsTmp as $e)
					$errorMessage .= "[".$e["code"]."] ".$e["message"]." ";
				}				
			}
		}			
	}
	

	AddEventHandler("tasks", "OnBeforeTaskDelete",  "FOnTaskDelete");
	function FOnTaskDelete($ID)
	{
		global $USER;
		$userID=$USER->GetID();
		if( ($userID != 1)){
			echo 'Недостаточно прав для выполнения этого действия!<br>';
			return false;
		} else {
			return true;
		}
	}
	
	AddEventHandler("tasks", "OnTaskUpdate", "FOnTaskUpdate");
	function FOnTaskUpdate($ID){
		if (CModule::IncludeModule("tasks")){
			$rsTask = CTasks::GetByID($ID);
			if ($arTask = $rsTask->GetNext()){
				
//  ******************************** STATUS 4  *************************************************** \\
		
				if($arTask['REAL_STATUS'] == 4 && true==empty($arTask['UF_TASK_CLOSE_DATE'])){
					$oTask = CTaskItem::getInstance($arTask['ID'], 1);
					$oTask->Update(array('UF_TASK_CLOSE_DATE' => $arTask['CLOSED_DATE']));
				// ********************************************* \\
					if(CModule::IncludeModule("iblock")){
						$arFilter = array(
								'ACTIVE' => 'Y',
								'IBLOCK_ID' => '42',
								'PROPERTY_TASK_ID' => $arTask['ID']
						);
						$groupBy = false;
						$pageNavigation = false;
						$arSelect = array(
								'IBLOCK_ID',
								'ID',
								'ACTIVE',
								'PROPERTY_TASK_ID'
						);
						$rsElementList = CIBlockElement::GetList(
								$arSort,
								$arFilter,
								$groupBy,
								$pageNavigation,
								$arSelect
						);
						if($element = $rsElementList->GetNext()){
							CIBlockElement::SetPropertyValueCode(
								$element['ID'], 
								'RESPONSIBLE', 
								array($arTask['RESPONSIBLE_ID'])
							);
						}
// ************************************************************* \\
						$arFilter = array('ACTIVE' => 'Y','IBLOCK_ID' => '31','PROPERTY_TASK_ID' => $arTask['ID']);
						$groupBy = false;
						$pageNavigation = false;
						$arSelect = array(
								'IBLOCK_ID',
								'ID',
								'ACTIVE',
								'PROPERTY_TASK_ID'
						);
						$rsElementList = CIBlockElement::GetList(
								$arSort,
								$arFilter,
								$groupBy,
								$pageNavigation,
								$arSelect
								);
						if($element = $rsElementList->GetNext()){
							CIBlockElement::SetPropertyValueCode(
								$element['ID'], 'FINISH_TIME', array($arTask['CLOSED_DATE'])
							);
						}
					}
				}
				
// ************************************************************************************************* //
		
				if($arTask['REAL_STATUS'] == 4 && $arTask['UF_TASK_CLOSE_DATE'] != $arTask['CLOSED_DATE']){
					$oTask = CTaskItem::getInstance($arTask['ID'], 1);
					$oTask->Update(array('UF_TASK_CLOSE_DATE' => $arTask['CLOSED_DATE']));
// ************************************************************** \\
					if(CModule::IncludeModule("iblock")){
						$arFilter = array(
								'ACTIVE' => 'Y',
								'IBLOCK_ID' => '42',
								'PROPERTY_TASK_ID' => $arTask['ID']
						);
						$groupBy = false;
						$pageNavigation = false;
						$arSelect = array(
								'IBLOCK_ID',
								'ID',
								'ACTIVE',
								'PROPERTY_TASK_ID'
						);
						$rsElementList = CIBlockElement::GetList(
								$arSort,
								$arFilter,
								$groupBy,
								$pageNavigation,
								$arSelect
								);
						if($element = $rsElementList->GetNext()){
							CIBlockElement::SetPropertyValueCode(
								$element['ID'],
								'RESPONSIBLE',
								array($arTask['RESPONSIBLE_ID'])
							);
							CIBlockElement::SetPropertyValueCode(
								$element['ID'], 'STATUS_ID', array('Выполнена')
							);
						}
						
// ********************************************************************** \\
			
						$arFilter = array(
								'ACTIVE' => 'Y',
								'IBLOCK_ID' => '31',
								'PROPERTY_TASK_ID' => $arTask['ID']
						);
						$groupBy = false;
						$pageNavigation = false;
						$arSelect = array(
								'IBLOCK_ID',
								'ID',
								'ACTIVE',
								'PROPERTY_TASK_ID'
						);
						$rsElementList = CIBlockElement::GetList(
								$arSort,
								$arFilter,
								$groupBy,
								$pageNavigation,
								$arSelect
								);
						if($element = $rsElementList->GetNext()){
							CIBlockElement::SetPropertyValueCode(
								$element['ID'], 'FINISH_TIME', array($arTask['CLOSED_DATE'])
							);
							CIBlockElement::SetPropertyValueCode(
									$element['ID'], 'STATUS_ID', array('Выполнена')
							);
						}
					}
				}
				
//  ********************************************************************************************************** \\
//  ******************************************* STATUS 5  **************************************************** \\
	
				if($arTask['REAL_STATUS']==5 && true==empty($arTask['UF_TASK_CLOSE_DATE'])){
					$oTask = CTaskItem::getInstance($arTask['ID'], 1);
					$oTask->Update(array('UF_TASK_CLOSE_DATE' => $arTask['CLOSED_DATE']));
					// ********************************************* \\
					if(CModule::IncludeModule("iblock")){
						$arFilter = array(
								'ACTIVE' => 'Y',
								'IBLOCK_ID' => '42',
								'PROPERTY_TASK_ID' => $arTask['ID']
						);
						$groupBy = false;
						$pageNavigation = false;
						$arSelect = array(
								'IBLOCK_ID',
								'ID',
								'ACTIVE',
								'PROPERTY_TASK_ID'
						);
						$rsElementList = CIBlockElement::GetList(
								$arSort,
								$arFilter,
								$groupBy,
								$pageNavigation,
								$arSelect
								);
						if($element = $rsElementList->GetNext()){
							CIBlockElement::SetPropertyValueCode(
									$element['ID'],
									'RESPONSIBLE',
									array($arTask['RESPONSIBLE_ID'])
									);
						}

// ******************************************************************************* \\
							
						$arFilter = array(
								'ACTIVE' => 'Y',
								'IBLOCK_ID' => '31',
								'PROPERTY_TASK_ID' => $arTask['ID']
						);
						$groupBy = false;
						$pageNavigation = false;
						$arSelect = array(
								'IBLOCK_ID',
								'ID',
								'ACTIVE',
								'PROPERTY_TASK_ID'
						);
						$rsElementList = CIBlockElement::GetList(
								$arSort,
								$arFilter,
								$groupBy,
								$pageNavigation,
								$arSelect
								);
						if($element = $rsElementList->GetNext()){
							CIBlockElement::SetPropertyValueCode(
								$element['ID'], 'FINISH_TIME', array($arTask['CLOSED_DATE'])
							);
							CIBlockElement::SetPropertyValueCode(
								$element['ID'], 'STATUS_ID', array('Выполнена')
							);
						}
					}
				}
//  **************************************** STATUS 2 - 3  **************************************************** \\				
				if($arTask['REAL_STATUS']==2 || $arTask['REAL_STATUS']==3){
// ******************************************************************************* \\
					$arFilter = array(
						'ACTIVE' => 'Y',
						'IBLOCK_ID' => '31',
						'PROPERTY_TASK_ID' => $arTask['ID']
					);
					$groupBy = false;
					$pageNavigation = false;
					$arSelect = array(
						'IBLOCK_ID',
						'ID',
						'ACTIVE',
						'PROPERTY_TASK_ID'
					);
					$rsElementList = CIBlockElement::GetList(
						$arSort,
						$arFilter,
						$groupBy,
						$pageNavigation,
						$arSelect
					);
					if($element = $rsElementList->GetNext()){
						CIBlockElement::SetPropertyValueCode(
							$element['ID'], 'STATUS_ID', array('В работе')
						);
						CIBlockElement::SetPropertyValueCode(
								$element['ID'], 'FINISH_TIME', array('')
						);
					}
				}
				
				// ************************  \\
				
				$arFilter = array(
						'ACTIVE' => 'Y',
						'IBLOCK_ID' => '31',
						'PROPERTY_TASK_ID' => $arTask['ID']
				);
				$groupBy = false;
				$pageNavigation = false;
				$arSelect = array(
						'IBLOCK_ID',
						'ID',
						'ACTIVE',
						'PROPERTY_TASK_ID'
				);
				$rsElementList = CIBlockElement::GetList(
						$arSort,
						$arFilter,
						$groupBy,
						$pageNavigation,
						$arSelect
						);
				if($element = $rsElementList->GetNext()){
					CIBlockElement::SetPropertyValueCode(
							$element['ID'], 'RESPONSIBLE', array($arTask['RESPONSIBLE_ID'])
							);
				}
				
				// ************************ \\
			}
		}
	}

	//OnBeforeTaskAdd
	AddEventHandler("tasks", "OnTaskAdd", "FOnTaskAdd");
		
	function FOnTaskAdd($ID)
	{
		if (CModule::IncludeModule("tasks"))
		{
			$rsTask = CTasks::GetByID($ID);
			if ($arTask = $rsTask->GetNext())
			{
				if($arTask[RESPONSIBLE_ID] == '931')
				{
					$arFields = array('RESPONSIBLE_ID' => '501');
					$obTask = new CTasks;
					$success = $obTask->Update($ID, $arFields);
				}
			}
		}		
	}

?>