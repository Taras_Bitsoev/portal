<?php 
class motivationReportClass{
	//public property
	public $MONTH;
	public $YEAR;
	public $START;
	public $END;
	public $DEPARTAMENTS;	
	
	// public methods
	public function giveReport(){
		echo 'Это отчет! giveReport отработал!<br/>';
		return false;
	}
	public function initiateDepartamet($departament = null){
		if($departament === null || (true !== is_string($departament))){
			echo 'Не указан Департамент!';
			return false;
		}
		$departament = self::safeValue($departament);
		switch ($departament){
			case 'IT':
				$IT = new ITstaffDepartamentClass();
				$IT->GetModel($this->START, $this->END);
				$IT->initiateGroups();
				$IT->givMessagesToGroup($_REQUEST);
				$IT->solveResult();
				$this->DEPARTAMENTS['IT'] = $IT;
				break;
			case 'DEV':
			//	$DEV = new DEV_staffDepartamentClass();
			//	$DEV->GetModel($this->START, $this->END);
			//	$DEV->initiateGroups();
			//	$DEV->givMessagesToGroup($_REQUEST);
			//	$DEV->solveResult();
			//	$this->DEPARTAMENTS['DEV'] = $DEV;
				break;
			case 'MRKT':
			//	$MRKT = new MRKT_staffDepartamentClass();
			//	$MRKT->GetModel($this->START, $this->END);
			//	$MRKT->initiateGroups();
			//	$MRKT->givMessagesToGroup($_REQUEST);
			//	$MRKT->solveResult();
			//	$this->DEPARTAMENTS['MRKT'] = $MRKT;
				break;
			case 'HR':
				//$HR = new HR_staffDepartamentClass();
				//$HR->GetModel($this->START, $this->END);
				//$HR->initiateGroups();
				//$HR->givMessagesToGroup($_REQUEST);
				//$HR->solveResult();
				//$this->DEPARTAMENTS['HR'] = $HR;
				break;
		}
		return true;
	}
	
	// system methods
	public function __construct($month = null, $year = null){
		if($month === null ){
			echo 'Не указан месяц!';
			self::__destruct();
		}
		if($year === null){
			echo 'Не указан год!';
			self::__destruct();
		}
		$month = ltrim($month, '0');
		$this->MONTH = self::safeValue($month);
		$this->YEAR = self::safeValue($year);
		
		self::getEND($month, $year);
		self::getSTART($month, $year);
	}
	public function __destruct() {
		
	}
	
	//private property

	//private methods
	private function getSafeArray(array $array){
		$goodArray = array();
		foreach ($array as $key => $value){
			$goodRequest[self::safeValue($key)] = self::safeValue($value);
		}
		return $goodArray;
	}
	private function safeValue($str = NULL){
		if($str == NULL){return false;}
		$input_text = trim($str);
		$input_text = self::stripData($input_text);
		$input_text = htmlspecialchars($input_text);
		$input_text = mysql_escape_string($input_text);
		return $input_text;
	}
	private function stripData($text){
		$quotes = array ("\x27", "\x22", "\x60", "\t", "\n", "\r", "*", "%", "<", ">", "?", "!" );
		$goodquotes = array ("+", "#" ); //"-",
		$repquotes = array ("\+", "\#" ); //"\-",
		$text = trim( strip_tags( $text ) );
		$text = str_replace( $quotes, '', $text );
		$text = str_replace( $goodquotes, $repquotes, $text );
		$text = ereg_replace(" +", " ", $text);
		return $text;
	}
	private function getSTART($month, $year){
		switch ($month){
			case 1:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 2:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 3:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 4:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 5:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 6:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 7:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 8:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 9:
				$this->START = '01.0'.$month.'.'.$year.' 00:00:00';
				break;
			case 10:
				$this->START = '01.'.$month.'.'.$year.' 00:00:00';
				break;
			case 11:
				$this->START = '01.'.$month.'.'.$year.' 00:00:00';
				break;
			case 12:
				$this->START = '01.'.$month.'.'.$year.' 00:00:00';
				break;
		}
	}
	private function getEND($month, $year){
		switch ($month){
			case 1:
				$this->END = '01.0'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 2:
				$this->END = '01.0'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 3:
				$this->END = '01.0'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 4:
				$this->END = '01.0'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 5:
				$this->END = '01.0'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 6:
				$this->END = '01.0'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 7:
				$this->END = '01.0'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 8:
				$this->END = '01.0'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 9:
				$this->END = '01.'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 10:
				$this->END = '01.'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 11:
				$this->END = '01.'.($month+1).'.'.$year.' 00:00:00';
				break;
			case 12:
				$this->END = '01.01.'.($year+1).' 00:00:00';
				break;
		}
	}
}