<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сводный отчет за месяц");?>
<?$usedb = new workWithDB;
$month = $usedb->getDistinktMonth('periods');
$year = $usedb->getDistinktYEAR('periods');

//pre($_REQUEST);

?>
<form action="/table/c_reports/report.php" method="post" name="ticket_statistick" accept-charset="UTF-8" method="post">
	<div class="bid_filter" style="float: left; margin-right: 50px; margin-left: 0;">
		<span class="filter_name" style="margin:0; width: 150px;">Выбрать службу:</span>
		<select name="IN_COMPANY_TITLE"   style="display: inline-block;
										  background-color: #fafafa;
										  background-image: -webkit-linear-gradient(top, #f4f4f4, #fff);
										  background-image: -moz-linear-gradient(top, #f4f4f4, #fff);
										  background-image: -ms-linear-gradient(top, #f4f4f4, #fff);
										  background-image: -o-linear-gradient(top, #f4f4f4, #fff);
										  background-image: linear-gradient(to bottom, #f4f4f4, #fff);
										  border: 1px solid;
										  border-color: #e1e4e5 #daddde #c7c9ca;
										  box-shadow: 0 1px 1px 0 #daddde, inset 0 1px 0 0 #fff;
										  line-height: 28px;
										  padding: 0 10px;
										  border-radius: 4px;
										  cursor: pointer;
										  text-decoration: none;
										  color: #67727a;
										  font-weight: normal;
										  font-size: 13px;
										  word-spacing: normal;
										  position: relative;
										  margin: 0 3px;">
			<option value="IT" <? if(isset($_POST['IN_COMPANY_TITLE'])){if($_POST['IN_COMPANY_TITLE']=='IT') echo ' selected="selected"';}?>>IT служба</option>
			<option value="DEV" <? if(isset($_POST['IN_COMPANY_TITLE'])){if($_POST['IN_COMPANY_TITLE']=='DEV') echo ' selected="selected"';}?>> Девелопмент</option>
			<option value="MRKT"<? if(isset($_POST['IN_COMPANY_TITLE'])){if($_POST['IN_COMPANY_TITLE']=='MRKT') echo ' selected="selected"';}?>> Маркетинг</option>
		</select>
	</div>
	<div class="data_element" style="float: left; margin-right: 75px;">
		<span class="filter_name" style="">Выбрать месяц:</span>
		<select name="month" style="  display: inline-block;
									  background-color: #fafafa;
									  background-image: -webkit-linear-gradient(top, #f4f4f4, #fff);
									  background-image: -moz-linear-gradient(top, #f4f4f4, #fff);
									  background-image: -ms-linear-gradient(top, #f4f4f4, #fff);
									  background-image: -o-linear-gradient(top, #f4f4f4, #fff);
									  background-image: linear-gradient(to bottom, #f4f4f4, #fff);
									  border: 1px solid;
									  border-color: #e1e4e5 #daddde #c7c9ca;
									  box-shadow: 0 1px 1px 0 #daddde, inset 0 1px 0 0 #fff;
									  line-height: 28px;
									  padding: 0 10px;
									  border-radius: 4px;
									  cursor: pointer;
									  text-decoration: none;
									  color: #67727a;
									  font-weight: normal;
									  font-size: 13px;
									  word-spacing: normal;
									  position: relative;
									  margin: 0 3px;">
		<?
		foreach ($month as $unit)
		{
			?><option value="<?=$unit?>" <? if(isset($_POST['month'])){if($_POST['month'] == $unit) echo ' selected="selected"';}?>><?=$usedb->getMonthName($unit);?></option><? 
		}
		?></select>
	</div>
	<div class="data_element" style="float: left; margin-right: 25px;">
		<span class="YEAR" style="">Выбрать год:</span>
		<select name="YEAR" style="  display: inline-block;
									 background-color: #fafafa;
									 background-image: -webkit-linear-gradient(top, #f4f4f4, #fff);
									 background-image: -moz-linear-gradient(top, #f4f4f4, #fff);
									 background-image: -ms-linear-gradient(top, #f4f4f4, #fff);
									 background-image: -o-linear-gradient(top, #f4f4f4, #fff);
									 background-image: linear-gradient(to bottom, #f4f4f4, #fff);
									 border: 1px solid;
									 border-color: #e1e4e5 #daddde #c7c9ca;
									 box-shadow: 0 1px 1px 0 #daddde, inset 0 1px 0 0 #fff;
									 line-height: 28px;
									 padding: 0 10px;
									 border-radius: 4px;
									 cursor: pointer;
									 text-decoration: none;
									 color: #67727a;
									 font-weight: normal;
									 font-size: 13px;
									 word-spacing: normal;
									 position: relative;
									 margin: 0 3px;">
			<?
		foreach ($year as $unit)
		{
			?><option value="<?=$unit?>" <? if(isset($_POST['month'])){if($_POST['month'] == $unit) echo ' selected="selected"';}?>><?=$unit;?></option><? 
		}
				
		//	<option value="2015">2015</option>?>
		</select>
	</div>
	<input type="hidden" value="clear_cache=Y">
	<p>
		<input type="submit" style=" 
								   display: inline-block;
								   color: #fff;
								   cursor: pointer;
								   font-size: 14px;
								   font-weight: bold;
								   text-shadow: rgba(0,0,0, 0.3) 0 1px 2px;
								   padding: 10px 20px;
								   background: #8DB418;
								   border-radius: 7px;">
	</p>
</form>
<?
if(isset($_REQUEST['month']) || isset($_REQUEST['YEAR']) || isset($_REQUEST['IN_COMPANY_TITLE']))
{
	switch ($_REQUEST['month'])
	{
		case 1:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.0'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 2:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.0'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 3:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.0'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 4:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.0'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 5:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.0'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 6:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.0'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 7:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.0'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 8:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.0'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 9:
			$start = '01.0'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 10:
			$start = '01.'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 11:
			$start = '01.'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.'.($_REQUEST['month']+1).'.'.$_REQUEST['YEAR'].' 00:00:00';
			break;
		case 12:
			$start = '01.'.$_REQUEST['month'].'.'.$_REQUEST['YEAR'].' 00:00:00';
			$end = '01.01.'.($_REQUEST['YEAR']+1).' 00:00:00';
			break;
	}

?><? //unset ($usedb);?>
<?// pre($start);?>
<?// pre($end);?>
<?// pre($_REQUEST['IN_COMPANY_TITLE']);
//die();?>
<?$report = new CompanyReport('periods', $start, $end, $_REQUEST['IN_COMPANY_TITLE'], null );?>
<style type="text/css">
TABLE {
	border-collapse: collapse; 
	width: 100%;
	text-align: center;
}
TABLE TD, TH {	 
	border: 1px solid black; 
}
th{
	width: 70px;
}
table tr:first-child{
	 background-color: #fafafa;
	 background-image: -webkit-linear-gradient(top, #f4f4f4, #fff);
	 background-image: -moz-linear-gradient(top, #f4f4f4, #fff);
	 background-image: -ms-linear-gradient(top, #f4f4f4, #fff);
	 background-image: -o-linear-gradient(top, #f4f4f4, #fff);
	 background-image: linear-gradient(to bottom, #f4f4f4, #fff);
	 background-color: none;
}

table tr th:nth-child(4){
	/*background: green;*/
	/*color: #00AF00;*/
}
table tr th:nth-child(5){
	/*background: #ffff00;*/
	/*color: #A8A503;*/
}
table tr th:nth-child(6){
	/*background: #ff7f24;*/
	/*color:  #ff7f24;*/
}
table tr th:nth-child(7){
	/*background: red;*/
	/*color: #F70D1A;*/
	font-weight: bold;
}
table tr th:nth-child(8){
	/*background: red;*/
	color: #FF0000;
	font-weight: bold;
}

/*STYLE COLORS COLOMS*/

table td.green{
	color:  #00AF00;
	font-weight: bold;
}
table td.yellow{
	color:  #A8A503;
	font-weight: bold;
}

table td.yellow-dark{
	color:  #F57A07;
	font-weight: bold;
}
table td.red{
	color:  #F70D1A;
	font-weight: bold;
}
table td.ddd{
	color:  #FF0000;
	font-weight: bold;
}

/**/
/*table tr:nth-child(2){
	background: RGBA(227,235,237, 0.5);
}*/
table tr:nth-child(3){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(4){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(5){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(6){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(7){
	background: #fff;
}
table tr:nth-child(8){
	background: #fff;
}
table tr:nth-child(9){
	background: #fff;
}

table tr:nth-child(10){
	background: #fff;
}
table tr:nth-child(11){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(12){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(13){
	background: RGBA(227,235,237, 0.5);
}

table tr:nth-child(14){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(15){
	background: #fff;
}
table tr:nth-child(16){
	background: #fff;
}
table tr:nth-child(17){
	background: #fff;
}

table tr:nth-child(18){
	background: #fff;
}
table tr:nth-child(19){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(20){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(21){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(22){
	background: RGBA(227,235,237, 0.5);
}

table tr:nth-child(23){
	background: #fff;
}
table tr:nth-child(24){
	background: #fff;
}
table tr:nth-child(25){
	background: #fff;
}
table tr:nth-child(26){
	background: #fff;
}

table tr:nth-child(27){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(28){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(29){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(30){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(31){
	background: #fff;
}
table tr:nth-child(32){
	background: #fff;
}
table tr:nth-child(33){
	background: #fff;
}
table tr:nth-child(34){
	background: #fff;
}

table tr:nth-child(35){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(36){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(37){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(38){
	background: RGBA(227,235,237, 0.5);
}

table tr:nth-child(39){
	background: #fff;
}
table tr:nth-child(40){
	background: #fff;
}
table tr:nth-child(41){
	background: #fff;
}
table tr:nth-child(42){
	background: fff;
}

table tr:nth-child(43){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(44){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(45){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(46){
	background: RGBA(227,235,237, 0.5);
}

table tr:nth-child(47){
	background: #fff;
}
table tr:nth-child(48){
	background: #fff;
}
table tr:nth-child(49){
	background: #fff;
}
table tr:nth-child(50){
	background: #fff;
}

table tr:nth-child(51){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(52){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(53){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(54){
	background: RGBA(227,235,237, 0.5);
}

table tr:nth-child(55){
	background: #fff;
}
table tr:nth-child(56){
	background: #fff;
}
table tr:nth-child(57){
	background: #fff;
}
table tr:nth-child(58){
	background: #fff;
}

table tr:nth-child(59){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(60){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(61){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(62){
	background: RGBA(227,235,237, 0.5);
}

table tr:nth-child(63){
	background: #fff;
}
table tr:nth-child(64){
	background: #fff;
}
table tr:nth-child(65){
	background: #fff;
}
table tr:nth-child(66){
	background: #fff;
}

table tr:nth-child(67){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(68){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(69){
	background: RGBA(227,235,237, 0.5);
}
table tr:nth-child(70){
	background: RGBA(227,235,237, 0.5);
}
.table_name{
    margin: 30px 0 30px 10px;
    text-transform: capitalize;
}
</style>

<?
if($_REQUEST['IN_COMPANY_TITLE'] == 'MRKT')
{
	$userGroupID = array('488','948','950','951','949','952');
}
if ($_REQUEST['IN_COMPANY_TITLE'] == 'IT')
{
	$userGroupID = $report->getDepartamentWorker (13);
}
if ($_REQUEST['IN_COMPANY_TITLE'] == 'DEV')
{
	$userGroupID = $report->getDistinktWorker ();
}

echo '<h1 class="table_name">Заявки</h1>';
echo '<table border="1">';
echo '<tr> 
			<th rowspan="2">Ф.И.О.</th>
			<th rowspan="2">Сложность</th>
			<th rowspan="2">Всего</th>
			<th rowspan="2">выполнено вовремя</th>
			
		 <th colspan="7">просроченo</th>
		 <th colspan="2">просроченo и </th>
		</tr>';
echo '<tr>
		
		<th> до 20% SLA</th>
		<th> до 40% SLA</th>
		<th> до 60% SLA</th>
		<th> до 100% SLA</th>
		<th> до 200% SLA</th>
		<th> до 300% SLA</th>
		<th> до 400% и более SLA</th>
		<th>Не сделанно</th>
		<th>Отложено</th>
	 </tr>';
		foreach ($userGroupID as $personal_id)
		{
			echo '<tr><td class="rowspan4" rowspan="4">';
			$dbUsers = CUser::GetByID($personal_id);
			$arUser = $dbUsers->Fetch();
			echo $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
			echo '</td>';
			$report->getRowTicket($personal_id, '1');
			$report->getRowTicket($personal_id, '2');
			$report->getRowTicket($personal_id, '3');
			$report->getRowTicket($personal_id, '4');
		}
echo '</table>';
echo '<br>';
echo '<br>';
echo '<br>';

echo '<h1 class="table_name">Задачи</h1>';
echo '<table border="1">';
echo '<tr>
			<th rowspan="2">Ф.И.О.</th>
			<th rowspan="2">Сложность</th>
			<th rowspan="2">Всего</th>
			<th rowspan="2">выполнено вовремя</th>
		
		 <th colspan="7">просроченo</th>
		 <th colspan="2">просроченo и </th>
		</tr>';
echo '<tr>
		
		<th> до 20% SLA</th>
		<th> до 40% SLA</th>
		<th> до 60% SLA</th>
		<th> до 100% SLA</th>
		<th> до 200% SLA</th>
		<th> до 300% SLA</th>
		<th> до 400% и более SLA</th>
		<th>Не сделанно</th>
		<th>Отложено</th>
	 </tr>';
	foreach ($userGroupID as $id)
	{
		echo '<tr><td rowspan="4" >';
		$dbUsers = CUser::GetByID($id);
		$arUser = $dbUsers->Fetch();
		echo $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
		echo '</td>';		
		$report->getRowTask($id, 1);
		$report->getRowTask($id, 2);
		$report->getRowTask($id, 3);
		$report->getRowTask($id, 4);
	}
echo '</table>';

}

?>
<?unset ($report);?>
<?unset ($usedb);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>