<?
$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/ext_www/test2";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);?>
<? 
$mt=array();
$start = time();
$mt['старт']= time() - $start;
$test = new workWithDB;

//удаляем старую таблицу
$test->dropTable('day');
$test->createTable('day');

$MONTH = $test->take_now_time_MONTH();
$YEAR = $test->take_now_time_YEAR();


$mt['выборка_начало']= time() - $start;



/********************** START TICKET *******************************/

if( CModule::IncludeModule("iblock") )
{

	$array_report = array();
	$array_report_temp = array();
	$ticket_task = array();

	$arSelect = Array("ID", "NAME", "DETAIL_TEXT", "DATE_CREATE", "PROPERTY_TASK_ID", "PROPERTY_FINISH_TIME", "PROPERTY_DEADLINE");
	$arFilter = Array(
			"IBLOCK_ID"=>'31', 
			"ACTIVE"=>"Y", 
			"!PROPERTY_TASK_ID" => false,
			">ID" => '2919',	 
	);
	$res = CIBlockElement::GetList(Array("ID"=>'asc'), $arFilter, false,false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$array_report_temp[$arFields['ID']]['ID'] = $arFields['ID'];
		$array_report_temp[$arFields['ID']]['NAME'] = $arFields['NAME'];
		$array_report_temp[$arFields['ID']]['DETAIL_TEXT'] = $arFields['DETAIL_TEXT'];
		$array_report_temp[$arFields['ID']]['DATE_CREATE'] = $arFields['DATE_CREATE'];
	}

	$mt['закончили выборку уникальных ИД заявок']= time() - $start;
	
	foreach ($array_report_temp as $element)
	{
		$PROPS = array();
		$PROPS['MONTH'] = $MONTH;
		$PROPS['YEAR'] = $YEAR;
		$PROPS['TYPE'] = 'TK';
		$PROPS['ID'] = $element['ID'];
		$PROPS['NAME'] = $test->format_to_save_string($element['NAME']);
		$PROPS['DETAIL_TEXT'] = $test->format_to_save_string ($element['DETAIL_TEXT']);
		$PROPS['DATE_CREATE'] = $element['DATE_CREATE'];

		$db_props = CIBlockElement::GetProperty('31', $PROPS['ID'], "sort", "asc", array());
		while($ar_props = $db_props->Fetch())
		{
			$text = str_replace("\r\n",'',$ar_props['VALUE']);
			$text = str_replace("\n",'',$text);
			$PROPS[$ar_props['CODE']] = $text;
		}
		
		if(!empty($PROPS['RESPONSIBLE']))
		{
			$re_RESPONSIBLENSIBLE = CUser::GetByID($PROPS['RESPONSIBLE']);
			$RESPONSIBLENSIBLE = $re_RESPONSIBLENSIBLE->Fetch();
			$PROPS['NAME_RESPONSIBLE'] = $RESPONSIBLENSIBLE['NAME'];
			$PROPS['SECOND_NAME_RESPONSIBLE'] = $RESPONSIBLENSIBLE['SECOND_NAME'];
			$PROPS['LAST_NAME_RESPONSIBLE'] = $RESPONSIBLENSIBLE['LAST_NAME'];
		}else{
			$PROPS['NAME_RESPONSIBLE'] = '';
			$PROPS['SECOND_NAME_RESPONSIBLE'] = '';
			$PROPS['LAST_NAME_RESPONSIBLE'] = '';
		}
		
		if( isset($PROPS['BP_EXEMPLE']) && !empty($PROPS['BP_EXEMPLE']) )
		{
			switch ($PROPS['BP_EXEMPLE']) {
				case 12:
					$PROPS['BP_TITLE'] = 'ИТ_Стандарт';
					$PROPS['DEPARTMENT'] = 'Служба ИТ';
					break;
				case 17:
					$PROPS['BP_TITLE'] = 'DEV_Аварийная';
					$PROPS['DEPARTMENT'] = 'Девелопмент';
					break;
				case 18:
					$PROPS['BP_TITLE'] = 'DEV_Плановая';
					$PROPS['DEPARTMENT'] = 'Девелопмент';
					break;
				case 19:
					$PROPS['BP_TITLE'] = 'MRK_Реклама';
					$PROPS['DEPARTMENT'] = 'Служба Маркетинга';
					break;
				case 20:
					$PROPS['BP_TITLE'] = 'MRK_Коммерция';
					$PROPS['DEPARTMENT'] = 'Служба Маркетинга';
					break;
				case 21:
					$PROPS['BP_TITLE'] = 'MRK_Акция';
					$PROPS['DEPARTMENT'] = 'Служба Маркетинга';
					break;
				case 43:
					$PROPS['BP_TITLE'] = 'logistika';
					$PROPS['DEPARTMENT'] = 'Служба Логистики';
					break;
			}
		
		}else{
			$PROPS['BP_TITLE'] = '';
			$PROPS['DEPARTMENT'] = '';
		}
		
		if(!empty($PROPS['SLA_ID']) )
		{
			if( (CModule::IncludeModule("support")) ) $SLA_INFO_array = CTicketSLA::GetByID($PROPS['SLA_ID']);
			if ($SLA_INFO = $SLA_INFO_array->GetNext())
			{
				$PROPS['SLA_TITLE'] = $SLA_INFO['NAME'];
				$PROPS['SLA_TIME'] = $SLA_INFO['M_RESPONSE_TIME']/60;
			}
			$PROPS['SLA_TIME_TITLE'] = 'ч';
		}else{
			$PROPS['SLA_TITLE'] = '';
			$PROPS['SLA_TIME'] = '';
			$PROPS['SLA_TIME_TITLE'] = '';
		}
		
		//DATE_CREATE
		if(!empty($PROPS['DATE_CREATE']))
		{
			$format = "DD.MM.YYYY HH:MI:SS";
			$HH_START = ParseDateTime($PROPS['DATE_CREATE'], $format);
			$PROPS['START_HH'] = $HH_START["HH"];
		
			$timestamp = MakeTimeStamp($temp_ar_element['DATE_CREATE'], $format);
			$START_DAY = FormatDate("l", $timestamp);
			$PROPS['START_DAY'] = $START_DAY;
		} else{
			$PROPS['START_HH'] = '';
			$PROPS['START_DAY'] = '';
			
		}
		
		//FINISH_TIME
		if(!empty($PROPS['FINISH_TIME']))
		{
			$format = "DD.MM.YYYY HH:MI:SS";
			$HH_FINISH = ParseDateTime($PROPS['FINISH_TIME'], $format);
			$PROPS['FINISH_HH'] = $HH_FINISH["HH"];
		
			$timestamp = MakeTimeStamp($PROPS['FINISH_TIME'], $format);
			$START_DAY = FormatDate("l", $timestamp);
			$PROPS['FINISH_DAY'] = $START_DAY;
		}else{
			$PROPS['FINISH_DAY']= '';
			$PROPS['FINISH_HH'] = '';
		}
		
		if(!empty($PROPS['KOMMERS_DIR']))
		{
			$KOMMERS_DIR = CUser::GetByID($PROPS['KOMMERS_DIR']);
			$KOMMERS_DIR = $KOMMERS_DIR->Fetch();
			$PROPS['NAME_KOMMERS_DIR'] = $KOMMERS_DIR['NAME'];
			$PROPS['SECOND_NAME_KOMMERS_DIR'] = $KOMMERS_DIR['SECOND_NAME'];
			$PROPS['LAST_NAME_KOMMERS_DIR'] = $KOMMERS_DIR['LAST_NAME'];
		} else{
			$PROPS['NAME_KOMMERS_DIR'] = '';
			$PROPS['SECOND_NAME_KOMMERS_DIR'] = '';
			$PROPS['LAST_NAME_KOMMERS_DIR'] = '';
		}
		
		if(!empty($PROPS['SALES_DIR']))
		{
			$SALES_DIR = CUser::GetByID($PROPS['SALES_DIR']);
			$SALES_DIR = $SALES_DIR->Fetch();
			$PROPS['NAME_SALES_DIR'] = $SALES_DIR['NAME'];
			$PROPS['SECOND_NAME_SALES_DIR'] = $SALES_DIR['SECOND_NAME'];
			$PROPS['LAST_NAME_SALES_DIR'] = $SALES_DIR['LAST_NAME'];
		} else{
			$PROPS['NAME_SALES_DIR'] = '';
			$PROPS['SECOND_NAME_SALES_DIR'] = '';
			$PROPS['LAST_NAME_SALES_DIR'] = '';
		}
		
		if(!empty($PROPS['REGION_DIR_DEV']))
		{
			$REGION_DIR_DEV = CUser::GetByID($PROPS['REGION_DIR_DEV']);
			$REGION_DIR_DEV = $REGION_DIR_DEV->Fetch();
			$PROPS['NAME_REGION_DIR_DEV'] = $REGION_DIR_DEV['NAME'];
			$PROPS['SECOND_NAME_REGION_DIR_DEV'] = $REGION_DIR_DEV['SECOND_NAME'];
			$PROPS['LAST_NAME_REGION_DIR_DEV'] = $REGION_DIR_DEV['LAST_NAME'];
		} else{
			$PROPS['NAME_REGION_DIR_DEV'] = '';
			$PROPS['SECOND_NAME_REGION_DIR_DEV'] = '';
			$PROPS['LAST_NAME_REGION_DIR_DEV'] = '';
		}
		
		unset(
			$PROPS['SLA_ID'],
			$PROPS['LAST_SECTION_ID'],
			$PROPS['DELTA_TIME_TO'],
			$PROPS['DELTA_TIME'],
			$PROPS['ID_TASK_LINKED'],
			$PROPS['ASSIGNED_BY_ID'],
			$PROPS['COMMENTS'],
			$PROPS['SOURCE_ID'],
			$PROPS['POST'],
			$PROPS['STATUS'],
			$PROPS['PRIORITY'],
			$PROPS['COMPLEXITY'],
			$PROPS['RESPONSIBLE_TRIGGER'],
			$PROPS['RESPONSIBLE_ID'],
			$PROPS['APPLICANT_ID'],
			$PROPS['RESPONSIBLE_GROUP'],
			$PROPS['RESPONSIBLE_GROUP_ID'],
			$PROPS['FILE_ATTACHMENT'],
			$PROPS['BP_EXEMPLE']			
		);
		
		// приводим время в порядок
		$PROPS['TICKET_TIME'] = $test->convert_time_to_sql($PROPS['TICKET_TIME']);
		$PROPS['LIMITATION'] = $test->convert_time_to_sql($PROPS['LIMITATION']);
		$PROPS['START_TIME'] = $test->convert_time_to_sql($PROPS['START_TIME']);
		$PROPS['DEADLINE'] = $test->convert_time_to_sql($PROPS['DEADLINE']);
		$PROPS['FINISH_TIME'] = $test->convert_time_to_sql($PROPS['FINISH_TIME']);
		$PROPS['DATE_CREATE'] = $test->convert_time_to_sql($PROPS['DATE_CREATE']);


		if(!empty($PROPS['TASK_ID']))
		{
			$ticket_task[] = $PROPS['TASK_ID'];
			$res = CTasks::GetList(array(), array("ID" => $PROPS['TASK_ID'], "CHECK_PERMISSIONS" => "N"), array("*", "UF_TASK_USER", "UF_TASK_CLOSE_DATE"), array());
			while ($arTask = $res->GetNext())
			{
				$PROPS['TASK_TASK_ID'] = $arTask['ID'];
				$PROPS['TASK_TITLE'] = $test->format_to_save_string ($arTask['TITLE']);
				$PROPS['TASK_DESCRIPTION'] = $test->format_to_save_string ($arTask['DESCRIPTION']);
				$PROPS['TASK_RESPONSIBLE_ID'] = $arTask['RESPONSIBLE_ID'];
				$PROPS['TASK_RESPONSIBLE_NAME'] = $arTask['RESPONSIBLE_NAME'];
				$PROPS['TASK_RESPONSIBLE_LAST_NAME'] = $arTask['RESPONSIBLE_LAST_NAME'];
				$PROPS['TASK_RESPONSIBLE_SECOND_NAME'] = $arTask['RESPONSIBLE_SECOND_NAME'];
				$PROPS['TASK_RESPONSIBLE_LOGIN'] = $test->format_to_save_string($arTask['RESPONSIBLE_LOGIN']);
				$PROPS['TASK_RESPONSIBLE_WORK_POSITION'] = $arTask['RESPONSIBLE_WORK_POSITION'];
				$PROPS['TASK_DATE_START'] = $test->convert_time_to_sql($arTask['DATE_START']);
				$PROPS['TASK_DURATION_PLAN'] = $arTask['DURATION_PLAN'];
				$PROPS['TASK_DURATION_TYPE'] = $arTask['DURATION_TYPE'];
				$PROPS['TASK_DURATION_FACT'] = $arTask['DURATION_FACT'];
				$PROPS['TASK_TIME_ESTIMATE'] = $arTask['TIME_ESTIMATE'];
				$PROPS['TASK_DEADLINE'] = $test->convert_time_to_sql($arTask['DEADLINE']);
				$PROPS['TASK_DEADLINE_ORIG'] = $arTask['DEADLINE_ORIG'];
				$PROPS['TASK_START_DATE_PLAN'] = $test->convert_time_to_sql($arTask['START_DATE_PLAN']);
				$PROPS['TASK_END_DATE_PLAN'] = $test->convert_time_to_sql($arTask['END_DATE_PLAN']);
				$PROPS['TASK_CREATED_BY'] = $arTask['CREATED_BY'];
				$PROPS['TASK_CREATED_BY_NAME'] = $arTask['CREATED_BY_NAME'];
				$PROPS['TASK_CREATED_BY_LAST_NAME'] = $arTask['CREATED_BY_LAST_NAME'];
				$PROPS['TASK_CREATED_BY_SECOND_NAME'] = $arTask['CREATED_BY_SECOND_NAME'];
				$PROPS['TASK_CREATED_BY_LOGIN'] = $test->format_to_save_string($arTask['CREATED_BY_LOGIN']);
				$PROPS['TASK_CREATED_DATE'] = $test->convert_time_to_sql($arTask['CREATED_DATE']);
				$PROPS['TASK_CHANGED_DATE'] = $test->convert_time_to_sql($arTask['CHANGED_DATE']);
				$PROPS['TASK_STATUS_CHANGED_DATE'] = $test->convert_time_to_sql($arTask['STATUS_CHANGED_DATE']);
				$PROPS['TASK_CLOSED_BY'] = $arTask['CLOSED_BY'];
				//if(($arTask['REAL_STATUS'] == '5' ||  $arTask['REAL_STATUS'] == '4') && !empty($arTask['UF_TASK_CLOSE_DATE']))
				if(($arTask['REAL_STATUS'] == '5' ||  $arTask['REAL_STATUS'] == '4') && !empty($arTask['UF_TASK_CLOSE_DATE']))
				{
					$PROPS['TASK_CLOSED_DATE'] = $test->convert_time_to_sql($arTask['UF_TASK_CLOSE_DATE']);
					$PROPS['FINISH_TIME'] = $test->convert_time_to_sql($arTask['UF_TASK_CLOSE_DATE']);
					$format = "DD.MM.YYYY HH:MI:SS";
					$HH_FINISH = ParseDateTime($PROPS['FINISH_TIME'], $format);
					$PROPS['FINISH_HH'] = $HH_FINISH["HH"];
					$timestamp = MakeTimeStamp($PROPS['FINISH_TIME'], $format);
					$START_DAY = FormatDate("l", $timestamp);
					$PROPS['FINISH_DAY'] = $START_DAY;
				} else {
					$PROPS['TASK_CLOSED_DATE'] = $test->convert_time_to_sql($arTask['CLOSED_DATE']);
				}
				$PROPS['TASK_ALLOW_CHANGE_DEADLINE'] = $arTask['ALLOW_CHANGE_DEADLINE'];
				$PROPS['TASK_ALLOW_TIME_TRACKING'] = $arTask['ALLOW_TIME_TRACKING'];
				$PROPS['TASK_TASK_CONTROL'] = $arTask['TASK_CONTROL'];
				$PROPS['TASK_PARENT_ID'] = $arTask['PARENT_ID'];
				$PROPS['TASK_COMMENTS_COUNT'] = $arTask['COMMENTS_COUNT'];
				$PROPS['TASK_GRADE'] = $arTask['UF_TASK_USER'];
				$PROPS['TASK_STATUS']  = $arTask['TASK_STATUS'];
				$PROPS['TASK_REAL_STATUS']  = $arTask['REAL_STATUS'];
				$PROPS['TASK_MULTITASK']  = $arTask['MULTITASK'];
				
			}
		}
		else {
			
			$PROPS['TASK_TASK_ID'] = '';
			$PROPS['TASK_TITLE'] = '';
			$PROPS['TASK_DESCRIPTION'] = '';
			$PROPS['TASK_RESPONSIBLE_ID'] = '';
			$PROPS['TASK_RESPONSIBLE_NAME'] = '';
			$PROPS['TASK_RESPONSIBLE_LAST_NAME'] = '';
			$PROPS['TASK_RESPONSIBLE_SECOND_NAME'] = '';
			$PROPS['TASK_RESPONSIBLE_LOGIN'] = '';
			$PROPS['TASK_RESPONSIBLE_WORK_POSITION'] = '';
			$PROPS['TASK_DATE_START'] = '';
			$PROPS['TASK_DURATION_PLAN'] = '';
			$PROPS['TASK_DURATION_TYPE'] = '';
			$PROPS['TASK_DURATION_FACT'] = '';
			$PROPS['TASK_TIME_ESTIMATE'] = '';
			$PROPS['TASK_DEADLINE'] = '';
			$PROPS['TASK_DEADLINE_ORIG'] = '';
			$PROPS['TASK_START_DATE_PLAN'] = '';
			$PROPS['TASK_END_DATE_PLAN'] = '';
			$PROPS['TASK_CREATED_BY'] = '';
			$PROPS['TASK_CREATED_BY_NAME'] = '';
			$PROPS['TASK_CREATED_BY_LAST_NAME'] = '';
			$PROPS['TASK_CREATED_BY_SECOND_NAME'] = '';
			$PROPS['TASK_CREATED_BY_LOGIN'] = '';
			$PROPS['TASK_CREATED_DATE'] = '';
			$PROPS['TASK_CHANGED_DATE'] = '';
			$PROPS['TASK_STATUS_CHANGED_DATE'] = '';
			$PROPS['TASK_CLOSED_BY'] = '';
			$PROPS['TASK_CLOSED_DATE'] = '';
			$PROPS['TASK_ALLOW_CHANGE_DEADLINE'] = '';
			$PROPS['TASK_ALLOW_TIME_TRACKING'] = '';
			$PROPS['TASK_TASK_CONTROL'] = '';
			$PROPS['TASK_PARENT_ID'] = '';
			$PROPS['TASK_COMMENTS_COUNT'] = '';
			$PROPS['TASK_GRADE'] = '';
			$PROPS['TASK_STATUS']  = '';
			$PROPS['TASK_REAL_STATUS']  = '';
			$PROPS['TASK_MULTITASK']  = '';
			
		}
		$array_report[] = $PROPS;
		
	}
}
/********************** END TICKET *********************************/
$mt['загнали заявки в массив']= time() - $start;
/*
echo '<pre>';
print_r($array_report);
echo '</pre>';
/**/
/************************ START TASK *******************************/
if( CModule::IncludeModule("iblock") )
{

//	$array_report = array();
	$array_report_temp = array();
	$ticket_task = array();

	$arSelect = Array("ID", "PROPERTY_TASK_ID");
	$arFilter = Array(
			"IBLOCK_ID"=>'31',
			"ACTIVE"=>"Y",
			"!PROPERTY_TASK_ID" => false,
	);
	$res = CIBlockElement::GetList(Array("ID"=>'asc'), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$array_report_temp[$arFields['PROPERTY_TASK_ID_VALUE']] = $arFields['PROPERTY_TASK_ID_VALUE'];

	}
}

$mt['выбрали задачи заявок']= time() - $start;

if(CModule::IncludeModule("tasks"))
{
	$select = array("ID", "CLOSED_DATE", "REAL_STATUS");
	$arFilter = array("CHECK_PERMISSIONS" => "N");
	$res = CTasks::GetList(array(), $arFilter, $select, array());
	while ($arTask = $res->GetNext())
	{
		$ollTaskT[$arTask['ID']] = $arTask['ID'];
	}
	
	$difer = array_diff_key($ollTaskT, $array_report_temp);
	$mt['определили чистые задачи']= time() - $start;
	foreach ($difer as $id)
	{
		$res = CTasks::GetList(array(), array("ID" =>$id, "CHECK_PERMISSIONS" => "N"), array("*", "UF_*"), array()); //"!ID" =>$ticket_task"<ID" => '2000'"!ID" =>$ticket_task
		while ($arTask = $res->GetNext())
		{
			$PROPS = array();
			$PROPS['MONTH'] = $MONTH;
			$PROPS['YEAR'] = $YEAR;
			$PROPS['TYPE'] = 'TS';
			$PROPS['ID'] = '';
			$PROPS['NAME'] = '';
			$PROPS['DATE_CREATE'] = $test->convert_time_to_sql($arTask['CREATED_DATE']); 
			$PROPS['DETAIL_TEXT'] = '';
			$PROPS['TICKET_ID'] ='';
			$PROPS['STATUS_ID']  ='';
			$PROPS['APPLICANT'] = $arTask['CREATED_BY'];
			$PROPS['LAST_NAME_APPLICANT'] = $arTask['CREATED_BY_LAST_NAME'];
			$PROPS['NAME_APPLICANT'] = $arTask['CREATED_BY_NAME'];
			$PROPS['SECOND_NAME_APPLICANT'] = $arTask['CREATED_BY_SECOND_NAME'];
			$PROPS['APPLICANT_DEPATMENT']  ='';
			$PROPS['RESPONSIBLE'] = $arTask['RESPONSIBLE_ID'];
			$PROPS['LAST_NAME_RESPONSIBLE'] = $arTask['RESPONSIBLE_LAST_NAME'];
			$PROPS['NAME_RESPONSIBLE'] = $arTask['RESPONSIBLE_NAME'];
			$PROPS['SECOND_NAME_RESPONSIBLE'] = $arTask['RESPONSIBLE_SECOND_NAME'];
			$PROPS['SITY'] = '';
			$PROPS['ADDRESS'] = '';
			$PROPS['KOMMERS_DIR']  ='';
			$PROPS['SALES_DIR']  ='';
			$PROPS['REGION_DIR_DEV']  ='';
			$PROPS['TICKET_TIME']  ='';
			$PROPS['START_TIME']  ='';
			$PROPS['DEADLINE']  = $test->convert_time_to_sql($arTask['DEADLINE']);
			$PROPS['FINISH_TIME']  ='';
			$PROPS['OVERDUE']  ='';
			$PROPS['TASK_ID']  ='';
			$PROPS['IN_COMPANY_TITLE']  ='';
			$PROPS['WORK_FLOW_TEMPLATED']  ='';
			$PROPS['TASK_RESULT']  ='';
			$PROPS['LIMITATION']  ='';
			$PROPS['CATEGORIYA']  ='';
			$PROPS['CRITICALITY_ID']  ='';
			$PROPS['SECTION_1']  ='';
			$PROPS['SECTION_2']  ='';
			$PROPS['SECTION_3']  ='';
			$PROPS['SECTION_4']  ='';
			$PROPS['SECTION_5']  ='';
			$PROPS['BP_TITLE']  ='';
			$PROPS['DEPARTMENT']  ='';
			$PROPS['SLA_TITLE']  ='';
			$PROPS['SLA_TIME']  ='';
			$PROPS['SLA_TIME_TITLE']  ='';
			$PROPS['START_HH']  ='';
			$PROPS['START_DAY']  ='';
			$PROPS['FINISH_HH']  ='';
			$PROPS['FINISH_DAY']  ='';
			$PROPS['NAME_KOMMERS_DIR']  ='';
			$PROPS['SECOND_NAME_KOMMERS_DIR']  ='';
			$PROPS['LAST_NAME_KOMMERS_DIR']  ='';
			$PROPS['NAME_SALES_DIR']  ='';
			$PROPS['SECOND_NAME_SALES_DIR']  ='';
			$PROPS['LAST_NAME_SALES_DIR']  ='';
			$PROPS['NAME_REGION_DIR_DEV']  ='';
			$PROPS['SECOND_NAME_REGION_DIR_DEV'] ='';
			$PROPS['LAST_NAME_REGION_DIR_DEV'] ='';
			$PROPS['TASK_TASK_ID'] = $arTask['ID'];
			$PROPS['TASK_TITLE'] = $test->format_to_save_string ($arTask['TITLE']);
			$PROPS['TASK_DESCRIPTION'] = $test->format_to_save_string ($arTask['DESCRIPTION']);
			$PROPS['TASK_RESPONSIBLE_ID'] = $arTask['RESPONSIBLE_ID'];
			$PROPS['TASK_RESPONSIBLE_NAME'] = $arTask['RESPONSIBLE_NAME'];
			$PROPS['TASK_RESPONSIBLE_LAST_NAME'] = $arTask['RESPONSIBLE_LAST_NAME'];
			$PROPS['TASK_RESPONSIBLE_SECOND_NAME'] = $arTask['RESPONSIBLE_SECOND_NAME'];
			$PROPS['TASK_RESPONSIBLE_LOGIN'] = $test->format_to_save_string($arTask['RESPONSIBLE_LOGIN']);
			$PROPS['TASK_RESPONSIBLE_WORK_POSITION'] = $arTask['RESPONSIBLE_WORK_POSITION'];
			$PROPS['TASK_DATE_START'] = $test->convert_time_to_sql($arTask['DATE_START']);
			$PROPS['TASK_DURATION_PLAN'] = $arTask['DURATION_PLAN'];
			$PROPS['TASK_DURATION_TYPE'] = $arTask['DURATION_TYPE'];
			$PROPS['TASK_DURATION_FACT'] = $arTask['DURATION_FACT'];
			$PROPS['TASK_TIME_ESTIMATE'] = $arTask['TIME_ESTIMATE'];
			$PROPS['TASK_DEADLINE'] = $test->convert_time_to_sql($arTask['DEADLINE']);
			$PROPS['TASK_DEADLINE_ORIG'] = $arTask['DEADLINE_ORIG'];
			$PROPS['TASK_START_DATE_PLAN'] = $test->convert_time_to_sql($arTask['START_DATE_PLAN']);
			$PROPS['TASK_END_DATE_PLAN'] = $test->convert_time_to_sql($arTask['END_DATE_PLAN']);
			$PROPS['TASK_CREATED_BY'] = $arTask['CREATED_BY'];
			$PROPS['TASK_CREATED_BY_NAME'] = $arTask['CREATED_BY_NAME'];
			$PROPS['TASK_CREATED_BY_LAST_NAME'] = $arTask['CREATED_BY_LAST_NAME'];
			$PROPS['TASK_CREATED_BY_SECOND_NAME'] = $arTask['CREATED_BY_SECOND_NAME'];
			$PROPS['TASK_CREATED_BY_LOGIN'] = $test->format_to_save_string($arTask['CREATED_BY_LOGIN']);
			$PROPS['TASK_CREATED_DATE'] = $test->convert_time_to_sql($arTask['CREATED_DATE']);
			$PROPS['TASK_CHANGED_DATE'] = $test->convert_time_to_sql($arTask['CHANGED_DATE']);
			$PROPS['TASK_STATUS_CHANGED_DATE'] = $test->convert_time_to_sql($arTask['STATUS_CHANGED_DATE']);
			$PROPS['TASK_CLOSED_BY'] = $arTask['CLOSED_BY'];
			if(($arTask['REAL_STATUS'] == '5' ||  $arTask['REAL_STATUS'] == '4') && !empty($arTask['UF_TASK_CLOSE_DATE']))
			{
				$PROPS['TASK_CLOSED_DATE'] = $test->convert_time_to_sql($arTask['UF_TASK_CLOSE_DATE']);
			} else {
				$PROPS['TASK_CLOSED_DATE'] = $test->convert_time_to_sql($arTask['CLOSED_DATE']);
			}
			//$PROPS['TASK_CLOSED_DATE'] = $test->convert_time_to_sql($arTask['CLOSED_DATE']);
			$PROPS['TASK_ALLOW_CHANGE_DEADLINE'] = $arTask['ALLOW_CHANGE_DEADLINE'];
			$PROPS['TASK_ALLOW_TIME_TRACKING'] = $arTask['ALLOW_TIME_TRACKING'];
			$PROPS['TASK_TASK_CONTROL'] = $arTask['TASK_CONTROL'];
			$PROPS['TASK_PARENT_ID'] = $arTask['PARENT_ID'];
			$PROPS['TASK_COMMENTS_COUNT'] = $arTask['COMMENTS_COUNT'];
			$PROPS['TASK_GRADE'] = $arTask['UF_TASK_USER'];
			$PROPS['TASK_STATUS']  = $arTask['TASK_STATUS'];
			$PROPS['TASK_REAL_STATUS']  = $arTask['REAL_STATUS'];
			$PROPS['TASK_MULTITASK']  = $arTask['MULTITASK'];
			$array_report[] = $PROPS;
		}
	}

}

$mt['загнали чистые задачи в массив']= time() - $start;

/************************ END TASK *********************************/

/*
echo '<pre>';
print_r($array_report);
echo '</pre>';
/**/

/********************** START SQL **********************************/


$VALUE = '';
$i=0;
$z=0;
foreach ($array_report as $value)
{
	$VALUE_ROW='';
	$VALUE_ROW ='(\'\','
			.'\''.$value['MONTH'].'\','
            .'\''.$value['YEAR'].'\','
			.'\''.$value['TYPE'].'\','
            .'\''.$value['ID'].'\','
            .'\''.$value['NAME'].'\','
			.'\''.$value['DETAIL_TEXT'].'\','
			.'\''.$value['DATE_CREATE'].'\','
            .'\''.$value['TICKET_ID'].'\','
            .'\''.$value['STATUS_ID'].'\','
            .'\''.$value['APPLICANT'].'\','
            .'\''.$value['LAST_NAME_APPLICANT'].'\','
            .'\''.$value['NAME_APPLICANT'].'\','
            .'\''.$value['SECOND_NAME_APPLICANT'].'\','
            .'\''.$value['APPLICANT_DEPATMENT'].'\','
            .'\''.$value['RESPONSIBLE'].'\','
            .'\''.$value['LAST_NAME_RESPONSIBLE'].'\','
            .'\''.$value['NAME_RESPONSIBLE'].'\','
            .'\''.$value['SECOND_NAME_RESPONSIBLE'].'\','
            .'\''.$value['SITY'].'\','
            .'\''.$value['ADDRESS'].'\','
            .'\''.$value['KOMMERS_DIR'].'\','
            .'\''.$value['SALES_DIR'].'\','
            .'\''.$value['REGION_DIR_DEV'].'\','
            .'\''.$value['TICKET_TIME'].'\','
            .'\''.$value['START_TIME'].'\','
            .'\''.$value['DEADLINE'].'\','
            .'\''.$value['FINISH_TIME'].'\','
            .'\''.$value['OVERDUE'].'\','
            .'\''.$value['TASK_ID'].'\','
            .'\''.$value['IN_COMPANY_TITLE'].'\','
            .'\''.$value['WORK_FLOW_TEMPLATED'].'\','
            .'\''.$value['TASK_RESULT'].'\',' 
            .'\''.$value['LIMITATION'].'\','
            .'\''.$value['CATEGORIYA'].'\','
            .'\''.$value['CRITICALITY_ID'].'\','
            .'\''.$value['SECTION_1'].'\','
            .'\''.$value['SECTION_2'].'\','
            .'\''.$value['SECTION_3'].'\','
            .'\''.$value['SECTION_4'].'\','
            .'\''.$value['SECTION_5'].'\','
            .'\''.$value['BP_TITLE'].'\','
            .'\''.$value['DEPARTMENT'].'\','
            .'\''.$value['SLA_TITLE'].'\','
            .'\''.$value['SLA_TIME'].'\','
            .'\''.$value['SLA_TIME_TITLE'].'\','
            .'\''.$value['FINISH_HH'].'\','
            .'\''.$value['FINISH_DAY'].'\','
			.'\''.$value['NAME_KOMMERS_DIR'].'\','
			.'\''.$value['SECOND_NAME_KOMMERS_DIR'].'\','
			.'\''.$value['LAST_NAME_KOMMERS_DIR'].'\','
			.'\''.$value['NAME_SALES_DIR'].'\','
			.'\''.$value['SECOND_NAME_SALES_DIR'].'\','
			.'\''.$value['LAST_NAME_SALES_DIR'].'\','
			.'\''.$value['NAME_REGION_DIR_DEV'].'\','
			.'\''.$value['SECOND_NAME_REGION_DIR_DEV'].'\','
			.'\''.$value['LAST_NAME_REGION_DIR_DEV'].'\','           		          		
            .'\''.$value['TASK_TASK_ID'].'\','
            .'\''.$value['TASK_TITLE'].'\','
            .'\''.$value['TASK_DESCRIPTION'].'\','
            .'\''.$value['TASK_RESPONSIBLE_ID'].'\','
            .'\''.$value['TASK_RESPONSIBLE_NAME'].'\','
            .'\''.$value['TASK_RESPONSIBLE_LAST_NAME'].'\','
            .'\''.$value['TASK_RESPONSIBLE_SECOND_NAME'].'\','
            .'\''.$value['TASK_RESPONSIBLE_LOGIN'].'\','
            .'\''.$value['TASK_RESPONSIBLE_WORK_POSITION'].'\','
            .'\''.$value['TASK_DATE_START'].'\','
            .'\''.$value['TASK_DURATION_PLAN'].'\','
            .'\''.$value['TASK_DURATION_TYPE'].'\','
            .'\''.$value['TASK_DURATION_FACT'].'\','
            .'\''.$value['TASK_TIME_ESTIMATE'].'\','
            .'\''.$value['TASK_DEADLINE'].'\','
            .'\''.$value['TASK_DEADLINE_ORIG'].'\','
            .'\''.$value['TASK_START_DATE_PLAN'].'\','
            .'\''.$value['TASK_END_DATE_PLAN'].'\','
            .'\''.$value['TASK_CREATED_BY'].'\','
            .'\''.$value['TASK_CREATED_BY_NAME'].'\','
            .'\''.$value['TASK_CREATED_BY_LAST_NAME'].'\','
            .'\''.$value['TASK_CREATED_BY_SECOND_NAME'].'\','
            .'\''.$value['TASK_CREATED_BY_LOGIN'].'\',' 
            .'\''.$value['TASK_CREATED_DATE'].'\','
            .'\''.$value['TASK_CHANGED_DATE'].'\',' 
            .'\''.$value['TASK_STATUS_CHANGED_DATE'].'\','
            .'\''.$value['TASK_CLOSED_BY'].'\',' 
            .'\''.$value['TASK_CLOSED_DATE'].'\','
            .'\''.$value['TASK_ALLOW_CHANGE_DEADLINE'].'\','
            .'\''.$value['TASK_ALLOW_TIME_TRACKING'].'\','
            .'\''.$value['TASK_TASK_CONTROL'].'\','
            .'\''.$value['TASK_PARENT_ID'].'\','
            .'\''.$value['TASK_COMMENTS_COUNT'].'\','
            .'\''.$value['TASK_GRADE'].'\','
            .'\''.$value['TASK_STATUS'].'\','
			.'\''.$value['TASK_REAL_STATUS'].'\','
			.'\''.$value['TASK_MULTITASK'].'\'),';
	
	
	$VALUE .= $VALUE_ROW;
	
	if( $i == 1000 )
	{
		$z++;
		$VALUE = trim($VALUE,',');
	
		$col = '
		(
			COUNT,
			MONTH,
			YEAR,
			TYPE,
			ID,
	        NAME,
			DETAIL_TEXT,
			DATE_CREATE,
	        TICKET_ID,
	        STATUS_ID,
	        APPLICANT,
	        LAST_NAME_APPLICANT,
	        NAME_APPLICANT,
	        SECOND_NAME_APPLICANT,
	        APPLICANT_DEPATMENT,
	        RESPONSIBLE,
	        LAST_NAME_RESPONSIBLE,
	        NAME_RESPONSIBLE,
	        SECOND_NAME_RESPONSIBLE,
	        SITY,
	        ADDRESS,
	        KOMMERS_DIR,
	        SALES_DIR,
	        REGION_DIR_DEV,
	        TICKET_TIME,
	        START_TIME,
	        DEADLINE,
	        FINISH_TIME,
	        OVERDUE,
	        TASK_ID,
	        IN_COMPANY_TITLE,
	        WORK_FLOW_TEMPLATED,
	        TASK_RESULT,
	        LIMITATION,
	        CATEGORIYA, 
	        CRITICALITY_ID,
	        SECTION_1,
	        SECTION_2,
	        SECTION_3,
	        SECTION_4,
	        SECTION_5, 
	        BP_TITLE,
	        DEPARTMENT,
	        SLA_TITLE,
	        SLA_TIME,
	        SLA_TIME_TITLE,
	        FINISH_HH,
	        FINISH_DAY,
	        NAME_KOMMERS_DIR,
	        SECOND_NAME_KOMMERS_DIR,
	        LAST_NAME_KOMMERS_DIR,
	        NAME_SALES_DIR,
	        SECOND_NAME_SALES_DIR,
	        LAST_NAME_SALES_DIR,
	        NAME_REGION_DIR_DEV,
	        SECOND_NAME_REGION_DIR_DEV,
	        LAST_NAME_REGION_DIR_DEV,
	        TASK_TASK_ID,
	        TASK_TITLE,
	        TASK_DESCRIPTION,
	        TASK_RESPONSIBLE_ID,
	        TASK_RESPONSIBLE_NAME,
	        TASK_RESPONSIBLE_LAST_NAME,
	        TASK_RESPONSIBLE_SECOND_NAME,
	        TASK_RESPONSIBLE_LOGIN,
	        TASK_RESPONSIBLE_WORK_POSITION,
	        TASK_DATE_START,
	        TASK_DURATION_PLAN,
	        TASK_DURATION_TYPE,
	        TASK_DURATION_FACT,
	        TASK_TIME_ESTIMATE,
	        TASK_DEADLINE,
	        TASK_DEADLINE_ORIG,
	        TASK_START_DATE_PLAN,
	        TASK_END_DATE_PLAN,
	        TASK_CREATED_BY,
	        TASK_CREATED_BY_NAME,
	        TASK_CREATED_BY_LAST_NAME,
	        TASK_CREATED_BY_SECOND_NAME,
	        TASK_CREATED_BY_LOGIN,
	        TASK_CREATED_DATE,
	        TASK_CHANGED_DATE,
	        TASK_STATUS_CHANGED_DATE,
	        TASK_CLOSED_BY,
	        TASK_CLOSED_DATE,
	        TASK_ALLOW_CHANGE_DEADLINE,
	        TASK_ALLOW_TIME_TRACKING,
	        TASK_TASK_CONTROL,
	        TASK_PARENT_ID,
	        TASK_COMMENTS_COUNT,
	        TASK_GRADE,
			TASK_STATUS,
        	TASK_REAL_STATUS,
        	TASK_MULTITASK
		)';
		
		$sql = 'INSERT INTO day '.$col.' VALUES '.$VALUE;  //periods
		$result = mysql_query($sql);
		
		if (!$result) {
			echo "Ошибка базы, не удалось получить список таблиц\n";
			echo 'Ошибка MySQL: ' . mysql_error().'<br>';
			exit;
		}
		
		$VALUE='';
		$i=0;
	}
	$i++;
}
	


echo ' В базу записанно '.$z.$i.' строк. <br>';

$VALUE = trim($VALUE,',');

$col = '
		(
			COUNT,
			MONTH,
			YEAR,
			TYPE,
			ID,
	        NAME,
			DETAIL_TEXT,
			DATE_CREATE,
	        TICKET_ID,
	        STATUS_ID,
	        APPLICANT,
	        LAST_NAME_APPLICANT,
	        NAME_APPLICANT,
	        SECOND_NAME_APPLICANT,
	        APPLICANT_DEPATMENT,
	        RESPONSIBLE,
	        LAST_NAME_RESPONSIBLE,
	        NAME_RESPONSIBLE,
	        SECOND_NAME_RESPONSIBLE,
	        SITY,
	        ADDRESS,
	        KOMMERS_DIR,
	        SALES_DIR,
	        REGION_DIR_DEV,
	        TICKET_TIME,
	        START_TIME,
	        DEADLINE,
	        FINISH_TIME,
	        OVERDUE,
	        TASK_ID,
	        IN_COMPANY_TITLE,
	        WORK_FLOW_TEMPLATED,
	        TASK_RESULT,
	        LIMITATION,
	        CATEGORIYA,
	        CRITICALITY_ID,
	        SECTION_1,
	        SECTION_2,
	        SECTION_3,
	        SECTION_4,
	        SECTION_5,
	        BP_TITLE,
	        DEPARTMENT,
	        SLA_TITLE,
	        SLA_TIME,
	        SLA_TIME_TITLE,
	        FINISH_HH,
	        FINISH_DAY,
	        NAME_KOMMERS_DIR,
	        SECOND_NAME_KOMMERS_DIR,
	        LAST_NAME_KOMMERS_DIR,
	        NAME_SALES_DIR,
	        SECOND_NAME_SALES_DIR,
	        LAST_NAME_SALES_DIR,
	        NAME_REGION_DIR_DEV,
	        SECOND_NAME_REGION_DIR_DEV,
	        LAST_NAME_REGION_DIR_DEV,
	        TASK_TASK_ID,
	        TASK_TITLE,
	        TASK_DESCRIPTION,
	        TASK_RESPONSIBLE_ID,
	        TASK_RESPONSIBLE_NAME,
	        TASK_RESPONSIBLE_LAST_NAME,
	        TASK_RESPONSIBLE_SECOND_NAME,
	        TASK_RESPONSIBLE_LOGIN,
	        TASK_RESPONSIBLE_WORK_POSITION,
	        TASK_DATE_START,
	        TASK_DURATION_PLAN,
	        TASK_DURATION_TYPE,
	        TASK_DURATION_FACT,
	        TASK_TIME_ESTIMATE,
	        TASK_DEADLINE,
	        TASK_DEADLINE_ORIG,
	        TASK_START_DATE_PLAN,
	        TASK_END_DATE_PLAN,
	        TASK_CREATED_BY,
	        TASK_CREATED_BY_NAME,
	        TASK_CREATED_BY_LAST_NAME,
	        TASK_CREATED_BY_SECOND_NAME,
	        TASK_CREATED_BY_LOGIN,
	        TASK_CREATED_DATE,
	        TASK_CHANGED_DATE,
	        TASK_STATUS_CHANGED_DATE,
	        TASK_CLOSED_BY,
	        TASK_CLOSED_DATE,
	        TASK_ALLOW_CHANGE_DEADLINE,
	        TASK_ALLOW_TIME_TRACKING,
	        TASK_TASK_CONTROL,
	        TASK_PARENT_ID,
	        TASK_COMMENTS_COUNT,
	        TASK_GRADE,
			TASK_STATUS,
        	TASK_REAL_STATUS,
        	TASK_MULTITASK
		)';

$sql = 'INSERT INTO day '.$col.' VALUES '.$VALUE; //periods
$result = mysql_query($sql);

if (!$result) {
	echo "Ошибка базы, не удалось получить список таблиц\n";
	echo 'Ошибка MySQL: ' . mysql_error().'<br>';
	exit;
}
$mt['загнали массив в базу']= time() - $start;
/********************** END SQL **********************************/

echo '<pre>';
print_r($mt);
echo '</pre>';
	
unset ($test);
?>!!OK!!
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>