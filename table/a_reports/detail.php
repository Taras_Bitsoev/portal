<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Детализация из отчета");?><?

//pre($_REQUEST);

function convert_sql_to_time($sqltime)
{
	if(!isset($sqltime)) return;
	$two_side = explode(' ', $sqltime);
	$left_side = explode('-',$two_side['0']);
	return $left_side['2'].'.'.$left_side['1'].'.'.$left_side['0'].' '.$two_side['1'];
}

function bildTableTK($arResult = array())
{
	$i=0;
	if(empty($arResult)){
		echo '<span>В данном разделе нет заявок.</span>';
		return false;
	} else {
		echo '<table><tr><th>№ п/п</th><th>№ заявки</th><th>№ задачи</th>';
		echo '<th>Старт</th><th>Финиш</th><th>Крайний срок</th><th>Ответственный по задаче</th></tr>';
		foreach ($arResult as $element){
		$i++;
			?>
			<tr>
				<td><?=$i;?></td>
				<td>
					<a href="/table/bid/the_bid_detail.php?ID=<?=$element['TICKET_ID'];?>&clear_cache=Y">
						<?=$element['TICKET_ID'];?>
					</a>
				</td>
				<td>
					<a href="/table/bid/test.php?TASK_ID=<?=$element['TASK_ID'];?>&clear_cache=Y">
						<?=$element['TASK_ID'];?>
					</a>
				</td>
				<td><?=convert_sql_to_time($element['DATE_CREATE']);?></td>
				<td><?=convert_sql_to_time($element['FINISH_TIME']);?></td>
				<td><?=convert_sql_to_time($element['DEADLINE']);?></td>
				<td><? 
					$dbUsers = CUser::GetByID($element['TASK_RESPONSIBLE_ID']);
					$arUser = $dbUsers->Fetch();
					echo '<a href="/company/personal/user/'.$element['TASK_RESPONSIBLE_ID'].'/">';
					echo $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
					echo '</a>';
				?></td>
			</tr><?
				$rsTask = CTasks::GetList(
						array('ID'=>'ASC'), 
						array('PARENT_ID'=>$element['TASK_ID'], '!PARENT_ID'=>0)
				);
				while ($arTask = $rsTask->GetNext()){
					?>	
					<tr>
						<td colspan="2"> Подзадача: </td>
						<td>
							<a href="/table/bid/the_bid_detail.php?ID=<?=$arTask['ID'];?>&clear_cache=Y">
							<?=$arTask['ID'];?>
							</a>
						</td>
						<td><?=$arTask['CREATED_DATE'];?></td>
						<td><?=$arTask['CLOSED_DATE'];?></td>
						<td><?=$arTask['DEADLINE'];?></td>
						<td><?
							echo '<a href="/company/personal/user/'.$arTask['RESPONSIBLE_ID'].'/">';
							echo $arUser['RESPONSIBLE_LAST_NAME'].' '.$arUser['RESPONSIBLE_NAME'].' '.$arUser['RESPONSIBLE_SECOND_NAME'];
							echo '</a>';
						?></td>
					</tr>
					<?
				}	
		}?>
		</table>
	<?}
	return true;
}

function bildTableTS($arResult = array()) //, $depart
{
	$i=0;
	if(empty($arResult)){
		echo '<span>В данном разделе нет задач.</span>';
		return false;
	} else {
		?>
		<table>
			<tr>
				<td>№ п/п</td>
				<td>№ задачи</td>
				<td>Старт</td>
				<td>Финиш</td>
				<td>Крайний срок</td>
				<td>Ответственный по задаче</td>	
			</tr>
		<?foreach ($arResult as $element){
		$i++;
			?>
		
			<tr>
				<td><?=$i;?></td>
				<td>
					<a href="/table/bid/test.php?TASK_ID=<?=$element['TASK_TASK_ID'];?>&clear_cache=Y">
						<?=$element['TASK_TASK_ID'];?>
					</a>
				</td>
				<td><?=convert_sql_to_time($element['TASK_CREATED_DATE']);?></td>
				<td><?=convert_sql_to_time($element['TASK_CLOSED_DATE']);?></td>
				<td><?=convert_sql_to_time($element['TASK_DEADLINE']);?></td>
				<td><? 
					$dbUsers = CUser::GetByID($element['TASK_RESPONSIBLE_ID']);
					$arUser = $dbUsers->Fetch();
					echo '<a href="/company/personal/user/'.$element['TASK_RESPONSIBLE_ID'].'/">';
					echo $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];
					echo '</a>';
				?></td>
			</tr><?
				$rsTask = CTasks::GetList(
						array('ID'=>'ASC'), 
						array('PARENT_ID'=>$element['TASK_ID'], '!PARENT_ID'=>0));
				while ($arTask = $rsTask->GetNext()){
					?>	
					<tr>
						<td> Подзадача: </td>
						<td>
							<a href="/table/bid/the_bid_detail.php?ID=<?=$arTask['ID'];?>&clear_cache=Y">
							<?=$arTask['ID'];?>
							</a>
						</td>
						<td><?=$arTask['CREATED_DATE'];?></td>
						<td><?=$arTask['CLOSED_DATE'];?></td>
						<td><?=$arTask['DEADLINE'];?></td>
						<td><?
							echo '<a href="/company/personal/user/'.$arTask['RESPONSIBLE_ID'].'/">';
							echo $arUser['RESPONSIBLE_LAST_NAME'].' '.$arUser['RESPONSIBLE_NAME'].' '.$arUser['RESPONSIBLE_SECOND_NAME'];
							echo '</a>';
						?></td>
					</tr>
					<?
				}	
		}?>
		</table>
	<?}
	return true;
}

switch ($_REQUEST['GR'])
{
	case 1:
		$GR = 1;
		break;
	case 2:
		$GR = 2;
		break;
	case 3:
		$GR = 3;
		break;
	case 4:
		$GR = 4;
		break;
	default:
		echo 'Не корректный запрос данных!';
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
		die();
		break;
}

switch (strtolower($_REQUEST['table'])){
	case 'periods':
		$tableName = 'periods';
		break;
	case 'day':
		$tableName = 'day';
		break;
	default:
		echo 'Не корректный запрос данных!';
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
		die();
		break;
}

switch ($_REQUEST['YEAR'])
{
	case 2014:
		$YEAR = 2014;
		break;
	case 2015:
		$YEAR = 2015;
		break;
	case 2016:
		$YEAR = 2016;
		break;
	case 2017:
		$YEAR = 2017;
		break;
	case 2018:
		$YEAR = 2018;
		break;
	case 2019:
		$YEAR = 2019;
		break;
	case 2020:
		$YEAR = 2020;
		break;
	default:
		echo 'Не корректный запрос данных!';
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
		die();
		break;
}
switch ($_REQUEST['month'])
{
	case 1:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 2:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 3:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 4:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 5:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 6:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 7:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 8:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 9:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.0'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 10:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 11:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.'.($_REQUEST['month']+1).'.'.$YEAR.' 00:00:00';
		break;
	case 12:
		$start = '01.'.$_REQUEST['month'].'.'.$YEAR.' 00:00:00';
		$end = '01.01.'.($YEAR+1).' 00:00:00';
		break;
	default:
		echo 'Не корректный запрос данных!';
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
		die();
		break;
}



switch ($_REQUEST['DEP'])
{
	case 'IT':
		$departament = 'IT';
		break;
	case 'DEV':
		$departament = 'DEV';
		break;
	case 'MRKT':
		$departament = 'MRKT';
		break;
	case 'HR':
		$departament = 'HR';
		break;
	default:
		echo 'Не корректный запрос данных!';
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
		die();
		break;
}

//echo '$departament :'.$departament.'<br>';
//echo '$start :'.$start.'<br>';
//echo '$end :'.$end.'<br>';



$dbUsers = CUser::GetByID($_REQUEST['ID']);
$arUser = $dbUsers->Fetch();?>

<p>Расшифровка заявок и задач по пользователю <a href="/company/personal/user/<?=$_REQUEST['ID']?>/">
<?echo $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'];?></a>:</p>
<?
$usedb = new workWithDB;
$report = new reportForCompani($tableName, $start, $end, $departament, null ); 

if(isset($_REQUEST['COLUM']) || !empty($_REQUEST['COLUM']) ){
	$arrDEP = explode('_', $_REQUEST['COLUM']);
	switch ($arrDEP['1']){
		case 'TK':
			switch ($arrDEP['0'])
			{
				case 'AL':
					// всего
					bildTableTK($report->getALLTicketDetail($_REQUEST['ID'], $GR));
					break;
				case 'IT':
					// вовремя
					bildTableTK($report->getInTimeTicketDetail($_REQUEST['ID'], $GR));
					break;
				case 'FA':
					bildTableTK($report->getFailTicketDetail($_REQUEST['ID'], $GR)['a']);
					break;
				case 'FB':
					bildTableTK($report->getFailTicketDetail($_REQUEST['ID'], $GR)['b']);
					break;
				case 'FC':
					bildTableTK($report->getFailTicketDetail($_REQUEST['ID'], $GR)['c']);
					break;
				case 'FD':
					bildTableTK($report->getFailTicketDetail($_REQUEST['ID'], $GR)['d']);
					break;
				case 'FE':
					bildTableTK($report->getFailTicketDetail($_REQUEST['ID'], $GR)['e']);
					break;
				case 'FF':
					bildTableTK($report->getFailTicketDetail($_REQUEST['ID'], $GR)['f']);
					break;
				case 'FG':
					bildTableTK($report->getFailTicketDetail($_REQUEST['ID'], $GR)['g']);
					break;
				case 'EF':
					//не сделано
					bildTableTK($report->getEpicFailTicketDetail($_REQUEST['ID'], $GR));
					break;
				case 'EP':
					// отложено
					bildTableTK($report->getPostponedEpicFailTicketDetail($_REQUEST['ID'], $GR));
					break;
				default:
					echo 'Не корректный запрос данных!';
					unset ($report);
					unset ($usedb);
					require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
					die();
					break;
			}
			break;
		case 'TS':
			switch ($arrDEP['0'])
			{
				case 'AL':
					// всего
					bildTableTS($report->getALLTaskDetail($_REQUEST['ID'], $GR));
					break;
				case 'IT':
					// вовремя
					bildTableTS($report->getInTimeTaskDetail($_REQUEST['ID'], $GR));
					break;
				case 'FA':
					bildTableTS($report->getFailTaskDetail($_REQUEST['ID'], $GR)['a']);
					break;
				case 'FB':
					bildTableTS($report->getFailTaskDetail($_REQUEST['ID'], $GR)['b']);
					break;
				case 'FC':
					bildTableTS($report->getFailTaskDetail($_REQUEST['ID'], $GR)['c']);
					break;
				case 'FD':
					bildTableTS($report->getFailTaskDetail($_REQUEST['ID'], $GR)['d']);
					break;
				case 'FE':
					bildTableTS($report->getFailTaskDetail($_REQUEST['ID'], $GR)['e']);
					break;
				case 'FF':
					bildTableTS($report->getFailTaskDetail($_REQUEST['ID'], $GR)['f']);
					break;
				case 'FG':
					//echo  '$GR='.$GR.'<br/>';
					//pre($report->getFailTaskDetail($_REQUEST['ID'], $GR));
					bildTableTS($report->getFailTaskDetail($_REQUEST['ID'], $GR)['g']);
					break;
				case 'EF':
					//не сделано
					bildTableTS($report->getEpicFailTaskDetail($_REQUEST['ID'], $GR));
					break;
				case 'EP':
					// отложено
					bildTableTS($report->getPostponedEpicFailTaskDetail($_REQUEST['ID'], $GR));
					break;
				default:
					echo 'Не корректный запрос данных!';
					unset ($report);
					unset ($usedb);
					require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
					die();
					break;
			}
			break;
		default:
			echo 'Не корректный запрос данных!';
			unset ($report);
			unset ($usedb);
			require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
			die();
			break;
	}
}else{
	echo 'Не корректный запрос данных!';
	unset ($report);
	unset ($usedb);
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
	die();
}
?>
<?unset ($report);?>
<?unset ($usedb);?>
<style>
	table{
		width: 100%;
		text-align: center;
		border-collapse: collapse;
		border: 1px solid #000;
	}
	table tr:nth-child(odd){
		background: RGBA(227,235,237, 0.5);
	}
	table th, td{
		padding: 10px 0;
		border: 1px solid #000;
	}
	
</style>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>