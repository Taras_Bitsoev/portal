<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?><?
$APPLICATION->SetTitle("Суточное поступление заявок");?>
<?/* this tab's for tablo.php */
$report = new report;?>
<div class="parent" id="parent">
	<div class="headTabs">
		<input id="tab_1" type="radio" checked="checked" name="radio_type" class="13" 
		value="v13" onclick="thatClick(13)" />
		<label for="tab_1">Служба ИТ</label>
	
		<input id="tab_2" type="radio" name="radio_type" class="23" value="v23" onclick="thatClick(23)"/>
		<label for="tab_2">Девелопмент</label>
	
		<input id="tab_3" type="radio" name="radio_type" class="10" value="v10" onclick="thatClick(10)"/>
		<label for="tab_3">Маркетинг</label>
		
		<input id="tab_4" type="radio" name="radio_type" class="460" value="v8" onclick="thatClick(460)"/>
		<label for="tab_4">Коммерческая служба</label>
	</div>
	<div class="tab_containers" id="tab_containers"></div>
</div>
<style>
	
	.parent{
		margin: 0 auto;
		padding: 20px;
	}
	.headTabs{
		float: left;
		width: 100%;
	    border-right: 1px solid white;
	    height: 50px;
	    
		z-index: 999;
	}
	.headTabs input{
		text-indent: -9999;
		position: absolute;
		left: -99999px;
		top: 0;
	}
	.headTabs label{
		display: block;
	    padding: 8px 15px;
	    border: 1px solid grey;
	    border-radius: 5px 5px 0 0;
	    float: left;
	    margin: 0 1px;
	    color: #000;
	    font-weight: bold;
	    position: relative;
	    top: 16px;
	}
	.headTabs label:hover{cursor: pointer;}
	.headTabs label:first-of-type{
		margin: 0 1px 0 0;
	}
	.headTabs input:checked + label{
		display: block;
	   padding: 16px 15px 7px 15px;
	   border: 1px solid grey;
	   border-bottom: 1px solid #fff;
	   z-index: 99;
	   float: left;
	   background: #FFF;
	   color: #000;
	   font-weight: bold;
	   border-radius: 5px 5px 0 0;
	   position: relative;
	   top: 9px;
	}
	.tab_containers{
	   clear: both;
	   padding: 20px 10px 20px 10px;
	   border: 1px solid grey;
	   top: -1px;
	   left: 0;
	   position: relative;
	}
	.headTabs input#tab_1 + label ~ .tab_containers .tab_1_conts,
	.headTabs input#tab_2 + label ~ .tab_containers .tab_2_conts,
	.headTabs input#tab_3 + label ~ .tab_containers .tab_3_conts,
	.headTabs input#tab_4 + label ~ .tab_containers .tab_4_conts{
		display: none;
	}
	.headTabs input#tab_1:checked + label ~ .tab_containers .tab_1_conts,
	.headTabs input#tab_2:checked + label ~ .tab_containers .tab_2_conts,
	.headTabs input#tab_3:checked + label ~ .tab_containers .tab_3_conts,
	.headTabs input#tab_4:checked + label ~ .tab_containers .tab_4_conts {
		display: block;
	}
	.tab_containers table tr:nth-child(odd){
		background: RGBA(227,235,237, .7);
	}
	.tab_containers #tableTabs tbody .fixedHead{
	    /*border: 1px solid black;*/
	    text-align: center;
	    /*font-weight: bold;*/
	    background: #e3ebed;
	    width: 100%;
    }
    .tab_containers #tableTabs{
    	position: relative;
    }
    .tab_containers #tableTabs tr td{
      text-align: center;
    }
</style>
<?php
//GLOBAL $USER;
$TargetGroupe = array(8,10,13,23);
$UserGroups=array();
$UserID = $USER->GetID();
//pre($UserID);
$res = CUser::GetUserGroupList($UserID);
while ($nowUSerGroups = $res->Fetch()){
	$UserGroups[]=$nowUSerGroups['GROUP_ID'];
}
//pre($UserGroups);
foreach ($TargetGroupe as $target){
	if(in_array($target, $UserGroups)){
		switch ($target){
			case '13':
				// IT
				$departament = 13;
				break;
			case '23':
				// DEV
				$departament = 23;
				break;
			case '10':
				// MRKTG
				$departament = 10;
				break;
			case '8':
				// HR
				$departament = 8;
				break;
		}
	} else{
		$departament = 13;
	}
}
?>
<script>
//JS use only
function reqestLOAD(a, b){
	params = "DEPARTAMENT="+a;
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/table/monitoring/router.php', true);
	xhr.onreadystatechange = function() { // (3)
		b.innerHTML = 'Загрузка данных...';
		if (xhr.readyState != 4) return;
		if (xhr.status != 200) {
			b.innerHTML = (xhr.status + ': ' + xhr.statusText);
		} else {
			b.innerHTML = xhr.responseText;
		}
	}
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(params);
}
if(document.getElementsByClassName){
	getElementsByClass = function(classList, node) {    
		return (node || document).getElementsByClassName(classList);
	}
} else {
	getElementsByClass = function(classList, node){			
		var node = node || document,
		list = node.getElementsByTagName('*'),
		length = list.length,
		classArray = classList.split(/\s+/),
		classes = classArray.length,
		result = [], i,j;
		for(i = 0; i < length; i++) {
			for(j = 0; j < classes; j++)  {
				if(list[i].className.search('\\b' + classArray[j] + '\\b') != -1) {
					result.push(list[i]);
					break;
				}
			}
		}
		return result;
	}
}
function bindReady(handler){
	var called = false;
	function ready() { // (1)
		if (called) return;
		called = true;
		handler();
	}
	if ( document.addEventListener ){
		document.addEventListener( "DOMContentLoaded", function(){
			ready();
		}, false );
	} else if ( document.attachEvent ) {
		if ( document.documentElement.doScroll && window == window.top ){
			function tryScroll(){
				if (called) return;
				if (!document.body) return;
				try {
					document.documentElement.doScroll("left")
					ready();
				} catch(e) {
					setTimeout(tryScroll, 0);
				}
			}
			tryScroll();
		}
		document.attachEvent("onreadystatechange", function(){
			if ( document.readyState === "complete" ){
				ready();
			}
		});
	}
    if (window.addEventListener){
        window.addEventListener('load', ready, false);
    }else if (window.attachEvent){
        window.attachEvent('onload', ready);
    }
}
readyList = [];
function onReady(handler) {
	if (!readyList.length) {
		bindReady(function() {
			for(var i=0; i<readyList.length; i++) {
				readyList[i]();
			}
		})
	}
	readyList.push(handler);
}
function isEmpty(str){
    return (!str || 0 === str.length);
}
var parent = document.getElementById('parent');
var result = document.getElementById('tab_containers');
var activeEl = <?=$departament?>;
var setID = 0;
onReady(function(){
	reqestLOAD(activeEl, result);
	setID = setInterval(function(){
		reqestLOAD(activeEl, result);
	}, 60000);
});
function thatClick(targetEL){
	clearInterval(setID);
	//var targetEL = event.target || event.srcElement;
	console.log(targetEL);
	activeEl = targetEL;
	if(!isEmpty(activeEl)){
		reqestLOAD(activeEl, result);
		setID = setInterval(function(){
			reqestLOAD(activeEl, result);
		}, 60000);
	}
}


// JQ use only
var $parent = $("#parent");
var $tab_containers = $parent.children('.tab_containers');
var $headTabs = $(".headTabs");

$(function(){
    $(window).scroll(function() {
    	$Table = $('#tableTabs');
    	//$fixedHeadTable = $('.fixedHead');
    	var $fixedHead = $('.tab_containers #tableTabs tbody .fixedHead');
        var top = $(document).scrollTop();
        if (top < 320) {
        	$headTabs.css({'height':'49px','width':'78.2%', 'border-bottom': '1px solid grey',  'top': '0', 'position': 'relative'});
            $tab_containers.css({'height': 'auto', 'width': '99%', 'top': '-1px', 'position': 'relative'});
           // $fixedHead.css({'position':'relative','width':'1005px', 'top':'0','left':'0','background': 'inherit' });
		} else {
        	$headTabs.css({'height':'49px','width':'78.2%', 'border-bottom': '1px solid grey', 'top': '0px', 'position': 'fixed', 'background': 'white'});
           	$tab_containers.css({'height': 'auto', 'width': '99%', 'top': '-8px', 'position': 'relative'});
           //	$fixedHead.css({'position':'fixed', 'width':'1005px', 'left':'inherit', 'top':'50px', 'background': '#e3ebed',  'z-index':'999'});
		}
    });
});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");